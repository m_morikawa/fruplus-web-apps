<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('filter');
	list($langFlag,$validate_translation) = loadTranslation('filter_validate');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	
	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;

	if(isset($_POST['userForm_submit'])){
		$isUpdateFormSeen = 1;
	}

	$filter = json_decode(json_encode($config->xml()->autoMode->filter),TRUE);
?>
<!DOCTYPE html>
<html lang="<?php echo $langFlag;?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>

<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>

function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<span  style="float: right;">
			<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
			<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
				<option v-for="language in languages" v-bind:value="language.value">
					{{language.name}}
				</option>
			</select>
		</span>
	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::FILTER); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo COMMON_UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>	
				<form method="post" action="filter.php" name="userForm"  class="forms" @submit.prevent="onSubmit">
					<fieldset>
						<legend><h5><?php echo HEADER_PCFILTER; ?></h5></legend>
						<section>
							<label><?php echo ITEM_STATUS; ?></label>
							<row>
								<column cols="6">
									<select v-model="pcFilterStatus" @change="fetchPcFilter" class="select custom" class="width-10">
										<option v-for="item in status" v-bind:value="item.value">
											{{item.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="pcfilterconfig" v-show="isPcFilterConfigSeen" class="configcontainer">
								<section>
									<label><?php echo COMMON_OFFSET; ?> : bit</label>
									<input type="text" class="width-4 custom" v-model="pcFilterOffset">
								</section>
								<section>
									<label><?php echo COMMON_LENGTH; ?> : bit</label>
									<input type="text" class="width-4 custom" v-model="pcFilterLength">
								</section>
								<section>
									<label><?php echo COMMON_DATA; ?> : HEX</label>
									<input type="text" class="width-4 custom" v-model="pcFilterData">
								</section>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_EPCFILTER; ?></h5></legend>
						<section>
							<label><?php echo ITEM_STATUS; ?></label>
							<row>
								<column cols="6">
									<select v-model="epcFilterStatus" @change="fetchEpcFilter" class="select custom" class="width-10">
										<option v-for="item in status" v-bind:value="item.value">
											{{item.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="epcfilterconfig" v-show="isEpcFilterConfigSeen" class="configcontainer">
								<section>
									<label><?php echo COMMON_OFFSET; ?> : bit</label>
									<input type="text" class="width-4 custom" v-model="epcFilterOffset">
								</section>
								<section>
									<label><?php echo COMMON_LENGTH; ?> : bit</label>
									<input type="text" class="width-4 custom" v-model="epcFilterLength">
								</section>
								<section>
									<label><?php echo COMMON_DATA; ?> : HEX</label>
									<input type="text" class="width-4 custom" v-model="epcFilterData">
									<p>{{epcFilterData.length}}/32<?php echo COMMON_LETTERS; ?></p>
								</section>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_RSSIFILTER; ?></h5></legend>
						<section>
							<label><?php echo ITEM_RSSIFILTER_LOWER; ?></label>
							<row>
								<column cols="6">
									<select v-model="rssiFilterLowerStatus" @change="fetchRssiFilterLower" class="select custom" class="width-10">
										<option v-for="item in status" v-bind:value="item.value">
											{{item.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="rssifilterlowerconfig" v-show="isRssiFilterLowerConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_RSSIFILTER_LOWER_VALUE; ?> : dBm</label>
									<input type="text" class="width-4 custom" v-model="rssiFilterLowerValue">
								</section>
							</div>
						</section>
						
						<section>
							<label><?php echo ITEM_RSSIFILTER_UPPER; ?></label>
							<row>
								<column cols="6">
									<select v-model="rssiFilterUpperStatus" @change="fetchRssiFilterUpper" class="select custom" class="width-10">
										<option v-for="item in status" v-bind:value="item.value">
											{{item.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="rssifilterupperconfig" v-show="isRssiFilterUpperConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_RSSIFILTER_UPPER_VALUE; ?> : dBm</label>
									<input type="text" class="width-4 custom" v-model="rssiFilterUpperValue">
								</section>
							</div>
						</section>
					</fieldset>
					<section>
						<button class="primary"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button" v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button"><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var validate_translation_config = JSON.parse('<?php echo json_encode($validate_translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($filter,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"ja"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		status:[
			{name:common_translation_config['COMMON_ENABLE'],value:true},
			{name:common_translation_config['COMMON_DISABLE'],value:false}
		],
		pcFilterStatus:current_config["pcFilter"]["enable"] == "true",
		pcFilterOffset:current_config["pcFilter"]["offset"],
		pcFilterLength:current_config["pcFilter"]["length"],
		pcFilterData:current_config["pcFilter"]["data"],
		isPcFilterConfigSeen:false,
		epcFilterStatus:current_config["epcFilter"]["enable"] == "true",
		epcFilterOffset:current_config["epcFilter"]["offset"],
		epcFilterLength:current_config["epcFilter"]["length"],
		epcFilterData:current_config["epcFilter"]["data"],
		isEpcFilterConfigSeen:false,
		rssiFilterLowerStatus:current_config["rssiFilter"]["lower"]["enable"] == "true",
		rssiFilterLowerValue:current_config["rssiFilter"]["lower"]["value"],
		isRssiFilterLowerConfigSeen:false,
		rssiFilterUpperStatus:current_config["rssiFilter"]["upper"]["enable"] == "true",
		rssiFilterUpperValue:current_config["rssiFilter"]["upper"]["value"],
		isRssiFilterUpperConfigSeen:false,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:0,
		errorMessage:"",
	},
	methods: {
		fetchPcFilter:function(){
			this.isPcFilterConfigSeen = this.pcFilterStatus;
		},
		fetchEpcFilter:function(){
			this.isEpcFilterConfigSeen = this.epcFilterStatus;
		},
		fetchRssiFilterLower:function(){
			this.isRssiFilterLowerConfigSeen = this.rssiFilterLowerStatus;
		},
		fetchRssiFilterUpper:function(){
			this.isRssiFilterUpperConfigSeen = this.rssiFilterUpperStatus;
		},
		toCurrent:function(){
			this.pcFilterStatus = current_config["pcFilter"]["enable"] == "true";
			this.pcFilterOffset = current_config["pcFilter"]["offset"];
			this.pcFilterLength = current_config["pcFilter"]["length"];
			this.pcFilterData = current_config["pcFilter"]["data"];
			this.epcFilterStatus = current_config["epcFilter"]["enable"] == "true";
			this.epcFilterOffset = current_config["epcFilter"]["offset"];
			this.epcFilterLength = current_config["epcFilter"]["length"];
			this.epcFilterData = current_config["epcFilter"]["data"];
			this.rssiFilterLowerStatus = current_config["rssiFilter"]["lower"]["enable"] == "true";
			this.rssiFilterLowerValue = current_config["rssiFilter"]["lower"]["value"];
			this.rssiFilterUpperStatus = current_config["rssiFilter"]["upper"]["enable"] == "true";
			this.rssiFilterUpperValue = current_config["rssiFilter"]["upper"]["value"];

			this.fetchPcFilter();
			this.fetchEpcFilter();
			this.fetchRssiFilterLower();
			this.fetchRssiFilterUpper();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		onSubmit: function () {
			axios.post('./api/v1/commands.php', 
			{
				"name":"setFilter",
				"parameters" : {
					"filter" : {
						"pcFilter" : {
							"enable" : this.pcFilterStatus,
							"offset" : this.pcFilterOffset,
							"length" : this.pcFilterLength,
							"data" : this.pcFilterData
						},
						"epcFilter" : {
							"enable" : this.epcFilterStatus,
							"offset" : this.epcFilterOffset,
							"length" : this.epcFilterLength,
							"data" : this.epcFilterData
						},
						"rssiFilter" : {
							"lower" : {
								"enable" : this.rssiFilterLowerStatus,
								"value" : this.rssiFilterLowerValue
							},
							"upper" : {
								"enable" : this.rssiFilterUpperStatus,
								"value" : this.rssiFilterUpperValue
							}
						}
					}
				}
			})
			.then(response=>{
				this.isErrorLabelSeen = false;
				var rand = Math.floor( Math.random() * 9998 ) + 1 ;
				var script = document.createElement('script');
				script.id = "beforeSubmit";
				script.src = './js/beforesubmit.php?rand='+rand;
				script.type = "text/javascript";
				beforeSubmit = undefined;
				if(script.addEventListener){
					script.addEventListener("load", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.userForm.submit();
						}
					}, false);
				}else if(script.attachEvent){
					script.attachEvent("onload", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.uuserForm.submit();
						}
					});
					script.onreadystatechange = function(){
						if(this.readyState=="loaded"||this.readyState=="complete"){
							if(typeof beforeSubmit == 'function'){
								beforeSubmit();
								document.userForm.submit();
							}
						}
					}
				}
				document.userForm.appendChild(script);
			})
			.catch(error=>{
				this.isErrorLabelSeen = true;
				this.errorMessage = error.response.data.error.detail + "<br>" + validate_translation_config[error.response.data.error.detail];
				window.scroll(0,0);
			});
		}
	},
	created:function(){
		this.fetchPcFilter();
		this.fetchEpcFilter();
		this.fetchRssiFilterLower();
		this.fetchRssiFilterUpper();
	},
	computed: {

	}
});

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById("btn_do_update") != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>
