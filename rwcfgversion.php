<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'RwConfigParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$parameter = new RwConfigParameter();
	$dat = $parameter->dat();

	if(isset($_POST['rwcfgversion_submit'])){
		function validateVersion(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'version_major')){
				$msg[] = "パラメータバージョンの版数を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['version_major'] !== "0"){
				if(!filter_input(INPUT_POST, 'version_major', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
					$msg[] = "パラメータバージョンの版数は0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'version_minor')){
				$msg[] = "パラメータバージョンのレベルを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['version_minor'] !== "0"){
				if(!filter_input(INPUT_POST, 'version_minor', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
					$msg[] = "パラメータバージョンのレベルは0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateVersion($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$dat[0xC8 + 1] = $_POST['version_major'];
			$dat[0xC9 + 1] = $_POST['version_minor'];
			if(!$parameter->save($dat)){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] .= "パラメータバージョン設定の保存に失敗しました。";
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::PARAMETERVERSION); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>マニュアルモード設定 > パラメータバージョン</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary" id="updateform">
<section>
<strong>パラメータバージョン設定を変更しました。</strong><Br>
この設定をリーダーに反映するには、以下の手順が必要です。
<div style="max-width:60em;padding-top:1em;">
<ol style="list-style-type:decimal">
<li style="width:100%;"><strong>設定を反映する</strong>ボタンをクリックします。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
<li style="width:100%;">リーダーのLED A(緑)とB(赤)が点滅後消灯し、LED A(緑)が点灯します。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
</ol>
</div>
</section>

<section>
<div style="max-width:60em;padding-top:1em;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
var elem = document.getElementById('update_rwcfgversion');
if(elem){
document.getElementsByTagName('body')[0].removeChild(elem);
}
var script = document.createElement('script');
script.src = './js/updaterwcfg.php';
script.id = 'update_rwcfgversion';
document.getElementsByTagName('body')[0].appendChild(script);
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').className = "feedback";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('btn_do_update').style.disabled = false;
document.getElementById('btn_do_update').className = "primary";
document.getElementById('btn_do_update').innerHTML = "設定を反映する";
document.getElementById('btn_do_update').style.display="none";
document.getElementById('btn_cancel_update').style.display="none";
}, 3000)
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</section>

</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="rwcfgversion.php" class="forms">
					<section>
						<label>版数</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="version_major" value="{$dat[0xC8 + 1]}" placeholder="{$dat[0xC8 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>レベル</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="version_minor" value="{$dat[0xC9 + 1]}" placeholder="{$dat[0xC9 + 1]}">
EOF;
						?>
					</section>
					<section>
						<button class="primary" name="rwcfgversion_submit">変更する</button>
						<button class="secondary" type="button" name="rwcfgversion_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="rwcfgversion_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		echo "document.getElementsByName(\"version_major\")[0].value = \"".$dat[0xC8 + 1]."\";";
		echo "document.getElementsByName(\"version_minor\")[0].value = \"".$dat[0xC9 + 1]."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("version_major")[0].value= "0";
		document.getElementsByName("version_minor")[0].value = "0";
	}
</script>
</body>
</html>
