<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'SystemParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$parameter = new SystemParameter();
	$developinfo = $parameter->xml()->develop;
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['develop_datamonitor'])){
		function validateDevelop(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'develop_datamonitor')){
				$msg[] = "データモニタの有効/無効を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['develop_datamonitor'] !== "true" && $_POST['develop_datamonitor'] !== "false"){
				$msg[] = "不正なデータモニタの有効/無効が入力されています。";
				$ret = FALSE;
			}
			return $ret;
		}

		$msg = array();
		if(!validateDevelop($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$developinfo->datamonitor[0] = $_POST['develop_datamonitor'];
			$parameter->save();

			exec('sudo frucgi update develop',$res);
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "開発設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
	</div>

	<div id="contents">
		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::DEVELOP); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>システム設定 > 開発</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary">
<strong>開発設定を変更しました。</strong> <br/>
この設定を反映するするためには、<strong>デバイスの電源を切り、再起動してください</strong>。
</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="develop.php" class="forms">
					<fieldset>
						<legend><h5>データモニタ</h5></legend>
						<h4>モニタ用PCからリーダーのポート60001へソケット接続すると、送信先へのデータと同一のデータをモニタ用PCへ送信します。</h4>
						<p>データモニタの有効にするかどうか設定してください。</p>
						<div style="padding-bottom:16px;">
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								データモニタ機能は、マニュアルモード以外のモードで有効です。
							</div>
						</div>
						<label>データモニタ</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" id="develop_datamonitor" name="develop_datamonitor">
									<?php
										if ( $developinfo->datamonitor == "true"){
											echo "<option value=true selected>有効</option><option value=false>無効</option>";
										} else if ( $developinfo->datamonitor == "false"){
											echo "<option value=true>有効</option><option value=false selected>無効</option>";
										}
									?>
								</select>
							</column>
						</row>
					</fieldset>
					<section>
						<button class="primary" name="develop_submit">変更する</button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>
