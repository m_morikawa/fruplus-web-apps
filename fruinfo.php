<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
  if(!isset($_SESSION['appversion']) || $_SESSION['appversion'] == ""){
		$_SESSION['appversion'] = trim(file_get_contents("/etc/fruapp/APPVERSION"));
	}
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

  $version_conf = array(
    'fwversion' => trim(file_get_contents("/etc/fruapp/FWVERSION")),
    'appversion' => trim(file_get_contents("/etc/fruapp/APPVERSION")),
    'configversion' => trim(file_get_contents("/etc/fruapp/CONFIGVERSION")),
    'kernelversion' => ''
  );

	exec('uname -r',$ret);
	$version_conf['kernelversion'] = trim($ret[0]);

	$fru_info = array();

	$ret = array();
	exec('sudo frucgi info',$ret);
	$status = json_decode($ret[0],true);
	if(strpos($status['status'],"success") !== false){
		$infofile = file_get_contents("/var/frustatus");
		if($infofile !== FALSE){
			$fru_info = json_decode(trim($infofile),true);
		}else{
			var_dump("RW情報の取得に失敗しました。");
		}
	}else{
		var_dump("RW情報の取得に失敗しました。");
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
  document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
  if(myWindow.addEventListener){
    myWindow.addEventListener('load', onWindowLoaded, false);
  }else if(myWindow.attachEvent){
    myWindow.attachEvent('onload', onWindowLoaded);
  }else{
    myWindow.onload = onWindowLoaded;
  }
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  	</div>

	<div id="contents">
		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::INFO); ?>
			<div id="main">
        			<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF0A7;"></span><strong>端末情報</strong></h2>
				<form method="post" action="" class="forms">
					<table>
						<caption><h5 style="text-decoration: underline;">バージョン</h5></caption>
						<tbody>
							<tr>
								<td>FWバージョン</td>
								<td style="text-align: right;"><?php echo  $version_conf['fwversion']?></td>
							</tr>
							<tr>
								<td>FRUAPPバージョン</td>
								<td style="text-align: right;"><?php echo  $version_conf['appversion']?></td>
							</tr>
							<tr>
								<td>設定ツールバージョン</td>
								<td style="text-align: right;"><?php echo  $version_conf['configversion']?></td>
							</tr>
							<tr>
								<td>カーネルバージョン</td>
								<td style="text-align: right;"><?php echo  $version_conf['kernelversion']?></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption><h5 style="text-decoration: underline;">端末情報</h5></caption>
						<tbody>
							<tr>
								<td>リーダーコード</td>
								<td style="text-align: right;"><?php echo  $fru_info['uhfmodule']['code']?></td>
							</tr>
							<tr>
								<td>UHFモジュールバージョン</td>
								<td style="text-align: right;"><?php echo  $fru_info['uhfmodule']['major'].'.'.$fru_info['uhfmodule']['minor']?></td>
							</tr>
							<tr>
								<td>MACアドレス</td>
								<td style="text-align: right;"><?php echo  $fru_info['device']['macaddress']?></td>
							</tr>
							<tr>
								<td>デバイスID</td>
								<td style="text-align: right;"><?php echo  $fru_info['device']['id']?></td>
							</tr>
							<tr>
								<td>端末時刻</td>
								<td style="text-align: right;"><?php echo  $fru_info['time']['datetime']?></td>
							</tr>
							<tr>
								<td>起動時間</td>
								<td style="text-align: right;"><?php echo  $fru_info['time']['uptime']?></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption><h5 style="text-decoration: underline;">ネットワーク</h5></caption>
						<tbody>
							<tr>
								<td>IP設定</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['bootproto']?></td>
							</tr>
							<tr>
								<td>IPアドレス</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['ipaddress']?></td>
							</tr>
							<tr>
								<td>ネットマスク</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['netmask']?></td>
							</tr>
							<tr>
								<td>ゲートウェイ</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['gateway']?></td>
							</tr>
							<tr>
								<td>優先DNS</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['dns1']?></td>
							</tr>
							<tr>
								<td>代替DNS</td>
								<td style="text-align: right;"><?php echo  $fru_info['network']['dns2']?></td>
							</tr>
						</tbody>
					</table>
					<div style="padding-top:64px;max-width:520px;">
						<figure>
							<img src="img/mts-logo.png" alt="" />
							<figcaption>Copyright(C) 2005-2017 MARS TOHKEN SOLUTION CO.LTD. All Rights Reserved</figcaption>
						</figure>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>
