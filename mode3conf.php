<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('mode3Conf');
	list($langFlag,$validate_translation) = loadTranslation('mode3Conf_validate');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;

	if(isset($_POST['userForm_submit'])){
		$isUpdateFormSeen = 1;
	}

	$mode3Conf = json_decode(json_encode($config->xml()->autoMode->mode3Conf),TRUE);
?>

<!DOCTYPE html>
<html lang="<?php echo $langFlag;?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>

<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<span  style="float: right;">
		<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
		<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
			<option v-for="language in languages" v-bind:value="language.value">
				{{language.name}}
			</option>
		</select>
	</span>
  </div>

  <div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::MODE3CONF); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo COMMON_UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>
				<form method="post" action="mode3conf.php" class="forms" name="userForm" @submit.prevent="onSubmit">
					<fieldset>
						<legend><h5><?php echo HEADER_EVENT; ?></h5></legend>
						<section>
							<label><?php echo ITEM_EVENT_IN; ?></label>
							<row>
								<column cols="6">
									<select v-model="event_in" class="select custom" class="width-10">
										<option v-for="option in notifyOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<section>
							<label><?php echo ITEM_EVENT_OUT; ?></label>
							<row>
								<column cols="6">
									<select v-model="event_out" class="select custom" class="width-10">
										<option v-for="option in notifyOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<section>
							<label><?php echo ITEM_EVENT_TIMEOUT; ?></label>
							<input type="text" class="width-6 custom" v-model="event_timeout" >
						</section>
						<section>
							<label><?php echo ITEM_EVENT_DATA; ?></label>
							<row>
								<column cols="6">
									<select v-model="event_data" @change="fetchMonitor" class="select custom" class="width-10">
										<option v-for="option in dataOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_MONITOR; ?></h5></legend>
						<section>
							<label><?php echo ITEM_MONITOR_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="monitor_enable" class="select custom" class="width-10">
										<option v-for="option in status" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isMonitorConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ITEM_MONITOR_INTERVAL; ?></label>
								<input type="text" class="width-6 custom" v-model="monitor_interval" >
							</section>
						</div>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_DOUT; ?></h5></legend>
						<section>
							<label><?php echo ITEM_DOUT_IN_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="dout_in_enable" @change="fetchDoutIn" class="select custom" class="width-10">
										<option v-for="option in doutOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isDoutInConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ITEM_DOUT_PORT; ?></label>
								<row>
									<column cols="6">
										<select v-model="dout_in_port" class="select custom" class="width-10">
											<option v-for="option in portOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_DOUT_HOLDTIME; ?></label>
								<input type="text" class="width-6 custom" v-model="dout_in_holdTime" >
							</section>
						</div>
						<section>
							<label><?php echo ITEM_DOUT_OUT_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="dout_out_enable" @change="fetchDoutOut" class="select custom" class="width-10">
										<option v-for="option in doutOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isDoutOutConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ITEM_DOUT_PORT; ?></label>
								<row>
									<column cols="6">
										<select v-model="dout_out_port" class="select custom" class="width-10">
											<option v-for="option in portOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_DOUT_HOLDTIME; ?></label>
								<input type="text" class="width-6 custom" v-model="dout_out_holdTime" >
							</section>
						</div>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_RESPONSE; ?></h5></legend>
						<section>
							<label><?php echo ITEM_RESPONSE_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="response_enable" @change="fetchResponse" class="select custom" class="width-10">
										<option v-for="option in status" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isResponseConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ITEM_RESPONSE_TIMEOUT; ?></label>
								<input type="text" class="width-6 custom" v-model="response_timeout" >
							</section>
						</div>
					</fieldset>
					<fieldset>
					<legend><h5><?php echo HEADER_REQUEST; ?></h5></legend>
						<section>
							<label><?php echo ITEM_REQUEST_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="request_enable" class="select custom" class="width-10">
										<option v-for="option in status" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<section>
						<button class="primary"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button"  v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button" ><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var validate_translation_config = JSON.parse('<?php echo json_encode($validate_translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($mode3Conf,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"ja"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		notifyOptions:[
			{name:translation_config["OPTION_NOTIFY"],value:true},
			{name:translation_config["OPTION_NOTNOTIFY"],value:false},
		],
		dataOptions:[
			{name:translation_config["OPTION_CHANGE"],value:"change"},
			{name:translation_config["OPTION_ALL"],value:"all"},
		],
		portOptions:[
			{name:common_translation_config["COMMON_PORT0"],value:0},
			{name:common_translation_config["COMMON_PORT1"],value:1},
		],
		doutOptions:[
			{name:common_translation_config["COMMON_DOUT_CONTROL"],value:true},
			{name:common_translation_config["COMMON_DOUT_NOTCONTROL"],value:false},
		],
		status:[
			{name:common_translation_config["COMMON_ENABLE"],value:true},
			{name:common_translation_config["COMMON_DISABLE"],value:false},
		],
		event_in:current_config["event"]["in"] == "true",
		event_out:current_config["event"]["out"] == "true",
		event_timeout:current_config["event"]["timeout"],
		event_data:current_config["event"]["data"],
		monitor_enable:current_config["monitor"]["enable"] == "true",
		monitor_interval:current_config["monitor"]["interval"],
		dout_in_enable:current_config["dout"]["in"]["enable"] == "true",
		dout_in_port:current_config["dout"]["in"]["port"],
		dout_in_holdTime:current_config["dout"]["in"]["holdTime"],
		dout_out_enable:current_config["dout"]["out"]["enable"] == "true",
		dout_out_port:current_config["dout"]["out"]["port"],
		dout_out_holdTime:current_config["dout"]["out"]["holdTime"],
		response_enable:current_config["response"]["enable"] == "true",
		response_timeout:current_config["response"]["timeout"],
		request_enable:current_config["request"]["enable"] == "true",

		isMonitorConfigSeen:false,
		isDoutInConfigSeen:false,
		isDoutOutConfigSeen:false,
		isResponseConfigSeen:false,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:0,
		errorMessage:"",
	},
	methods: {
		fetchMonitor:function(){
			this.isMonitorConfigSeen = this.monitor_enable;
		},
		fetchDoutIn:function(){
			this.isDoutInConfigSeen = this.dout_in_enable;
		},
		fetchDoutOut:function(){
			this.isDoutOutConfigSeen = this.dout_out_enable;
		},
		fetchResponse:function(){
			this.isResponseConfigSeen = this.response_enable;
		},
		toCurrent:function(){
			this.event_in = current_config["event"]["in"];
			this.event_out = current_config["event"]["out"];
			this.event_timeout = current_config["event"]["timeout"];
			this.event_data = current_config["event"]["data"];
			this.monitor_enable = current_config["monitor"]["enable"];
			this.monitor_interval = current_config["monitor"]["interval"];
			this.dout_in_enable = current_config["dout"]["in"]["enable"];
			this.dout_in_port = current_config["dout"]["in"]["port"];
			this.dout_in_holdTime = current_config["dout"]["in"]["hold"];
			this.dout_out_enable = current_config["dout"]["out"]["enable"];
			this.dout_out_port = current_config["dout"]["out"]["port"];
			this.dout_out_holdTime = current_config["dout"]["out"]["hold"];
			this.response_enable = current_config["response"]["enable"];
			this.response_timeout = current_config["response"]["timeout"];
			this.request_enable = current_config["request"]["enable"];

			this.fetchMonitor();
			this.fetchDoutIn();
			this.fetchDoutOut();
			this.fetchResponse();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		onSubmit: function () {
			axios.post('./api/v1/commands.php', {
				"name":"setMode3Conf",
				"parameters" : {
					"mode3Conf": {
						"event" : {
							"in" : this.event_in,
							"out" : this.event_out,
							"timeout" : this.event_timeout,
							"data" : this.event_data
						},
						"monitor" : {
							"enable" : this.monitor_enable,
							"interval" : this.monitor_interval
						},
						"dout" : {
							"in" : {
								"enable" : this.dout_in_enable,
								"port" : this.dout_in_port,
								"holdTime" : this.dout_in_holdTime
							},
							"out" : {
								"enable" : this.dout_out_enable,
								"port" : this.dout_out_port,
								"holdTime" : this.dout_out_holdTime
							}
						},
						"response" : {
							"enable" : this.response_enable,
							"timeout" : this.response_timeout
						},
						"request" : {
							"enable" : this.request_enable
						}
					}
				}
			})
			.then(response=>{
				this.isErrorLabelSeen = false;
				var rand = Math.floor( Math.random() * 9998 ) + 1 ;
				var script = document.createElement('script');
				script.id = "beforeSubmit";
				script.src = './js/beforesubmit.php?rand='+rand;
				script.type = "text/javascript";
				beforeSubmit = undefined;
				if(script.addEventListener){
					script.addEventListener("load", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.userForm.submit();
						}
					}, false);
				}else if(script.attachEvent){
					script.attachEvent("onload", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.uuserForm.submit();
						}
					});
					script.onreadystatechange = function(){
						if(this.readyState=="loaded"||this.readyState=="complete"){
							if(typeof beforeSubmit == 'function'){
								beforeSubmit();
								document.userForm.submit();
							}
						}
					}
				}
				document.userForm.appendChild(script);
			})
			.catch(error=>{
				this.isErrorLabelSeen = true;
				this.errorMessage = error.response.data.error.detail + "<br>" + validate_translation_config[error.response.data.error.detail];
				window.scroll(0,0);
			});
		}
	},
	created:function(){
		this.fetchMonitor();
		this.fetchDoutIn();
		this.fetchDoutOut();
		this.fetchResponse();
	},
	computed: {

	}
});

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById('btn_do_update') != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>