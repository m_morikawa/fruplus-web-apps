<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'SystemParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::RESET); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF129;"></span><strong>リセット</strong></h2>
				<div id="disconnect" style="padding-top:12px;">
					<section>
						<h3>通信切断</h3>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								デバイスのモードがモード1/2/3の場合は、サーバとの通信を一時的に切断します。その後、サーバへ送信するデータが発生した場合は再度接続を試みます。<br/>
								デバイスのモードがマニュアルモードの場合は、接続中のインターフェイスとの切断を行い、デバイスをクローズします。再度制御を行うためには、'OpenReader'コマンドが必要になります。
							</div>
						</div>
					</section>
					<section style="padding-top:12px;">
						<button class="primary" id="btn_disconnect">通信切断する</button>
						<?php
echo <<< EOF
<script>
function onDisconnect(){
var elem = document.getElementById('reset_disconnectscript');
if(elem){
document.getElementById('disconnect').removeChild(elem);
}
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
script.src = './js/reset.php?resettype=disconnect&rand=' + rand;
script.id = 'reset_disconnectscript';
document.getElementById('disconnect').appendChild(script);
document.getElementById('btn_disconnect').style.disabled = true;
document.getElementById('btn_disconnect').className = "feedback";
document.getElementById('btn_disconnect').innerHTML = "通信切断しました";
setTimeout(function(){
document.getElementById('btn_disconnect').style.disabled = false;
document.getElementById('btn_disconnect').className = "primary";
document.getElementById('btn_disconnect').innerHTML = "通信切断する";
}, 3000)
}
if(document.getElementById('btn_disconnect').addEventListener){
document.getElementById('btn_disconnect').addEventListener("click", onDisconnect, false);
}else if(document.getElementById('btn_disconnect').attachEvent){
document.getElementById('btn_disconnect').attachEvent("onclick", onDisconnect);
}else{
document.getElementById('btn_disconnect').onclick = onDisconnect;
}
</script>
EOF;
						?>
					</section>
				</div>
				<div id="appreset" style="padding-top:36px;">
					<section>
						<h3>内部アプリケーション再起動</h3>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								デバイスで動作中のアプリケーションを中止し、再度立ち上げます。その際に動作するモードは、最後に起動していたモードです。
							</div>
						</div>
					</section>
					<section style="padding-top:12px;">
						<button class="primary" id="btn_appreset">再起動する</button>
						<?php
echo <<< EOF
<script>
function onAppReset(){
var elem = document.getElementById('reset_appresetscript');
if(elem){
document.getElementById('appreset').removeChild(elem);
}
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
script.src = './js/reset.php?resettype=appreset&rand=' + rand;
script.id = 'reset_appresetscript';
document.getElementById('appreset').appendChild(script);
document.getElementById('btn_appreset').style.disabled = true;
document.getElementById('btn_appreset').className = "feedback";
document.getElementById('btn_appreset').innerHTML = "再起動しました";
setTimeout(function(){
document.getElementById('btn_appreset').style.disabled = false;
document.getElementById('btn_appreset').className = "primary";
document.getElementById('btn_appreset').innerHTML = "再起動する";
}, 3000)
}
if(document.getElementById('btn_appreset').addEventListener){
document.getElementById('btn_appreset').addEventListener("click", onAppReset, false);
}else if(document.getElementById('btn_appreset').attachEvent){
document.getElementById('btn_appreset').attachEvent("onclick", onAppReset);
}else{
document.getElementById('btn_appreset').onclick = onAppReset;
}
</script>
EOF;
						?>
					</section>
				</div>
				<div id="softreset" style="padding-top:36px;">
					<section>
						<h3>ソフトウェアリセット</h3>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								デバイスを安全にリブートします。
							</div>
						</div>
					</section>
					<section style="padding-top:12px;">
						<button class="primary" id="btn_softreset">リセットする</button>
					</section>
					<?php
echo <<< EOF
<script>
function onSoftResetClicked(){
var elem = document.getElementById('reset_softresetscript');
if(elem){
document.getElementById('softreset').removeChild(elem);
}
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
script.src = './js/reset.php?resettype=softreset&rand=' + rand;
script.id = 'reset_softresetscript';
document.getElementById('softreset').appendChild(script);
document.getElementById('btn_softreset').style.disabled = true;
document.getElementById('btn_softreset').className = "feedback";
document.getElementById('btn_softreset').innerHTML = "リセットしました";
setTimeout(function(){
document.getElementById('btn_softreset').style.disabled = false;
document.getElementById('btn_softreset').className = "primary";
document.getElementById('btn_softreset').innerHTML = "リセットする";
}, 3000)
}
if(document.getElementById('btn_softreset').addEventListener){
document.getElementById('btn_softreset').addEventListener("click", onSoftResetClicked, false);
}else if(document.getElementById('btn_softreset').attachEvent){
document.getElementById('btn_softreset').attachEvent("onclick", onSoftResetClicked);
}else{
document.getElementById('btn_softreset').onclick = onSoftResetClicked;
}
</script>
EOF;
					?>
				</div>
				<div id="modeconfreset" style="padding-top:36px;">
					<section>
						<h3>動作モード設定初期化</h3>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								動作モード設定(送信先、フィルター、モード)を初期化します。<Br/>
								初期化処理終了後、初期値を反映した状態で動作モードが再起動します。
							</div>
						</div>
					</section>
					<section style="padding-top:12px;">
						<button class="primary" id="btn_modeconfreset">動作モード設定を初期化する</button>
					</section>
					<?php
echo <<< EOF
<script>
function onModeConfResetClicked(){
var elem = document.getElementById('reset_modeconfresetscript');
if(elem){
document.getElementById('modeconfreset').removeChild(elem);
}
if(window.confirm('動作モード設定の初期化を行います。よろしいですか？')){
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
script.src = './js/reset.php?resettype=modeconfreset&rand=' + rand;
script.id = 'reset_modeconfresetscript';
document.getElementById('modeconfreset').appendChild(script);

window.alert('動作モード設定の初期化を行いました。');
document.getElementById('btn_modeconfreset').style.disabled = true;
document.getElementById('btn_modeconfreset').className = "feedback";
document.getElementById('btn_modeconfreset').innerHTML = "動作モード設定を初期化しました";
setTimeout(function(){
document.getElementById('btn_modeconfreset').style.disabled = false;
document.getElementById('btn_modeconfreset').className = "primary";
document.getElementById('btn_modeconfreset').innerHTML = "動作モード設定を初期化する";
}, 3000);
}else{
window.alert('動作モード設定の初期化をキャンセルしました。');
}
}
if(document.getElementById('btn_modeconfreset').addEventListener){
document.getElementById('btn_modeconfreset').addEventListener("click", onModeConfResetClicked, false);
}else if(document.getElementById('btn_modeconfreset').attachEvent){
document.getElementById('btn_modeconfreset').attachEvent("onclick", onModeConfResetClicked);
}else{
document.getElementById('btn_modeconfreset').onclick = onModeConfResetClicked;
}
</script>
EOF;
					?>
				</div>
				<div id="softresetandinit" style="padding-top:36px;">
					<section>
						<h3>端末初期化</h3>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								<strong>リーダー設定を除く</strong>デバイスの設定を工場出荷時の状態に戻します。<Br/>
								初期化処理終了後、リーダーは自動的に再起動します。<Br/>
								再起動後のリーダーのIPアドレスは192.168.209.10です。
							</div>
						</div>
					</section>
					<section style="padding-top:12px;">
						<button class="primary" id="btn_softresetandinit">端末初期化する</button>
					</section>
					<?php
echo <<< EOF
<script>
function onSoftResetAndInitClicked(){
var elem = document.getElementById('reset_softresetandinitscript');
if(elem){
document.getElementById('softresetandinit').removeChild(elem);
}
if(window.confirm('端末初期化を行います。よろしいですか？')){
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
script.src = './js/reset.php?resettype=softresetandinit&rand=' + rand;
script.id = 'reset_softresetandinitscript';
document.getElementById('softresetandinit').appendChild(script);

window.alert('端末初期化を行っています...\\n初期化後の設定ページはhttp://192.168.209.10/index.phpになります。');
document.getElementById('btn_softresetandinit').style.disabled = true;
document.getElementById('btn_softresetandinit').className = "feedback";
document.getElementById('btn_softresetandinit').innerHTML = "端末初期化しました";
setTimeout(function(){
document.getElementById('btn_softresetandinit').style.disabled = false;
document.getElementById('btn_softresetandinit').className = "primary";
document.getElementById('btn_softresetandinit').innerHTML = "端末初期化する";
}, 3000);
}else{
window.alert('端末初期化をキャンセルしました。');
}
}
if(document.getElementById('btn_softresetandinit').addEventListener){
document.getElementById('btn_softresetandinit').addEventListener("click", onSoftResetAndInitClicked, false);
}else if(document.getElementById('btn_softresetandinit').attachEvent){
document.getElementById('btn_softresetandinit').attachEvent("onclick", onSoftResetAndInitClicked);
}else{
document.getElementById('btn_softresetandinit').onclick = onSoftResetAndInitClicked;
}
</script>
EOF;
					?>
				</div>
	    </div>
		</div>
  </div>
</div>
</body>
</html>
