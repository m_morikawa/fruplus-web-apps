<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'RwConfigParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$parameter = new RwConfigParameter();
	$dat = $parameter->dat();

	if(isset($_POST['inventory_submit'])){
		function validateInventory(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'inventory_antennacycle')){
				$msg[] = "アンテナサイクルを入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'inventory_antennacycle', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>65534)))){
				$msg[] = "アンテナ1のInvCntは1から65534の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'inventory_qalgorithm')){
				$msg[] = "Q Algorithmを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_qalgorithm'] !== "0" && $_POST['inventory_qalgorithm'] !== "1"){
				$msg[] = "不正なQ Algorithmが入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'inventory_matchrep')){
				$msg[] = "MATCH REPを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_matchrep'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_matchrep', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
					$msg[] = "MATCH REPは0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'inventory_startq')){
				$msg[] = "Start Qを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_startq'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_startq', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
					$msg[] = "Start Qは0から15の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'inventory_maximumq')){
				$msg[] = "Maximum Qを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_maximumq'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_maximumq', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
					$msg[] = "Maximum Qは0から15の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'inventory_minimumq')){
				$msg[] = "Minimum Qを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_minimumq'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_minimumq', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
					$msg[] = "Minimum Qは0から15の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if($_POST['inventory_maximumq'] < $_POST['inventory_minimumq']){
				$msg[] = "Maximum Q はMinimum Q よりも小さい値に設定できません。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'inventory_thresholdmultipler')){
				$msg[] = "Threshold Multiplerを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_thresholdmultipler'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_thresholdmultipler', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
					$msg[] = "Threshold Multiplerは0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'inventory_queryretrycount')){
				$msg[] = "Query Retry Countを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_queryretrycount'] !== "0"){
				if(!filter_input(INPUT_POST, 'inventory_queryretrycount', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
					$msg[] = "Query Retry Countは0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'inventory_runtillzero')){
				$msg[] = "Run_Till_Zeroの状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['inventory_runtillzero'] !== "0" && $_POST['inventory_runtillzero'] !== "1"){
				$msg[] = "不正なRun_Till_Zeroの状態が入力されています。";
				$ret = FALSE;
			}
			return $ret;
		}

		$msg = array();
		if(!validateInventory($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			function valueToArray($value,&$first,&$second){
				$first = $value % 256;
				$second = ($value - $first) / 256;
			}

			valueToArray($_POST['inventory_antennacycle'],$dat[0x3C + 1],$dat[0x3D + 1]);
			$dat[0x3E + 1] = $_POST['inventory_qalgorithm'];
			$dat[0x3F + 1] = $_POST['inventory_matchrep'];
			$dat[0x46 + 1] = $_POST['inventory_startq'];
			$dat[0x47 + 1] = $_POST['inventory_maximumq'];
			$dat[0x48 + 1] = $_POST['inventory_minimumq'];
			$dat[0x49 + 1] = $_POST['inventory_thresholdmultipler'];
			$dat[0x4A + 1] = $_POST['inventory_queryretrycount'];
			$dat[0x4B + 1] = $_POST['inventory_runtillzero'];
			if(!$parameter->save($dat)){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] .= "インベントリ設定の保存に失敗しました。";
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<?php
if(!isset($_POST['inventory_submit'])){
echo <<< EOF
<script>
window.alert("インベントリ設定はリーダーの読み取り性能に関わる設定です。不必要に変更しないでください。");
</script>
EOF;
}
?>
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}
(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::INVENTORY); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>マニュアルモード設定 > インベントリ</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary" id="updateform">
<section>
<strong>インベントリ設定を変更しました。</strong>
この設定をリーダーに反映するには、以下の手順が必要です。<Br>
<div style="max-width:60em;padding-top:1em;">
<ol style="list-style-type:decimal">
<li style="width:100%;"><strong>設定を反映する</strong>ボタンをクリックします。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
<li style="width:100%;">リーダーのLED A(緑)とB(赤)が点滅後消灯し、LED A(緑)が点灯します。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
</ol>
</div>
</section>

<section>
<div style="max-width:60em;padding-top:1em;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
var elem = document.getElementById('update_inventory');
if(elem){
document.getElementsByTagName('body')[0].removeChild(elem);
}
var script = document.createElement('script');
script.src = './js/updaterwcfg.php';
script.id = 'update_inventory';
document.getElementsByTagName('body')[0].appendChild(script);
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').className = "feedback";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('btn_do_update').style.disabled = false;
document.getElementById('btn_do_update').className = "primary";
document.getElementById('btn_do_update').innerHTML = "設定を反映する";
document.getElementById('btn_do_update').style.display="none";
document.getElementById('btn_cancel_update').style.display="none";
}, 3000)
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</section>

</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="inventory.php" class="forms">
					<section>
						<label>アンテナサイクル</label>
						<?php
						$antennaCycle = hexdec(sprintf("%02X", $dat[0x3D + 1]).sprintf("%02X", $dat[0x3C + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_antennacycle" value="{$antennaCycle}" placeholder="{$antennaCycle}">
EOF;
						?>
					</section>
					<section>
						<label>Q Algorithm</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="inventory_qalgorithm">
								<?php
									if ( $dat[0x3E + 1] == 0){
										echo "<option value=0 selected>Fixed Q</option><option value=1>Dynamic Q</option>";
									} else if ( $dat[0x3E + 1] == 1){
										echo "<option value=0>Fixed Q</option><option value=1 selected>Dynamic Q</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>MATCH REP</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_matchrep" value="{$dat[0x3F + 1]}" placeholder="{$dat[0x3F + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Start Q</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_startq" value="{$dat[0x46 + 1]}" placeholder="{$dat[0x46 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Maximum Q</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_maximumq" value="{$dat[0x47 + 1]}" placeholder="{$dat[0x47 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Minimum Q</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_minimumq" value="{$dat[0x48 + 1]}" placeholder="{$dat[0x48 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Threshold Multipler</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_thresholdmultipler" value="{$dat[0x49 + 1]}" placeholder="{$dat[0x49 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Query Retry Count</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_queryretrycount" value="{$dat[0x4A + 1]}" placeholder="{$dat[0x4A + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>Run_Till_Zero</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="inventory_runtillzero">
								<?php
									if ( $dat[0x4B + 1] == 0){
										echo "<option value=0 selected>無効</option><option value=1>有効</option>";
									} else if ( $dat[0x4B + 1] == 1){
										echo "<option value=0>無効</option><option value=1 selected>有効</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<button class="primary" name="inventory_submit">変更する</button>
						<button class="secondary" type="button" name="inventory_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="inventory_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		echo "document.getElementsByName(\"inventory_antennacycle\")[0].value = \"".$antennaCycle."\";";
		if ( $dat[0x3E + 1] == 0){
			echo "document.getElementsByName(\"inventory_qalgorithm\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x3E + 1] == 1){
			echo "document.getElementsByName(\"inventory_qalgorithm\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"inventory_matchrep\")[0].value = \"".$dat[0x3F + 1]."\";";
		echo "document.getElementsByName(\"inventory_startq\")[0].value = \"".$dat[0x46 + 1]."\";";
		echo "document.getElementsByName(\"inventory_maximumq\")[0].value = \"".$dat[0x47 + 1]."\";";
		echo "document.getElementsByName(\"inventory_minimumq\")[0].value = \"".$dat[0x48 + 1]."\";";
		echo "document.getElementsByName(\"inventory_thresholdmultipler\")[0].value = \"".$dat[0x49 + 1]."\";";
		echo "document.getElementsByName(\"inventory_queryretrycount\")[0].value = \"".$dat[0x4A + 1]."\";";
		if ( $dat[0x4B + 1] == 0){
			echo "document.getElementsByName(\"inventory_runtillzero\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x4B + 1] == 1){
			echo "document.getElementsByName(\"inventory_runtillzero\")[0].selectedIndex= \"1\";";
		}
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("inventory_antennacycle")[0].value= "1";
		document.getElementsByName("inventory_qalgorithm")[0].selectedIndex = "1";
		document.getElementsByName("inventory_matchrep")[0].value = "0";
		document.getElementsByName("inventory_startq")[0].value = "4";
		document.getElementsByName("inventory_maximumq")[0].value = "15";
		document.getElementsByName("inventory_minimumq")[0].value = "0";
		document.getElementsByName("inventory_thresholdmultipler")[0].value = "4";
		document.getElementsByName("inventory_queryretrycount")[0].value = "0";
		document.getElementsByName("inventory_runtillzero")[0].selectedIndex = "0";
	}
</script>
</body>
</html>
