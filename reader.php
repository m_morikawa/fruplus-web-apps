<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'RwConfigParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$parameter = new RwConfigParameter();
	$dat = $parameter->dat();

	$model = new Model();
	$deffreqch = $model->defaultFreqCh();
	$optionsToIndex = $model->chToIndex();
	$options = $model->channelStrings();

	if(isset($_POST['reader_submit'])){
		function validateReader(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'reader_baudrate')){
				$msg[] = "ボーレートを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['reader_baudrate'] !== "0"){
				if(!filter_input(INPUT_POST, 'reader_baudrate', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>5)))){
					$msg[] = "ボーレートは0から5の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'reader_bcccheck')){
				$msg[] = "BCCチェックを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['reader_bcccheck'] !== "0" && $_POST['reader_bcccheck'] !== "1"){
				$msg[] = "不正なBCCチェックが入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'reader_verifywait')){
				$msg[] = "ベリファイウェイトを入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'reader_verifywait', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>255)))){
				$msg[] = "ベリファイウェイトは1から255の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'reader_dio')){
				$msg[] = "DIOを入力して下さい。";
				$ret = FALSE;
			}
			if((strlen($_POST['reader_dio']) != 2) || (!ctype_xdigit($_POST['reader_dio']))){
				$msg[] = "DIOは1byteの16進数で入力して下さい。";
				$ret = FALSE;
			}
			require_once("function/check_dio.php");
			if(!filter_var($_POST['reader_dio'], FILTER_CALLBACK, array('options' => 'check_dio'))){
				$msg[] = "不正なDIOのフォーマットが入力されました。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'reader_freqch')){
				$msg[] = "周波数チャンネルを入力して下さい。";
				$ret = FALSE;
			}
			$ch = 0;
			if($_POST['reader_freqch'] !== "0"){
				$ch = filter_input(INPUT_POST, 'reader_freqch', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)));
				if(!$ch){
					$msg[] = "周波数チャンネルは0から255の整数で入力して下さい。";
					$ret = FALSE;
				}
				require_once("function/check_ch.php");
				if(!filter_var($ch, FILTER_CALLBACK, array('options' => 'check_ch'))){
					$msg[] = "不正な周波数チャンネルのフォーマットが入力されました。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateReader($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$dat[0x0A + 1] = $_POST['reader_baudrate'];
			$dat[0x0B + 1] = $_POST['reader_bcccheck'];
			$dat[0x0C + 1] = $_POST['reader_verifywait'];
			$dat[0x14 + 1] = hexdec($_POST['reader_dio']);
			$dat[0x1E + 1] = $_POST['reader_freqch'];
			if(!$parameter->save($dat)){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] .= "リーダー設定の保存に失敗しました。";
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::READER); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>マニュアルモード設定 > リーダー</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary" id="updateform">
<section>
<strong>リーダー設定を変更しました。</strong><Br>
この設定をリーダーに反映するには、以下の手順が必要です。
<div style="max-width:60em;padding-top:1em;">
<ol style="list-style-type:decimal">
<li style="width:100%;"><strong>設定を反映する</strong>ボタンをクリックします。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
<li style="width:100%;">リーダーのLED A(緑)とB(赤)が点滅後消灯し、LED A(緑)が点灯します。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
</ol>
</div>
</section>

<section>
<div style="max-width:60em;padding-top:1em;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
var elem = document.getElementById('update_reader');
if(elem){
document.getElementsByTagName('body')[0].removeChild(elem);
}
var script = document.createElement('script');
script.src = './js/updaterwcfg.php';
script.id = 'update_reader';
document.getElementsByTagName('body')[0].appendChild(script);
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').className = "feedback";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('btn_do_update').style.disabled = false;
document.getElementById('btn_do_update').className = "primary";
document.getElementById('btn_do_update').innerHTML = "設定を反映する";
document.getElementById('btn_do_update').style.display="none";
document.getElementById('btn_cancel_update').style.display="none";
}, 3000)
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</section>

</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="reader.php" class="forms">
					<section>
						<label>ボーレート</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="reader_baudrate">
								<?php
									if ( $dat[0x0A + 1] == 0){
										echo "<option value=0 selected>2400 bps</option><option value=1>4800 bps</option><option value=2>9600 bps</option><option value=3>19200 bps</option><option value=4>38400 bps</option><option value=5>115200 bps</option>";
									} else if ( $dat[0x0A + 1] == 1){
										echo "<option value=0>2400 bps</option><option value=1 selected>4800 bps</option><option value=2>9600 bps</option><option value=3>19200 bps</option><option value=4>38400 bps</option><option value=5>115200 bps</option>";
									} else if( $dat[0x0A + 1] == 2){
										echo "<option value=0>2400 bps</option><option value=1>4800 bps</option><option value=2 selected>9600 bps</option><option value=3>19200 bps</option><option value=4>38400 bps</option><option value=5>115200 bps</option>";
									} else if( $dat[0x0A + 1] == 3){
										echo "<option value=0>2400 bps</option><option value=1>4800 bps</option><option value=2>9600 bps</option><option value=3 selected>19200 bps</option><option value=4>38400 bps</option><option value=5>115200 bps</option>";
									} else if( $dat[0x0A + 1] == 4){
										echo "<option value=0>2400 bps</option><option value=1>4800 bps</option><option value=2>9600 bps</option><option value=3>19200 bps</option><option value=4 selected>38400 bps</option><option value=5>115200 bps</option>";
									} else if( $dat[0x0A + 1] == 5){
										echo "<option value=0>2400 bps</option><option value=1>4800 bps</option><option value=2>9600 bps</option><option value=3>19200 bps</option><option value=4>38400 bps</option><option value=5 selected>115200 bps</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>BCCチェック</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="reader_bcccheck">
								<?php
									if ( $dat[0x0B + 1] == 0){
										echo "<option value=0 selected>有効</option><option value=1>無効</option>";
									} else if ( $dat[0x0B + 1] == 1){
										echo "<option value=0>有効</option><option value=1 selected>無効</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>ベリファイウエイト時間 : 100ms</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="reader_verifywait" value="{$dat[0x0C + 1]}" placeholder="{$dat[0x0C + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>外部入出力 : hex</label>
						<?php
						$dio = sprintf("%02X", $dat[0x14 + 1]);
echo <<< EOF
<input type="text" class="width-4 custom" name="reader_dio" value="{$dio}" placeholder="{$dio}">
EOF;
						?>
					</section>
					<section>
						<label>周波数チャンネル</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="reader_freqch">
								<?php 
									foreach ($options as $key=>$value) {
									  $selected = "";
									  if ($key == $dat[0x1E + 1]) {
										$selected = " selected";
									  }
									  echo "<option value=\"$key\"$selected>$value</option>\n";
									}
								?>
								<select>
							</column>
						</row>
					</section>
					<section>
						<button class="primary" name="reader_submit">変更する</button>
						<button class="secondary" type="button" name="reader_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="reader_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
<script>
function onToCurrentClicked(){
	<?php
		if ( $dat[0x0A + 1] == 0){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x0A + 1] == 1){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"1\";";
		} else if( $dat[0x0A + 1] == 2){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"2\";";
		} else if( $dat[0x0A + 1] == 3){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"3\";";
		} else if( $dat[0x0A + 1] == 4){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"4\";";
		} else if( $dat[0x0A + 1] == 5){
			echo "document.getElementsByName(\"reader_baudrate\")[0].selectedIndex= \"5\";";
		}
		if ( $dat[0x0B + 1] == 0){
			echo "document.getElementsByName(\"reader_bcccheck\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x0B + 1] == 1){
			echo "document.getElementsByName(\"reader_bcccheck\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"reader_verifywait\")[0].value = \"".$dat[0x0C + 1]."\";";
		echo "document.getElementsByName(\"reader_dio\")[0].value = \"".$dio."\";";
		echo "document.getElementsByName(\"reader_freqch\")[0].selectedIndex= \"".$optionsToIndex[$dat[0x1E + 1]]."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("reader_baudrate")[0].selectedIndex= "5";
		document.getElementsByName("reader_bcccheck")[0].selectedIndex = "0";
		document.getElementsByName("reader_verifywait")[0].value = "255";
		document.getElementsByName("reader_dio")[0].value = "00";
	<?php
		echo "document.getElementsByName(\"reader_freqch\")[0].selectedIndex = \"".$optionsToIndex[$deffreqch]."\";";
	?>
	}
</script>
</body>
</html>
