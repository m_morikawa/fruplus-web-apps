<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('sendTo');
	list($langFlag,$validate_translation) = loadTranslation('sendTo_validate');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;

	if(isset($_POST['userForm_submit'])){
		$isUpdateFormSeen = 1;
	}

	$sendTo = json_decode(json_encode($config->xml()->autoMode->sendTo),TRUE);
?>
<!DOCTYPE html>
<html lang="<?php echo $langFlag;?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>

<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<span  style="float: right;">
		<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
		<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
			<option v-for="language in languages" v-bind:value="language.value">
				{{language.name}}
			</option>
		</select>
	</span>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::TRANSFER); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo COMMON_UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>	
				<form method="post" action="sendto.php" class="forms" name="userForm" @submit.prevent="onSubmit">
					<fieldset>
						<legend><h5><?php echo HEADER_SENDTO; ?></h5></legend>
						<section>
							<label><?php echo ITEM_PROTOCOL; ?></label>
							<row>
								<column cols="6">
									<select v-model="protocol" @change="fetchProtocol" class="select custom" class="width-10">
										<option v-for="protocol in protocols" v-bind:value="protocol.name">
											{{protocol.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="socketconfig" v-show="isSocketConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_IPHOST; ?></label>
									<input type="text" class="width-6 custom" v-model="socket_host" >
								</section>
								<section>
									<label><?php echo ITEM_PORT; ?></label>
									<input type="text" class="width-4 custom" v-model="socket_port">
								</section>
							</div>
							<div id="httpconfig" v-show="isHTTPConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_METHOD; ?></label>
									<row>
										<column cols="6">
											<select v-model="http_method" class="select custom" class="width-10">
												<option v-for="method in methods" v-bind:value="method.name">
													{{method.name}}
												</option>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label><?php echo ITEM_IPHOST; ?></label>
									<input type="text" class="width-6 custom" v-model="http_host" >
								</section>
								<section>
									<label><?php echo ITEM_PATH; ?></label>
									<input type="text" class="width-6 custom" v-model="http_path" >
								</section>
								<section>
									<label><?php echo ITEM_PORT; ?></label>
									<input type="text" class="width-6 custom" v-model="http_port" >
								</section>
								<section>
									<label><?php echo ITEM_FORMAT; ?></label>
									<row>
										<column cols="6">
											<select v-model="http_format" class="select custom" class="width-10">
												<option v-for="format in formats" v-bind:value="format.name">
													{{format.name}}
												</option>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label><?php echo ITEM_BASICUSER; ?></label>
									<input type="text" class="width-6 custom" v-model="http_authUser" >
								</section>
								<section>
									<label><?php echo ITEM_BASICPASS; ?></label>
									<input type="text" class="width-6 custom" v-model="http_authPass" >
								</section>
							</div>
						</section>
					</fieldset>
					<section>
						<button class="primary"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button" v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button"><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
  	</div>
</div>

<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var validate_translation_config = JSON.parse('<?php echo json_encode($validate_translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($sendTo,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"ja"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		protocols:[
			{name:'serial'},
			{name:'socket'},
			{name:'http'},
			{name:'https'},
		],
		methods:[
			{name:'post'},
			{name:'get'},
		],
		formats:[
			{name:'query'},
			{name:'json'},
		],
		protocol:current_config["protocol"],
		socket_host:current_config["socket"]["host"],
		socket_port:current_config["socket"]["port"],
		http_method:current_config["http"]["method"],
		http_host:current_config["http"]["host"],
		http_path:current_config["http"]["path"],
		http_port:current_config["http"]["port"],
		http_format:current_config["http"]["format"],
		http_authUser:current_config["http"]["authUser"],
		http_authPass:current_config["http"]["authPass"],
		isSocketConfigSeen:false,
		isHTTPConfigSeen:false,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:0,
		errorMessage:"",
	},
	methods: {
		fetchProtocol:function(){
			this.isSocketConfigSeen = (this.protocol=="socket");
			this.isHTTPConfigSeen = (this.protocol=="http" || this.protocol=="https");
		},
		toCurrent:function(){
			this.protocol = current_config["protocol"];
			this.socket_host = current_config["socket"]["host"];
			this.socket_port = current_config["socket"]["port"];
			this.http_method = current_config["http"]["method"];
			this.http_host = current_config["http"]["host"];
			this.http_path = current_config["http"]["path"];
			this.http_port = current_config["http"]["port"];
			this.http_format = current_config["http"]["format"];
			this.http_authUser = current_config["http"]["authUser"];
			this.http_authPass = current_config["http"]["authPass"];
			this.fetchProtocol();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		onSubmit: function () {
			axios.post('./api/v1/commands.php', {
				"name":"setSendTo",
				"parameters" : {
					"sendTo" : {
						"protocol" : this.protocol,
						"socket" : {
							"host" : this.socket_host,
							"port" : this.socket_port 
						},
						"http" : {
							"host" : this.http_host,
							"path" : this.http_path,
							"port" : this.http_port,
							"method" : this.http_method,
							"format" : this.http_format,
							"authUser" : this.http_authUser,
							"authPass" : this.http_authPass
						}
					}
				}
			})
			.then(response=>{
				this.isErrorLabelSeen = false;
				var rand = Math.floor( Math.random() * 9998 ) + 1 ;
				var script = document.createElement('script');
				script.id = "beforeSubmit";
				script.src = './js/beforesubmit.php?rand='+rand;
				script.type = "text/javascript";
				beforeSubmit = undefined;
				if(script.addEventListener){
					script.addEventListener("load", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.userForm.submit();
						}
					}, false);
				}else if(script.attachEvent){
					script.attachEvent("onload", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.uuserForm.submit();
						}
					});
					script.onreadystatechange = function(){
						if(this.readyState=="loaded"||this.readyState=="complete"){
							if(typeof beforeSubmit == 'function'){
								beforeSubmit();
								document.userForm.submit();
							}
						}
					}
				}
				document.userForm.appendChild(script);
			})
			.catch(error=>{
				this.isErrorLabelSeen = true;
				this.errorMessage = error.response.data.error.detail + "<br>" + validate_translation_config[error.response.data.error.detail];
				window.scroll(0,0);
			});
		}
	},
	created:function(){
		this.fetchProtocol();
	},
	computed: {

	}
});

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById('btn_do_update') != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>
