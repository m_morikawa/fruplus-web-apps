FORMAT: 1A
 
# Requests API
FRU-4025Plus/FRU-4100Plus(以下FRU-4xxxPlus)はHTTP接続で当APIにより、動作モードの切り替えや設定を実行できます。
 
# オートモード設定
FRU-4xxxPlusのオートモードのパラメータを取得または設定します。
 
## パラメータ取得/設定 [/api/v1/auto]
 
### Retrieve a Message [GET]
In API Blueprint, _requests_ can hold exactly the same kind of information and
can be described using exactly the same structure as _responses_, only with
different signature – using the `Request` keyword. The string that follows
after the `Request` keyword is a request identifier. Again, using explanatory
and simple naming is the best way to go.
 
+ Request Plain Text Message
    + Headers
            Accept: text/plain
 
+ Response 200 (text/plain)
    + Headers
            X-My-Message-Header: 42
    + Body
            Hello World!
 
+ Request JSON Message
    + Headers
            Accept: application/json
 
+ Response 200 (application/json)
    + Headers
            X-My-Message-Header: 42
    + Body
            { "message": "Hello World!" }
 
### Update a Message [PUT]
 
+ Request Update Plain Text Message (text/plain)
        All your base are belong to us.
 
+ Request Update JSON Message (application/json)
        { "message": "All your base are belong to us." }
 
+ Response 204