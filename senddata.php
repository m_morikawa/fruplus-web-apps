<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('sendData');
	list($langFlag,$validate_translation) = loadTranslation('sendData_validate');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;

	if(isset($_POST['userForm_submit'])){
		$isUpdateFormSeen = 1;
	}

	$sendData = json_decode(json_encode($config->xml()->autoMode->sendData),TRUE);
?>
<!DOCTYPE html>
<html lang="<?php echo $langFlag;?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>

<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<span  style="float: right;">
		<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
		<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
			<option v-for="language in languages" v-bind:value="language.value">
				{{language.name}}
			</option>
		</select>
	</span>
  </div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::SENDDATA); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo COMMON_UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>
				<form method="post" action="senddata.php" class="forms" name="userForm" @submit.prevent="onSubmit">
					<fieldset>
						<legend><h5><?php echo HEADER_TAGINFO; ?></h5></legend>
						<section>
							<section>
								<label><?php echo ITEM_TAGINFO_FORMAT; ?></label>
								<row>
									<column cols="6">
										<select v-model="tagInfo_format" @change="fetchFormat" class="select custom" class="width-10">
											<option v-for="format in formats" v-bind:value="format.value">
												{{format.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<div v-show="isTagInfoConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_TAGINFO_OFFSET; ?></label>
									<input type="text" class="width-6 custom" v-model="tagInfo_offset" >
								</section>
								<section>
									<label><?php echo ITEM_TAGINFO_LENGTH; ?></label>
									<input type="text" class="width-4 custom" v-model="tagInfo_length">
								</section>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_ADDINFO; ?></h5></legend>
						<section>
							<section>
								<label><?php echo ITEM_ADDINFO_ID; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_id" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_TIME; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_time" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_ANTENNA; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_antenna" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_PC; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_pc" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_RSSI; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_rssi" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_STATE; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_state" class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label><?php echo ITEM_ADDINFO_MEMORY; ?></label>
								<row>
									<column cols="6">
										<select v-model="addInfo_memory" @change="fetchMemory"  class="select custom" class="width-10">
											<option v-for="option in addOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<div v-show="isAccessInfoConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ITEM_ADDINO_ACCESSINFO_PASSWORD; ?></label>
									<input type="text" class="width-6 custom" v-model="addInfo_accessInfo_password" >
								</section>
								<section>
									<label><?php echo ITEM_ADDINFO_ACCESSINFO_BANK; ?></label>
									<row>
										<column cols="6">
											<select v-model="addInfo_accessInfo_bank" class="select custom" class="width-10">
												<option v-for="option in bankOptions" v-bind:value="option.value">
													{{option.name}}
												</option>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label><?php echo ITEM_ADDINO_ACCESSINFO_OFFSET; ?></label>
									<input type="text" class="width-4 custom" v-model="addInfo_accessInfo_offset">
								</section>
								<section>
									<label><?php echo ITEM_ADDINO_ACCESSINFO_LENGTH; ?></label>
									<input type="text" class="width-4 custom" v-model="addInfo_accessInfo_length">
								</section>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_ERRORINFO; ?></h5></legend>
						<section>
							<label><?php echo ITEM_ERRORINFO_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="errorInfo_enable" class="select custom" class="width-10">
										<option v-for="option in sendOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_NOREADINFO; ?></h5></legend>
						<section>
							<label><?php echo ITEM_NOREADINFO_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="noReadInfo_enable" class="select custom" class="width-10">
										<option v-for="option in sendOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<section>
						<button class="primary"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button"  v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button" ><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var validate_translation_config = JSON.parse('<?php echo json_encode($validate_translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($sendData,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"ja"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		formats:[
			{name:translation_config["OPTION_ALL"],value:'all'},
			{name:translation_config["OPTION_PART"],value:'part'}
		],
		addOptions:[
			{name:translation_config["OPTION_ADD"],value:true},
			{name:translation_config["OPTION_NOTADD"],value:false},
		],
		bankOptions:[
			{name:translation_config["OPTION_RESERVED"],value:"reserved"},
			{name:translation_config["OPTION_EPC"],value:"epc"},
			{name:translation_config["OPTION_TID"],value:"tid"},
			{name:translation_config["OPTION_USER"],value:"user"}
		],
		sendOptions:[
			{name:translation_config["OPTION_SEND"],value:true},
			{name:translation_config["OPTION_NOTSEND"],value:false},
		],
		tagInfo_format:current_config["tagInfo"]["format"],
		tagInfo_offset:current_config["tagInfo"]["offset"],
		tagInfo_length:current_config["tagInfo"]["length"],
		addInfo_id:current_config["addInfo"]["id"] == "true",
		addInfo_time:current_config["addInfo"]["time"] == "true",
		addInfo_antenna:current_config["addInfo"]["antenna"] == "true",
		addInfo_pc:current_config["addInfo"]["pc"] == "true",
		addInfo_rssi:current_config["addInfo"]["rssi"] == "true",
		addInfo_state:current_config["addInfo"]["state"] == "true",
		addInfo_memory:current_config["addInfo"]["memory"] == "true",
		addInfo_accessInfo_password:current_config["addInfo"]["accessInfo"]["password"],
		addInfo_accessInfo_bank:current_config["addInfo"]["accessInfo"]["bank"],
		addInfo_accessInfo_offset:current_config["addInfo"]["accessInfo"]["offset"],
		addInfo_accessInfo_length:current_config["addInfo"]["accessInfo"]["length"],
		errorInfo_enable:current_config["errorInfo"]["enable"] == "true",
		noReadInfo_enable:current_config["noReadInfo"]["enable"] == "true",
		isTagInfoConfigSeen:false,
		isAccessInfoConfigSeen:false,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:0,
		errorMessage:"",
	},
	methods: {
		fetchFormat:function(){
			this.isTagInfoConfigSeen = (this.tagInfo_format=="part");
		},
		fetchMemory:function(){
			this.isAccessInfoConfigSeen= this.addInfo_memory;
		},
		toCurrent:function(){
			this.tagInfo_format = current_config["tagInfo"]["format"];
			this.tagInfo_offset = current_config["tagInfo"]["offset"];
			this.tagInfo_length = current_config["tagInfo"]["length"];
			this.addInfo_id = current_config["addInfo"]["id"] == "true";
			this.addInfo_time = current_config["addInfo"]["time"] == "true";
			this.addInfo_antenna = current_config["addInfo"]["antenna"] == "true";
			this.addInfo_pc = current_config["addInfo"]["pc"] == "true";
			this.addInfo_rssi = current_config["addInfo"]["rssi"] == "true";
			this.addInfo_state = current_config["addInfo"]["state"] == "true";
			this.addInfo_memory = current_config["addInfo"]["memory"] == "true";
			this.addInfo_accessInfo_password = current_config["addInfo"]["accessInfo"]["password"];
			this.addInfo_accessInfo_bank = current_config["addInfo"]["accessInfo"]["bank"];
			this.addInfo_accessInfo_offset = current_config["addInfo"]["accessInfo"]["offset"];
			this.addInfo_accessInfo_length = current_config["addInfo"]["accessInfo"]["length"];
			this.errorInfo_enable = current_config["errorInfo"]["enable"] == "true";
			this.noReadInfo_enable = current_config["errorInfo"]["enable"] == "true";
			this.fetchFormat();
			this.fetchMemory();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		onSubmit: function () {
			axios.post('./api/v1/commands.php', {
				"name":"setSendData",
				"parameters" : {
					"sendData": {
						"tagInfo" : {
							"format" : this.tagInfo_format,
							"offset" : this.tagInfo_offset,
							"length" : this.tagInfo_length,
						},
						"addInfo" : {
							"id" : this.addInfo_id,
							"time" : this.addInfo_time,
							"antenna" : this.addInfo_antenna,
							"pc" : this.addInfo_pc,
							"rssi" : this.addInfo_rssi,
							"state" : this.addInfo_state,
							"memory" : this.addInfo_memory,
							"accessInfo" : {
								"password" : this.addInfo_accessInfo_password,
								"bank" : this.addInfo_accessInfo_bank,
								"offset" : this.addInfo_accessInfo_offset,
								"length" : this.addInfo_accessInfo_length
							}
						},
						"errorInfo" : {
							"enable" : this.errorInfo_enable
						},
						"noReadInfo" : {
							"enable" : this.noReadInfo_enable
						}
					}
				}
			})
			.then(response=>{
				this.isErrorLabelSeen = false;
				var rand = Math.floor( Math.random() * 9998 ) + 1 ;
				var script = document.createElement('script');
				script.id = "beforeSubmit";
				script.src = './js/beforesubmit.php?rand='+rand;
				script.type = "text/javascript";
				beforeSubmit = undefined;
				if(script.addEventListener){
					script.addEventListener("load", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.userForm.submit();
						}
					}, false);
				}else if(script.attachEvent){
					script.attachEvent("onload", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.uuserForm.submit();
						}
					});
					script.onreadystatechange = function(){
						if(this.readyState=="loaded"||this.readyState=="complete"){
							if(typeof beforeSubmit == 'function'){
								beforeSubmit();
								document.userForm.submit();
							}
						}
					}
				}
				document.userForm.appendChild(script);
			})
			.catch(error=>{
				this.isErrorLabelSeen = true;
				this.errorMessage = error.response.data.error.detail + "<br>" + validate_translation_config[error.response.data.error.detail];
				window.scroll(0,0);
			});
		}
	},
	created:function(){
		this.fetchFormat();
		this.fetchMemory();
	},
	computed: {

	}
});

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById('btn_do_update') != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>
