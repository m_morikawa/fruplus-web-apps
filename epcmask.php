<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'RwConfigParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$parameter = new RwConfigParameter();
	$dat = $parameter->dat();
	$model = new Model();

	if(isset($_POST['epcmask_submit'])){
		function validateEpcMask(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'epcmask_enabled')){
				$msg[] = "EPC MASKの状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['epcmask_enabled'] !== "0" && $_POST['epcmask_enabled'] !== "1"){
				$msg[] = "不正なEPC MASKの状態が入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'epcmask_offset')){
				$msg[] = "EPC MASKの開始位置を入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'epcmask_length')){
				$msg[] = "EPC MASKのデータ長を入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'epcmask_data')){
				$msg[] = "EPC MASK データを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['epcmask_enabled'] === "1"){
				if($_POST['epcmask_offset'] !== "0"){
					if(!filter_input(INPUT_POST, 'epcmask_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>127)))){
						$msg[] = "EPC MASKの開始位置は0から127の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
				if(!filter_input(INPUT_POST, 'epcmask_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>128)))){
					$msg[] = "EPC MASKのデータ長は1から128の整数で入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['epcmask_data']) != 32 || (!ctype_xdigit($_POST['epcmask_data']))){
					$msg[] = "EPC MASK データは16byteの16進数で入力して下さい。";
					$ret = FALSE;
				}
				if($ret){
					unset($res);
					exec('sudo frucgi validate epcmask '.$_POST['epcmask_offset'].' '.$_POST['epcmask_length'].' '.$_POST['epcmask_data'],$res);
					$status = json_decode($res[0],true);
					if(strpos($status['status'],'success') === false){
						$msg[] = "EPC MASKのフォーマットが不正です。";
						$ret = FALSE;
					}
				}
			}else{
				if($_POST['epcmask_offset'] !== "0"){
					$msg[] = "EPC MASK無効時は開始位置に0をセットして下さい。";
					$ret = FALSE;
				}
				if($_POST['epcmask_length'] !== "0"){
					$msg[] = "EPC MASK無効時はデータ長に0をセットして下さい。";
					$ret = FALSE;
				}
				if($_POST['epcmask_data'] !== "00000000000000000000000000000000"){
					$msg[] = "EPC MASK無効時はデータはすべて0埋めして下さい。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateEpcMask($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$dat[0x50 + 1] = $_POST['epcmask_enabled'];
			$dat[0x51 + 1] = $_POST['epcmask_offset'];
			$dat[0x52 + 1] = $_POST['epcmask_length'];
			for($i = 0; $i < 16; $i++){
				$dat[0x53 + 1 + $i] = hexdec(substr($_POST['epcmask_data'],($i * 2),2));
			}
			if(!$parameter->save($dat)){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] .= "EPC MASK設定の保存に失敗しました。";
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::EPCMASK); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>マニュアルモード設定 > EPCマスク</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary" id="updateform">
<section>
<strong>EPCマスク設定を変更しました。</strong><Br>
この設定をリーダーに反映するには、以下の手順が必要です。
<div style="max-width:60em;padding-top:1em;">
<ol style="list-style-type:decimal">
<li style="width:100%;"><strong>設定を反映する</strong>ボタンをクリックします。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
<li style="width:100%;">リーダーのLED A(緑)とB(赤)が点滅後消灯し、LED A(緑)が点灯します。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
</ol>
</div>
</section>

<section>
<div style="max-width:60em;padding-top:1em;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>

<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
var elem = document.getElementById('update_epcmask');
if(elem){
document.getElementsByTagName('body')[0].removeChild(elem);
}
var script = document.createElement('script');
script.src = './js/updaterwcfg.php';
script.id = 'update_epcmask';
document.getElementsByTagName('body')[0].appendChild(script);
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').className = "feedback";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('btn_do_update').style.disabled = false;
document.getElementById('btn_do_update').className = "primary";
document.getElementById('btn_do_update').innerHTML = "設定を反映する";
document.getElementById('btn_do_update').style.display="none";
document.getElementById('btn_cancel_update').style.display="none";
}, 3000)
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</section>

</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="epcmask.php" class="forms">
					<section>
						<label>EPCマスク</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="epcmask_enabled">
								<?php
									if ( $dat[0x50 + 1] == 0){
										echo "<option value=0 selected>無効</option><option value=1>有効</option>";
									} else if ( $dat[0x50 + 1] == 1){
										echo "<option value=0>無効</option><option value=1 selected>有効</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>開始位置 : bit</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="epcmask_offset" value="{$dat[0x51 + 1]}" placeholder="{$dat[0x51 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>データ長 : bit</label>
						<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="epcmask_length" value="{$dat[0x52 + 1]}" placeholder="{$dat[0x52 + 1]}">
EOF;
						?>
					</section>
					<section>
						<label>データ : hex</label>
						<?php
						$epcmask = "";
						for($i = 1; $i < 17; $i++){
							$epcmask .= sprintf("%02X", $dat[0x53 + $i]);
						}
echo <<< EOF
<input type="text" class="width-6 custom" name="epcmask_data" value="{$epcmask}" placeholder="{$epcmask}" onkeyup="onShowLength('showlength', value);" maxlength="32">
EOF;
						?>
<p id="showlength"><?php echo strlen($epcmask); ?>文字</p>
					</section>
					<section>
						<button class="primary" name="epcmask_submit">変更する</button>
						<button class="secondary" type="button" name="epcmask_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="epcmask_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		if ( $dat[0x50 + 1] == 0){
			echo "document.getElementsByName(\"epcmask_enabled\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x50 + 1] == 1){
			echo "document.getElementsByName(\"epcmask_enabled\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"epcmask_offset\")[0].value = \"".$dat[0x51 + 1]."\";";
		echo "document.getElementsByName(\"epcmask_length\")[0].value = \"".$dat[0x52 + 1]."\";";
		echo "document.getElementsByName(\"epcmask_data\")[0].value = \"".$epcmask."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("epcmask_enabled")[0].selectedIndex= "0";
		document.getElementsByName("epcmask_offset")[0].value = "0";
		document.getElementsByName("epcmask_length")[0].value = "0";
		document.getElementsByName("epcmask_data")[0].value = "00000000000000000000000000000000";
	}
	function onShowLength(idn, str) {
	   document.getElementById(idn).innerHTML = str.length + "文字";
	}
	function onCountDownLength(idn, str, mnum) {
	   document.getElementById(idn).innerHTML = "あと" + (mnum - str.length) + "文字";
	}
</script>
</body>
</html>
