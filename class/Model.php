<?php
class Model {
	private $modelname = "";
	public function __construct()
	{
		if (strpos(file_get_contents("/etc/MODELNAME"),'FRU4025') !== false) {
			$this->modelname = "FRU4025";
		} else if(strpos(file_get_contents("/etc/MODELNAME"),'FRU4100') !== false) {
			$this->modelname = "FRU4100";
		}else{
			$this->modelname = "FRU4025";
		}
	}

	public function model() {
		return $this->modelname;
	}
	
	public function isFRU4025() {
		return $this->modelname == "FRU4025";
	}
	
	public function isFRU4100() {
		return $this->modelname == "FRU4100";
	}

	public function logoPath()
	{
		if($this->modelname == "FRU4025"){
			return "img/FRU-4025Plus-white.png";
		} else if($this->modelname == "FRU4100"){
			return "img/FRU-4100Plus-white.png";
		}
	}

	public function antennaPortCount()
	{
		if($this->modelname == "FRU4025"){
			return 2;
		} else if($this->modelname == "FRU4100"){
			return 4;
		}
	}

	public function rfPower()
	{
		if($this->modelname == "FRU4025"){
			return '240';
		} else if($this->modelname == "FRU4100"){
			return '300';
		}
	}

	public function rfPowerRange()
	{
		if($this->modelname == "FRU4025"){
			return array('min_range'=>100, 'max_range'=>240);
		} else if($this->modelname == "FRU4100"){
			return array('min_range'=>50, 'max_range'=>300);
		}
	}

	public function defaultFreqCh()
	{
		if($this->modelname == "FRU4025"){
			return '26';
		} else if($this->modelname == "FRU4100"){
			return '5';
		}
	}

	public function chToIndex()
	{
		if($this->modelname == "FRU4025"){
			return array( 0=>0, 5=>1, 11=>2, 17=>3, 23=>4, 24=>5, 25=>6, 26=>7, 27=>8, 28=>9, 29=>10, 30=>11, 31=>12, 32=>13, 255=>14);
		} else if($this->modelname == "FRU4100"){
			return array( 5=>0, 11=>1, 17=>2, 23=>3);
		}
	}

	public function channelStrings()
	{
		if($this->modelname == "FRU4025"){
			return array( 0=>'LBT Scan(All)',
					5=>'CH5(916.8MHz)',
					11=>'CH11(918.0MHz)',
					17=>'CH17(919.2MHz)',
					23=>'CH23(920.4MHz)',
					24=>'CH24(920.6MHz)',
					25=>'CH25(920.8MHz)',
					26=>'CH26(921.0MHz)',
					27=>'CH27(921.2MHz)',
					28=>'CH28(921.4MHz)',
					29=>'CH29(921.6MHz)',
					30=>'CH30(921.8MHz)',
					31=>'CH31(922.0MHz)',
					32=>'CH32(922.2MHz)',
					255=>'LBT Scan(CH23 - CH32)');
		} else if($this->modelname == "FRU4100"){
			return array( 5=>'CH5(916.8MHz)',
					11=>'CH11(918.0MHz)',
					17=>'CH17(919.2MHz)',
					23=>'CH23(920.4MHz)');
		}
	}
}
?>
