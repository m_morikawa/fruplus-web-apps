<?php
class LogParameter{
	private $xml = null;
	public function __construct()
	{
		$this->xml = new SimpleXMLElement('./others/logconfig.xml',null,true);
	}
	
	function __destruct() 
	{
       unset($xml);
    }
    
    public function xml(){
    	return $this->xml;
    }
    
    public function save(){
    	call_user_func(function(){
			$permission = substr(sprintf('%o', fileperms('./others/logconfig.xml')), -4);
			$cmd = "sudo chmod %s ./others/logconfig.xml";
			if (!strstr($permission, '777')) {
				exec(sprintf($cmd, '777'), $output);
			}
		});
		$this->xml->asXml('./others/logconfig.xml');
    }	
}
?>

