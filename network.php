<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'SystemParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$parameter = new SystemParameter();
	$networkinfo = $parameter->xml()->network;
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['network_submit'])){
		function validateNetwork(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'network_bootproto')){
				$msg[] = "IP設定を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['network_bootproto'] !== "static" && $_POST['network_bootproto'] !== "dhcp"){
				$msg[] = "不正なIP設定が入力されています。";
				$ret = FALSE;
			}
			if($_POST['network_bootproto'] === "static"){
				if(!filter_has_var(INPUT_POST, 'network_ipaddress')){
					$msg[] = "IPアドレスを入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_input(INPUT_POST,'network_ipaddress',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
					$msg[] = "不正なIPアドレスが入力されています。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'network_netmask')){
					$msg[] = "ネットマスクを入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_input(INPUT_POST,'network_netmask',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
					$msg[] = "不正なネットマスクが入力されています。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'network_gateway')){
					$msg[] = "ゲートウェイを入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_input(INPUT_POST,'network_gateway',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
					$msg[] = "不正なゲートウェイが入力されています。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'network_primarydns')){
				$msg[] = "優先DNSを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['network_primarydns'] != ""){
				if(!filter_input(INPUT_POST,'network_primarydns',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
					$msg[] = "不正な優先DNSが入力されています。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'network_secondarydns')){
				$msg[] = "代替DNSを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['network_secondarydns'] != ""){
				if(!filter_input(INPUT_POST,'network_secondarydns',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
					$msg[] = "不正な代替DNSが入力されています。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateNetwork($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$networkinfo->bootproto[0] = $_POST['network_bootproto'];
			if($networkinfo->bootproto[0] == 'static'){
				$networkinfo->ipaddress[0] = $_POST['network_ipaddress'];
				$networkinfo->netmask[0] = $_POST['network_netmask'];
				$networkinfo->gateway[0] = $_POST['network_gateway'];
			}
			$networkinfo->primarydns[0] = $_POST['network_primarydns'];
			$networkinfo->secondarydns[0] = $_POST['network_secondarydns'];
			$parameter->save();

			exec('sudo frucgi update network',$res);
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "ネットワーク設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::NETWORK); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>システム設定 > ネットワーク</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary">
<strong>ネットワーク設定を変更しました。</strong> <br/>
この設定を反映するするためには、<strong>デバイスの電源を切り、再起動してください</strong>。
</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="network.php" class="forms">
					<section>
						<label>IP設定</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="network_bootproto" id="network_bootproto">
								<?php
									if ( $networkinfo->bootproto == "static"){
										echo "<option value=static selected>Static</option><option value=dhcp>DHCP</option>";
									} else if ( $networkinfo->bootproto == "dhcp"){
										echo "<option value=static>Static</option><option value=dhcp selected>DHCP</option>";
									}
								?>
								</select>
							</column>
						</row>
						<script>
							function onNetworkBootprotoChanged(){
								if(this.options[this.selectedIndex].value == "static"){
									document.getElementById("staticconfig").style.display="block";
								}else if(this.options[this.selectedIndex].value == "dhcp"){
									document.getElementById("staticconfig").style.display="none";
								}
							}
							if(document.getElementById("network_bootproto").addEventListener){
								document.getElementById("network_bootproto").addEventListener("change", onNetworkBootprotoChanged, false);
							}else if(document.getElementById("network_bootproto").attachEvent){
								document.getElementById("network_bootproto").attachEvent("onchange", onNetworkBootprotoChanged);
							}else{
								document.getElementById("network_bootproto").onchange = onNetworkBootprotoChanged;
							}
						</script>
					</section>

					<div id="staticconfig">
						<section>
							<label>IPアドレス</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="network_ipaddress" value="{$networkinfo->ipaddress}" placeholder="{$networkinfo->ipaddress}">
EOF;
							?>
						</section>
						<section>
							<label>ネットマスク</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="network_netmask" value="{$networkinfo->netmask}" placeholder="{$networkinfo->netmask}">
EOF;
							?>
						</section>
						<section>
							<label>ゲートウェイ</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="network_gateway" value="{$networkinfo->gateway}" placeholder="{$networkinfo->gateway}">
EOF;
							?>
						</section>
						<section>
							<label>優先DNS</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="network_primarydns" value="{$networkinfo->primarydns}" placeholder="{$networkinfo->primarydns}">
EOF;
							?>
						</section>
						<section>
							<label>代替DNS</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="network_secondarydns" value="{$networkinfo->secondarydns}" placeholder="{$networkinfo->secondarydns}">
EOF;
							?>
						</section>
						<?php
						if ( $networkinfo->bootproto == "static"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("staticconfig").style.display="block";
</script>
EOF;
						} else if ( $networkinfo->bootproto == "dhcp"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("staticconfig").style.display="none";
</script>
EOF;
						}
						?>
					</div>
					<section>
						<button class="primary" name="network_submit">変更する</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
</body>
</html>
