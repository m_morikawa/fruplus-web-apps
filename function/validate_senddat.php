<?php
	require_once('function/load_translation.php');
	list($langFlag,$validate_translation) = loadTranslation('senddat_validate');

	function validate_senddat(&$msg){
		$ret = TRUE;
		if(!filter_has_var(INPUT_POST, 'addinfo_enabled')){
			$msg[] = "付加情報の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_enabled'] !== "true" && $_POST['addinfo_enabled'] !== "false"){
			$msg[] = "不正な付加情報の状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_id')){
			$msg[] = "付加情報の端末IDを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_id'] !== "true" && $_POST['addinfo_id'] !== "false"){
			$msg[] = "不正な付加情報の端末IDが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_time')){
			$msg[] = "付加情報の端末時刻を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_time'] !== "true" && $_POST['addinfo_time'] !== "false"){
			$msg[] = "不正な付加情報の端末時刻が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_pc')){
			$msg[] = "付加情報のPCコードを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_pc'] !== "true" && $_POST['addinfo_pc'] !== "false"){
			$msg[] = "不正な付加情報のPCコードが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_antennano')){
			$msg[] = "付加情報のアンテナポートを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_antennano'] !== "true" && $_POST['addinfo_antennano'] !== "false"){
			$msg[] = "不正な付加情報のアンテナポートが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_rssi')){
			$msg[] = "付加情報のRSSIを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_rssi'] !== "true" && $_POST['addinfo_rssi'] !== "false"){
			$msg[] = "不正な付加情報のRSSIが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_enabled')){
			$msg[] = "付加情報のタグメモリの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_enabled'] !== "true" && $_POST['addinfo_memory_enabled'] !== "false"){
			$msg[] = "不正な付加情報のタグメモリの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_password')){
			$msg[] = "付加情報のタグメモリのパスワードを入力して下さい。";
			$ret = FALSE;
		}
		if(!ctype_xdigit($_POST['addinfo_memory_password'])){
			$msg[] = "付加情報のタグメモリのパスワードは16進数で入力して下さい。";
			$ret = FALSE;
		}
		if(strlen($_POST['addinfo_memory_password']) != 8){
			$msg[] = "付加情報のタグメモリのパスワードは半角で8桁で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_bank')){
			$msg[] = "付加情報のタグメモリのバンクを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_bank'] !== "reserved" && $_POST['addinfo_memory_bank'] !== "epc" && $_POST['addinfo_memory_bank'] !== "tid" && $_POST['addinfo_memory_bank'] !== "user"){
			$msg[] = "不正な付加情報のタグメモリのバンクが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_offset')){
			$msg[] = "付加情報のタグメモリのOffsetを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_offset'] !== "0"){
			if(!filter_input(INPUT_POST, 'addinfo_memory_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
				$msg[] = "付加情報のタグメモリのOffsetは0から255の整数で入力して下さい。";
				$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_length')){
			$msg[] = "付加情報のタグメモリのLengthを入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'addinfo_memory_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>256)))){
			$msg[] = "付加情報のタグメモリのLengthは1から256の整数で入力して下さい。";
			$ret = FALSE;
		}
		return $ret;
	}
?>
