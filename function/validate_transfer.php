<?php
	require_once('function/load_translation.php');
	list($langFlag,$validate_translation) = loadTranslation('transfer_validate');

	function validate_transfer(&$msg){
		$ret = TRUE;
		if(!filter_has_var(INPUT_POST, 'transfer_protocol')){
			$msg[] = NO_PROTOCOL;
			$ret = FALSE;
		}
		if($_POST['transfer_protocol'] === "serial"){
		
		}else if($_POST['transfer_protocol'] === "socket"){
			if(!filter_has_var(INPUT_POST, 'transfer_ipaddress')){
				$msg[] = NO_IPADDRESS;
				$ret = FALSE;
			}
			if(strlen($_POST['transfer_ipaddress']) > 256){
				$msg[] = IPADDRESS_TOO_LONG;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_tcpport')){
				$msg[] = NO_TCPPORT;
				$ret = FALSE;
			}
			if($_POST['transfer_tcpport'] !== "0"){
				if(!filter_input(INPUT_POST, 'transfer_tcpport', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = INVALID_TCPPORT;
					$ret = FALSE;
				}
			}
		}else if($_POST['transfer_protocol'] === "http" || $_POST['transfer_protocol'] === "https"){
			if(!filter_has_var(INPUT_POST, 'transfer_httpmethod')){
				$msg[] = NO_HTTPMETHOD;
				$ret = FALSE;
			}
			if($_POST['transfer_httpmethod'] !== "post" && $_POST['transfer_httpmethod'] !== "get"){
				$msg[] = INVALID_HTTPMETHOD;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_host')){
				$msg[] = NO_HOST;
				$ret = FALSE;
			}
			if(strlen($_POST['transfer_host']) > 256){
				$msg[] = HOST_TOO_LONG;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_path')){
				$msg[] = NO_PATH;
				$ret = FALSE;
			}
			if(strlen($_POST['transfer_path']) > 256){
				$msg[] = PATH_TOO_LONG;
				$ret = FALSE;
			}
			if(!"http://".filter_var($_POST['transfer_host'].$_POST['transfer_path'],FILTER_VALIDATE_URL)){
				$msg[] = INVALID_HOST_PATH;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_httpport')){
				$msg[] = NO_HTTPPORT;
				$ret = FALSE;
			}
			if($_POST['transfer_httpport'] !== "0"){
				if(!filter_input(INPUT_POST, 'transfer_httpport', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = INVALID_HTTPPORT;
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'transfer_format')){
				$msg[] = NO_FORMAT;
				$ret = FALSE;
			}
			if($_POST['transfer_format'] !== "query" && $_POST['transfer_format'] !== "json"){
				$msg[] = INVALID_FORMAT;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_authuser')){
				$msg[] = NO_AUTHUSER;
				$ret = FALSE;
			}
			if(strlen($_POST['transfer_authuser']) > 32){
				$msg[] = AUTHUSER_TOO_LONG;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'transfer_authpass')){
				$msg[] = NO_AUTHPASS;
				$ret = FALSE;
			}
			if(strlen($_POST['transfer_authpass']) > 32){
				$msg[] = AUTHPASS_TOO_LONG;
				$ret = FALSE;
			}
		}else{
			$msg[] = INVALID_PROTOCOL;
			$ret = FALSE;
		}
		return $ret;
	}
?>
