<?php
	function loadTranslation($item){
		session_start();

		$langFlag = "ja";
		if(!isset($_SESSION['lang'])){
			if(isset($_GET['lang'])){
				if($_GET['lang'] == "en"){
					$langFlag = "en";
				}
			}
			$_SESSION['lang'] = $langFlag;
		}else{
			if($_SESSION['lang'] == "en"){
				$langFlag = "en";
			}
		}
		$json = file_get_contents("translation/".$langFlag.".json");
		$params = json_decode($json,TRUE)[$item];

		foreach($params as $key=>$val) {
			define($key, $val);
		}

		return array($langFlag,$params);
	}
?>
