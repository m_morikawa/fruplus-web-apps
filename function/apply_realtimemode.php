<?php
	function apply_mode(){
		global $realtimemode;
		$realtimemode->antenna[0]->enabled[0] = $_POST['antenna_0_enabled'];
		if($realtimemode->antenna[0]->enabled[0] == "true"){
			$realtimemode->antenna[0]->power[0] = $_POST['antenna_0_power'];
			$realtimemode->antenna[0]->dwell[0] = $_POST['antenna_0_dwell'];
			$realtimemode->antenna[0]->invcnt[0] = $_POST['antenna_0_invcnt'];
		}
		$realtimemode->antenna[1]->enabled[0] = $_POST['antenna_1_enabled'];
		if($realtimemode->antenna[1]->enabled[0] == "true"){
			$realtimemode->antenna[1]->power[0] = $_POST['antenna_1_power'];
			$realtimemode->antenna[1]->dwell[0] = $_POST['antenna_1_dwell'];
			$realtimemode->antenna[1]->invcnt[0] = $_POST['antenna_1_invcnt'];
		}
		$realtimemode->frequency[0]->ch[0] = $_POST['frequency_ch'];
		$realtimemode->multiepccheck[0]->enabled[0] = $_POST['multiepccheck_enabled'];
		$realtimemode->multiepccheck[0]->scanreset[0]->byinterval[0]->enabled[0] = $_POST['multiepccheck_scanreset_byinterval_enabled'];
		if($realtimemode->multiepccheck[0]->scanreset[0]->byinterval[0]->enabled[0]){
			$realtimemode->multiepccheck[0]->scanreset[0]->byinterval[0]->interval[0] = $_POST['multiepccheck_scanreset_byinterval_interval'];
		}
		$realtimemode->multiepccheck[0]->scanreset[0]->bytrigger[0]->enabled[0] = $_POST['multiepccheck_scanreset_bytrigger_enabled'];
		if($realtimemode->multiepccheck[0]->scanreset[0]->bytrigger[0]->enabled[0]){
			$realtimemode->multiepccheck[0]->scanreset[0]->bytrigger[0]->port[0] = $_POST['multiepccheck_scanreset_bytrigger_port'];
			$realtimemode->multiepccheck[0]->scanreset[0]->bytrigger[0]->trigger[0] = $_POST['multiepccheck_scanreset_bytrigger_trigger'];
		}
		$realtimemode->taginfo[0]->format[0] = $_POST['taginfo_format'];
		if($realtimemode->taginfo[0]->format[0] == "part"){
			$realtimemode->taginfo[0]->offset[0] = $_POST['taginfo_offset'];
			$realtimemode->taginfo[0]->length[0] = $_POST['taginfo_length'];
		}
		$realtimemode->addinfo[0]->enabled[0] = $_POST['addinfo_enabled'];
		if($realtimemode->addinfo[0]->enabled[0] == "true"){
			$realtimemode->addinfo[0]->id[0] = $_POST['addinfo_id'];
			$realtimemode->addinfo[0]->time[0] = $_POST['addinfo_time'];
			$realtimemode->addinfo[0]->antennano[0] = $_POST['addinfo_antennano'];
			$realtimemode->addinfo[0]->pc[0] = $_POST['addinfo_pc'];
			$realtimemode->addinfo[0]->rssi[0] = $_POST['addinfo_rssi'];
			$realtimemode->addinfo[0]->memory[0]->enabled[0] = $_POST['addinfo_memory_enabled'];
			if($realtimemode->addinfo[0]->memory[0]->enabled[0] == "true"){
				$realtimemode->addinfo[0]->memory[0]->password[0] = $_POST['addinfo_memory_password'];
				$realtimemode->addinfo[0]->memory[0]->bank[0] = $_POST['addinfo_memory_bank'];
				$realtimemode->addinfo[0]->memory[0]->offset[0] = $_POST['addinfo_memory_offset'];
				$realtimemode->addinfo[0]->memory[0]->length[0] = $_POST['addinfo_memory_length'];
			}
		}
		
		$realtimemode->dout[0]->enabled[0] = $_POST['dout_enabled'];
		if($realtimemode->dout[0]->enabled[0] == "true"){
			$realtimemode->dout[0]->port[0] = $_POST['dout_port'];
			$realtimemode->dout[0]->downtime[0] = $_POST['dout_downtime'];
		}
		$realtimemode->response[0]->enabled[0] = $_POST['response_enabled'];
		if($realtimemode->response[0]->enabled[0] == "true"){
			$realtimemode->response[0]->timeout[0] = $_POST['response_timeout'];
		}
		$realtimemode->request[0]->enabled[0] = $_POST['request_enabled'];
		$realtimemode->errorinfo[0]->enabled[0] = $_POST['errorinfo_enabled'];
	}
?>
