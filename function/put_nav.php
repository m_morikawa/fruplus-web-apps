<?php
	class NavState
	{
		const NOTANY = 0x0000;
		const TRANSFER = 0x1001;
		const FILTER = 0x1002;
		const SENDDATA = 0x1003;
		const RFIDCONF = 0x1004;
		const MODE1CONF = 0x1005;
		const MODE2CONF = 0x1006;
		const MODE3CONF = 0x1007;
		const HTTP = 0x2010;
		const READER = 0x2020;
		const ANTENNA = 0x2030;
		const EPCMASK = 0x2040;
		const INVENTORY = 0x2050;
		const PARAMETERVERSION = 0x2060;
		const NETWORK = 0x3100;
		const DEVICE = 0x3200;
		const PASSWORD = 0x3300;
		const DEVELOP = 0x3400;
		const DEBUGLOG = 0x4000;
		const RESET = 0x5000;
		const FWUPDATE = 0x6000;
		const INFO = 0x7000;
	}

	require_once('function/load_translation.php');
	loadTranslation('navi');

	function put_nav($state){
		echo "<div id=\"nav\">";
		echo "<ul class=\"menu\">";
		//<--オートモード設定-->
		if(($state & 0xF000) === 0x1000){
			echo "<li class=\"current\">";
		}else{
			echo "<li>";
		}
		echo "<a href=\"javascript:void(0);\"><span class=\"batch\" data-icon=\"&#xF06B;\"></span> ".NAVI_AUTOMODE."</a>";
		echo "<ul>";
		if(($state & 0x000F) == 0x0001){
			echo "<li><a href=\"sendto.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_SENDTO."</a></li>";
		}else{
			echo "<li><a href=\"sendto.php\">".NAVI_SENDTO."</a></li>";
		}
		if(($state & 0x000F) == 0x0002){
			echo "<li><a href=\"filter.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_FILTER."</a></li>";
		}else{
			echo "<li><a href=\"filter.php\">".NAVI_FILTER."</a></li>";
		}
		if(($state & 0x000F) == 0x0003){
			echo "<li><a href=\"senddata.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_SENDDATA."</a></li>";
		}else{
			echo "<li><a href=\"senddata.php\">".NAVI_SENDDATA."</a></li>";
		}
		if(($state & 0x000F) == 0x0004){
			echo "<li><a href=\"rfidconf.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_RFIDCONF."</a></li>";
		}else{
			echo "<li><a href=\"rfidconf.php\">".NAVI_RFIDCONF."</a></li>";
		}
		if(($state & 0x000F) == 0x0005){
			echo "<li><a href=\"mode1conf.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_MODE1CONF."</a></li>";
		}else{
			echo "<li><a href=\"mode1conf.php\">".NAVI_MODE1CONF."</a></li>";
		}
		if(($state & 0x000F) == 0x0006){
			echo "<li><a href=\"mode2conf.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_MODE2CONF."</a></li>";
		}else{
			echo "<li><a href=\"mode2conf.php\">".NAVI_MODE2CONF."</a></li>";
		}
		if(($state & 0x000F) == 0x0007){
			echo "<li><a href=\"mode3conf.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_MODE3CONF."</a></li>";
		}else{
			echo "<li><a href=\"mode3conf.php\">".NAVI_MODE3CONF."</a></li>";
		}
		echo "</ul>";
		echo "</li>";
		//<--オートモード設定-->
		//<--マニュアルモード設定-->
		if(($state & 0xF000) == 0x2000){
			echo "<li class=\"current\">";
		}else{
			echo "<li>";
		}
		echo "<a href=\"javascript:void(0);\"><span class=\"batch\" data-icon=\"&#xF06B;\"></span> ".NAVI_MANUALMODE."</a>";
		echo "<ul>";
		if(($state & 0x00F0) == 0x0010){
			echo "<li><a href=\"http.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_COMM."</a></li>";
		}else{
			echo "<li><a href=\"http.php\">".NAVI_COMM."</a></li>";
		}
		if(($state & 0x00F0) == 0x0020){
			echo "<li><a href=\"reader.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_READER."</a></li>";
		}else{
			echo "<li><a href=\"reader.php\">".NAVI_READER."</a></li>";
		}
		if(($state & 0x00F0) == 0x0030){
			echo "<li><a href=\"antenna.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_ANTENNA."</a></li>";
		}else{
			echo "<li><a href=\"antenna.php\">".NAVI_ANTENNA."</a></li>";
		}
		if(($state & 0x00F0) == 0x0040){
			echo "<li><a href=\"epcmask.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_EPCMASK."</a></li>";
		}else{
			echo "<li><a href=\"epcmask.php\">".NAVI_EPCMASK."</a></li>";
		}
		if(($state & 0x00F0) == 0x0050){
			echo "<li><a href=\"inventory.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_INVENTORY."</a></li>";
		}else{
			echo "<li><a href=\"inventory.php\">".NAVI_INVENTORY."</a></li>";
		}
		if(($state & 0x00F0) == 0x0060){
			echo "<li><a href=\"rwcfgversion.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_CONFVERSION."</a></li>";
		}else{
			echo "<li><a href=\"rwcfgversion.php\">".NAVI_CONFVERSION."</a></li>";
		}
		echo "</ul>";
		echo "</li>";
		//<--マニュアルモード設定-->
		//<--システム設定-->
		if(($state & 0xF000) == 0x3000){
			echo "<li class=\"current\">";
		}else{
			echo "<li>";
		}
		echo "<a href=\"javascript:void(0);\"><span class=\"batch\" data-icon=\"&#xF06B;\"></span> ".NAVI_SYSTEM."</a>";
		echo "<ul>";
		if(($state & 0x0F00) == 0x0100){
			echo "<li><a href=\"network.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_NETWORK."</a></li>";
		}else{
			echo "<li><a href=\"network.php\">".NAVI_NETWORK."</a></li>";
		}
		if(($state & 0x0F00) == 0x0200){
			echo "<li><a href=\"system.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_DEVICE."</a></li>";
		}else{
			echo "<li><a href=\"system.php\">".NAVI_DEVICE."</a></li>";
		}
		if(($state & 0x0F00) == 0x0300){
			echo "<li><a href=\"password.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_PASSWORD."</a></li>";
		}else{
			echo "<li><a href=\"password.php\">".NAVI_PASSWORD."</a></li>";
		}
		if(($state & 0x0F00) == 0x0400){
			echo "<li><a href=\"develop.php\" class=\"selected\"><span class=\"batch\" data-icon=\"&#xF0D3;\"></span> ".NAVI_DEVELOP."</a></li>";
		}else{
			echo "<li><a href=\"develop.php\">".NAVI_DEVELOP."</a></li>";
		}
		echo "</ul>";
		echo "</li>";
		//<--システム設定-->
		//<--デバッグログ-->
		echo "<li><a href=\"debuglog.php\" class=\"no_children\"><span class=\"batch\" data-icon=\"&#xF097;\"></span> ".NAVI_DEBUGLOG."</a></li>";
		//<--デバッグログ-->
		//<--リセット-->
		echo "<li><a href=\"reset.php\" class=\"no_children\"><span class=\"batch\" data-icon=\"&#xF129;\"></span> ".NAVI_RESET."</a></li>";
		//<--リセット-->
		//<--FW更新-->
		echo "<li><a href=\"fwupdate.php\" class=\"no_children\"><span class=\"batch\" data-icon=\"&#xF0BD;\"></span> ".NAVI_FWUPDATE."</a></li>";
		//<--FW更新-->
		//<--端末情報-->
		echo "<li><a href=\"fruinfo.php\" class=\"no_children\"><span class=\"batch\" data-icon=\"&#xF0A7;\"></span> ".NAVI_DEVICEINFO."</a></li>";
		//<--端末情報-->
		echo "</ul>";
		echo "</div>";
	}
?>
