<?php
	function validate_mode(&$msg){
		$ret = TRUE;
		if(!filter_has_var(INPUT_POST, 'http_command_format')){
			$msg[] = "HTTPコマンドフォーマットを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['http_command_format'] !== "query" && $_POST['http_command_format'] !== "json"){
			$msg[] = "不正なHTTPコマンドフォーマットが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'http_response_format')){
			$msg[] = "HTTPレスポンスフォーマットを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['http_response_format'] !== "json" && $_POST['http_response_format'] !== "xml"){
			$msg[] = "不正なHTTPレスポンスフォーマットが入力されています。";
			$ret = FALSE;
		}
		return $ret;
	}
?>
