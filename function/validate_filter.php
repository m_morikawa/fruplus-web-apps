<?php
	require_once('function/load_translation.php');
	list($langFlag,$validate_translation) = loadTranslation('filter_validate');

	function validate_filter(&$msg){
		$ret = TRUE;
		if(!filter_has_var(INPUT_POST, 'pcfilter_enabled')){
			$msg[] = NO_PCFILTER;
			$ret = FALSE;
		}
		if($_POST['pcfilter_enabled'] !== "true" && $_POST['pcfilter_enabled'] !== "false"){
			$msg[] = INVALID_PCFILTER;
			$ret = FALSE;
		}
		if($_POST['pcfilter_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'pcfilter_offset')){
				$msg[] = NO_PCFILTER_OFFSET;
				$ret = FALSE;
			}
			if($_POST['pcfilter_offset'] !== "0"){
				if(!filter_input(INPUT_POST, 'pcfilter_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
					$msg[] = INVALID_PCFILTER_OFFSET;
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'pcfilter_length')){
				$msg[] = NO_PCFILTER_LENGTH;
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'pcfilter_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>16)))){
				$msg[] = INVALID_PCFILTER_LENGTH;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'pcfilter_data')){
				$msg[] = NO_PCFILTER_DATA;
				$ret = FALSE;
			}
			if(!ctype_xdigit($_POST['pcfilter_data'])){
				$msg[] = BAD_FORMAT_PCFILTER_DATA;
				$ret = FALSE;
			}
			if(strlen($_POST['pcfilter_data']) != 4){
				$msg[] = BAD_LENGTH_PCFILTER_DATA;
				$ret = FALSE;
			}
			if($ret){
				exec('sudo frucgi validate pcmask '.$_POST['pcfilter_offset'].' '.$_POST['pcfilter_length'].' '.$_POST['pcfilter_data'],$res);
				$validate = json_decode($res[0],true);
				if(strpos($validate["status"],'success') === false){
					$msg[] = INVALID_PCFILTER_DATA;
					$ret = FALSE;
				}
			}
		}
		if(!filter_has_var(INPUT_POST, 'epcfilter_enabled')){
			$msg[] = NO_EPCFILTER;
			$ret = FALSE;
		}
		if($_POST['epcfilter_enabled'] !== "true" && $_POST['epcfilter_enabled'] !== "false"){
			$msg[] = INVALID_EPCFILTE;
			$ret = FALSE;
		}
		if($_POST['epcfilter_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'epcfilter_offset')){
				$msg[] = NO_EPCFILTER_OFFSET;
				$ret = FALSE;
			}
			if($_POST['epcfilter_offset'] !== "0"){
				if(!filter_input(INPUT_POST, 'epcfilter_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>127)))){
					$msg[] = INVALID_EPCFILTER_OFFSET;
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'epcfilter_length')){
				$msg[] = NO_EPCFILTER_LENGTH;
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'epcfilter_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>128)))){
				$msg[] = INVALID_EPCFILTER_LENGTH;
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'epcfilter_data')){
				$msg[] = NO_EPCFILTER_DATA;
				$ret = FALSE;
			}
			if(!ctype_xdigit($_POST['epcfilter_data'])){
				$msg[] = BAD_FORMAT_EPCFILTER_DATA;
				$ret = FALSE;
			}
			if(strlen($_POST['epcfilter_data']) != 32){
				$msg[] = BAD_LENGTH_EPCFILTER_DATA;
				$ret = FALSE;
			}
			if($ret){
				unset($res);
				exec('sudo frucgi validate epcmask '.$_POST['epcfilter_offset'].' '.$_POST['epcfilter_length'].' '.$_POST['epcfilter_data'],$res);
				$validate = json_decode($res[0],true);
				if(strpos($validate["status"],'success') === false){
					$msg[] = INVALID_EPCFILTER_DATA;
					$ret = FALSE;
				}
			}
		}
		if(!filter_has_var(INPUT_POST, 'rssifilter_lower_enabled')){
			$msg[] = NO_RSSIFILTER_LOWER;
			$ret = FALSE;
		}
		if($_POST['rssifilter_lower_enabled'] !== "true" && $_POST['rssifilter_lower_enabled'] !== "false"){
			$msg[] = INVALID_RSSIFILTER_LOWER;
			$ret = FALSE;
		}
		if($_POST['rssifilter_lower_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'rssifilter_lower_value')){
				$msg[] = NO_RSSIFILTER_LOWER_VALUE;
				$ret = FALSE;
			}
			if($_POST['rssifilter_lower_value']  !== '0'){
				if(!filter_input(INPUT_POST, 'rssifilter_lower_value', FILTER_VALIDATE_INT, array('options' => array('min_range'=>-100, 'max_range'=>0)))){
					$msg[] = INVALID_RSSIFILTER_LOWER_VALUE;
					$ret = FALSE;
				}
			}
		}
		if(!filter_has_var(INPUT_POST, 'rssifilter_upper_enabled')){
			$msg[] = NO_RSSIFILTER_UPPER;
			$ret = FALSE;
		}
		if($_POST['rssifilter_upper_enabled'] !== "true" && $_POST['rssifilter_upper_enabled'] !== "false"){
			$msg[] = INVALID_RSSIFILTER_UPPER;
			$ret = FALSE;
		}
		if($_POST['rssifilter_upper_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'rssifilter_upper_value')){
				$msg[] = NO_RSSIFILTER_UPPER_VALUE;
				$ret = FALSE;
			}
			if($_POST['rssifilter_upper_value']  !== '0'){
				if(!filter_input(INPUT_POST, 'rssifilter_upper_value', FILTER_VALIDATE_INT, array('options' => array('min_range'=>-100, 'max_range'=>0)))){
					$msg[] = INVALID_RSSIFILTER_UPPER_VALUE;
					$ret = FALSE;
				}
			}
		}
		if($_POST['rssifilter_lower_enabled'] === "true" && $_POST['rssifilter_upper_enabled'] === "true"){
			if($_POST['rssifilter_upper_value'] < $_POST['rssifilter_lower_value']){
				$msg[] = BAD_RSSIFILTER_VALUES;
				$ret = FALSE;
			}
		}
		return $ret;
	}
?>
