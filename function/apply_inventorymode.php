<?php
	function apply_mode(){
		global $inventorymode;
		$inventorymode->antenna[0]->enabled[0] = $_POST['antenna_0_enabled'];
		if($inventorymode->antenna[0]->enabled[0] == "true"){
			$inventorymode->antenna[0]->power[0] = $_POST['antenna_0_power'];
			$inventorymode->antenna[0]->dwell[0] = $_POST['antenna_0_dwell'];
			$inventorymode->antenna[0]->invcnt[0] = $_POST['antenna_0_invcnt'];
		}
		$inventorymode->antenna[1]->enabled[0] = $_POST['antenna_1_enabled'];
		if($inventorymode->antenna[1]->enabled[0] == "true"){
			$inventorymode->antenna[1]->power[0] = $_POST['antenna_1_power'];
			$inventorymode->antenna[1]->dwell[0] = $_POST['antenna_1_dwell'];
			$inventorymode->antenna[1]->invcnt[0] = $_POST['antenna_1_invcnt'];
		}
		$inventorymode->frequency[0]->ch[0] = $_POST['frequency_ch'];
		$inventorymode->multiepccheck[0]->enabled[0] = $_POST['multiepccheck_enabled'];
		$inventorymode->inventory[0]->port[0] = $_POST['inventory_port'];
		$inventorymode->inventory[0]->trigger[0] = $_POST['inventory_trigger'];
		$inventorymode->inventory[0]->mode[0] = $_POST['inventory_scanmode'];
		if($_POST['inventory_scanmode'] == "1"){
			$inventorymode->inventory[0]->mode1conf[0]->timeout[0] = $_POST['inventory_timeout'];
		}else if($_POST['inventory_scanmode'] == "2"){
			$inventorymode->inventory[0]->mode2conf[0]->delaytime[0] = $_POST['inventory_delaytime'];
			$inventorymode->inventory[0]->mode2conf[0]->scantime[0] = $_POST['inventory_scantime'];
		}
		$inventorymode->whendopost = $_POST['whendopost'];
		$inventorymode->taginfo[0]->format[0] = $_POST['taginfo_format'];
		if($inventorymode->taginfo[0]->format[0] == "part"){
			$inventorymode->taginfo[0]->offset[0] = $_POST['taginfo_offset'];
			$inventorymode->taginfo[0]->length[0] = $_POST['taginfo_length'];
		}
		$inventorymode->addinfo[0]->enabled[0] = $_POST['addinfo_enabled'];
		if($inventorymode->addinfo[0]->enabled[0] == "true"){
			$inventorymode->addinfo[0]->id[0] = $_POST['addinfo_id'];
			$inventorymode->addinfo[0]->time[0] = $_POST['addinfo_time'];
			$inventorymode->addinfo[0]->antennano[0] = $_POST['addinfo_antennano'];
			$inventorymode->addinfo[0]->pc[0] = $_POST['addinfo_pc'];
			$inventorymode->addinfo[0]->rssi[0] = $_POST['addinfo_rssi'];
			$inventorymode->addinfo[0]->memory[0]->enabled[0] = $_POST['addinfo_memory_enabled'];
			if($inventorymode->addinfo[0]->memory[0]->enabled[0] == "true"){
				$inventorymode->addinfo[0]->memory[0]->password[0] = $_POST['addinfo_memory_password'];
				$inventorymode->addinfo[0]->memory[0]->bank[0] = $_POST['addinfo_memory_bank'];
				$inventorymode->addinfo[0]->memory[0]->offset[0] = $_POST['addinfo_memory_offset'];
				$inventorymode->addinfo[0]->memory[0]->length[0] = $_POST['addinfo_memory_length'];
			}
		}
		$inventorymode->dout[0]->enabled[0] = $_POST['dout_enabled'];
		if($inventorymode->dout[0]->enabled[0] == "true"){
			$inventorymode->dout[0]->port[0] = $_POST['dout_port'];
			$inventorymode->dout[0]->downtime[0] = $_POST['dout_downtime'];
		}
		$inventorymode->noreadinfo[0]->enabled[0] = $_POST['noreadinfo_enabled'];
		$inventorymode->errorinfo[0]->enabled[0] = $_POST['errorinfo_enabled'];
		$inventorymode->response[0]->enabled[0] = $_POST['response_enabled'];
		if($inventorymode->response[0]->enabled[0] == "true"){
			$inventorymode->response[0]->timeout[0] = $_POST['response_timeout'];
		}
		$inventorymode->request[0]->enabled[0] = $_POST['request_enabled'];
	}
?>
