<?php
	function validate_mode(&$msg){
		global $model;
		$ret = TRUE;
		if(!filter_has_var(INPUT_POST, 'antenna_0_enabled')){
			$msg[] = "アンテナ0の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['antenna_0_enabled'] !== "true" && $_POST['antenna_0_enabled'] !== "false"){
			$msg[] = "不正なアンテナ0の状態が入力されています。";
			$ret = FALSE;
		}
		$options = array('options' => $model->rfPowerRange());
		if($_POST['antenna_0_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'antenna_0_power')){
				$msg[] = "アンテナ0のPowerを入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'antenna_0_power', FILTER_VALIDATE_INT, $options)){
				$msg[] = "アンテナ0のPowerは" . $options['options']['min_range'] . "から" . $options['options']['max_range'] ."の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_0_dwell')){
				$msg[] = "アンテナ0のDwellを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_0_dwell'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_0_dwell', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ0のDwellは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'antenna_0_invcnt')){
				$msg[] = "アンテナ0のInvCntを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_0_invcnt'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_0_invcnt', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ0のInvCntは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if($_POST['antenna_0_dwell'] === "0" && $_POST['antenna_0_invcnt'] === "0" ){
					$msg[] = "アンテナ0のDwellとInvCntはともに0にはできません。";
					$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'antenna_1_enabled')){
			$msg[] = "アンテナ1の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['antenna_1_enabled'] !== "true" && $_POST['antenna_1_enabled'] !== "false"){
			$msg[] = "不正なアンテナ1の状態が入力されています。";
			$ret = FALSE;
		}
		if($_POST['antenna_1_enabled'] === "true"){
			if(!filter_has_var(INPUT_POST, 'antenna_1_power')){
				$msg[] = "アンテナ1のPowerを入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST, 'antenna_1_power', FILTER_VALIDATE_INT, $options)){
				$msg[] = "アンテナ1のPowerは" . $options['options']['min_range'] . "から" . $options['options']['max_range'] ."の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_dwell')){
				$msg[] = "アンテナ1のDwellを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_1_dwell'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_1_dwell', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ1のDwellは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_invcnt')){
				$msg[] = "アンテナ1のInvCntを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_1_invcnt'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_1_invcnt', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ1のInvCntは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if($_POST['antenna_1_dwell'] === "0" && $_POST['antenna_1_invcnt'] === "0" ){
					$msg[] = "アンテナ1のDwellとInvCntはともに0にはできません。";
					$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'frequency_ch')){
			$msg[] = "周波数チャンネルを入力して下さい。";
			$ret = FALSE;
		}
		$ch = 0;
		if($_POST['frequency_ch'] !== "0"){
			$ch = filter_input(INPUT_POST, 'frequency_ch', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)));
			if(!$ch){
				$msg[] = "周波数チャンネルは0から255の整数で入力して下さい。";
				$ret = FALSE;
			}
			require_once("check_ch.php");
			if(!filter_var($ch, FILTER_CALLBACK, array('options' => 'check_ch'))){
				$msg[] = "不正な周波数チャンネルのフォーマットが入力されました。";
				$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_enabled')){
			$msg[] = "タグ重複チェックの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['multiepccheck_enabled'] !== "true" && $_POST['multiepccheck_enabled'] !== "false"){
			$msg[] = "不正なタグ重複チェックの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_scanreset_bytrigger_enabled')){
			$msg[] = "重複チェックのトリガによるリセットの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['multiepccheck_scanreset_bytrigger_enabled'] !== "true" && $_POST['multiepccheck_scanreset_bytrigger_enabled'] !== "false"){
			$msg[] = "不正な重複チェックのトリガによるリセットの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_scanreset_bytrigger_port')){
			$msg[] = "重複チェックのトリガによるリセットのポートを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['multiepccheck_scanreset_bytrigger_port'] !== "0" && $_POST['multiepccheck_scanreset_bytrigger_port'] !== "1"){
			$msg[] = "不正な重複チェックのトリガによるリセットのポートが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_scanreset_bytrigger_trigger')){
			$msg[] = "重複チェックのトリガによるリセットの外部入力トリガを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['multiepccheck_scanreset_bytrigger_trigger'] !== "up" && $_POST['multiepccheck_scanreset_bytrigger_trigger'] !== "down"){
			$msg[] = "不正な重複チェックのトリガによるリセットの外部入力トリガが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_scanreset_byinterval_enabled')){
			$msg[] = "重複チェックの周期的リセットの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['multiepccheck_scanreset_byinterval_enabled'] !== "true" && $_POST['multiepccheck_scanreset_byinterval_enabled'] !== "false"){
			$msg[] = "不正な重複チェックの周期的リセットの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'multiepccheck_scanreset_byinterval_interval')){
			$msg[] = "重複チェックの周期的リセットのインターバルを入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'multiepccheck_scanreset_byinterval_interval', FILTER_VALIDATE_INT, array('options' => array('min_range'=>300, 'max_range'=>3600000)))){
			$msg[] = "重複チェックの周期的リセットのインターバルは300から3600000の整数で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'taginfo_format')){
			$msg[] = "タグ情報のフォーマットを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['taginfo_format'] !== "all" && $_POST['taginfo_format'] !== "part"){
			$msg[] = "不正なタグ情報のフォーマットが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'taginfo_offset')){
			$msg[] = "タグ情報のOffsetを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['taginfo_offset'] !== "0"){
			if(!filter_input(INPUT_POST, 'taginfo_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
				$msg[] = "タグ情報のOffsetは0から15の整数で入力して下さい。";
				$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'taginfo_length')){
			$msg[] = "タグ情報のLengthを入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'taginfo_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>16)))){
			$msg[] = "タグ情報のLengthは1から16の整数で入力して下さい。";
			$ret = FALSE;
		}
		if(intval($_POST['taginfo_offset']) + intval($_POST['taginfo_length']) > 16){
			$msg[] = "範囲外のタグ情報が入力されました。タグ情報は最大16byteです。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_enabled')){
			$msg[] = "付加情報の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_enabled'] !== "true" && $_POST['addinfo_enabled'] !== "false"){
			$msg[] = "不正な付加情報の状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_id')){
			$msg[] = "付加情報の端末IDを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_id'] !== "true" && $_POST['addinfo_id'] !== "false"){
			$msg[] = "不正な付加情報の端末IDが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_time')){
			$msg[] = "付加情報の端末時刻を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_time'] !== "true" && $_POST['addinfo_time'] !== "false"){
			$msg[] = "不正な付加情報の端末時刻が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_pc')){
			$msg[] = "付加情報のPCコードを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_pc'] !== "true" && $_POST['addinfo_pc'] !== "false"){
			$msg[] = "不正な付加情報のPCコードが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_antennano')){
			$msg[] = "付加情報のアンテナポートを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_antennano'] !== "true" && $_POST['addinfo_antennano'] !== "false"){
			$msg[] = "不正な付加情報のアンテナポートが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_rssi')){
			$msg[] = "付加情報のRSSIを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_rssi'] !== "true" && $_POST['addinfo_rssi'] !== "false"){
			$msg[] = "不正な付加情報のRSSIが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_enabled')){
			$msg[] = "付加情報のタグメモリの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_enabled'] !== "true" && $_POST['addinfo_memory_enabled'] !== "false"){
			$msg[] = "不正な付加情報のタグメモリの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_password')){
			$msg[] = "付加情報のタグメモリのパスワードを入力して下さい。";
			$ret = FALSE;
		}
		if(!ctype_xdigit($_POST['addinfo_memory_password'])){
			$msg[] = "付加情報のタグメモリのパスワードは16進数で入力して下さい。";
			$ret = FALSE;
		}
		if(strlen($_POST['addinfo_memory_password']) != 8){
			$msg[] = "付加情報のタグメモリのパスワードは半角で8桁で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_bank')){
			$msg[] = "付加情報のタグメモリのバンクを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_bank'] !== "reserved" && $_POST['addinfo_memory_bank'] !== "epc" && $_POST['addinfo_memory_bank'] !== "tid" && $_POST['addinfo_memory_bank'] !== "user"){
			$msg[] = "不正な付加情報のタグメモリのバンクが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_offset')){
			$msg[] = "付加情報のタグメモリのOffsetを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['addinfo_memory_offset'] !== "0"){
			if(!filter_input(INPUT_POST, 'addinfo_memory_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>255)))){
				$msg[] = "付加情報のタグメモリのOffsetは0から255の整数で入力して下さい。";
				$ret = FALSE;
			}
		}
		if(!filter_has_var(INPUT_POST, 'addinfo_memory_length')){
			$msg[] = "付加情報のタグメモリのLengthを入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'addinfo_memory_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>256)))){
			$msg[] = "付加情報のタグメモリのLengthは1から256の整数で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'dout_enabled')){
			$msg[] = "外部出力設定の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['dout_enabled'] !== "true" && $_POST['dout_enabled'] !== "false"){
			$msg[] = "不正な外部出力設定の状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'dout_port')){
			$msg[] = "外部出力設定のポートを入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['dout_port'] !== "0" && $_POST['dout_port'] !== "1"){
			$msg[] = "不正な外部出力設定のポートが入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'dout_downtime')){
			$msg[] = "外部出力設定の立ち下がり時間を入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'dout_downtime', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>60000)))){
			$msg[] = "外部出力設定の立ち下がり時間は1から60000の整数で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'errorinfo_enabled')){
			$msg[] = "エラー通知の状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['errorinfo_enabled'] !== "true" && $_POST['errorinfo_enabled'] !== "false"){
			$msg[] = "不正なエラー通知の状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'response_enabled')){
			$msg[] = "レスポンスの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['response_enabled'] !== "true" && $_POST['response_enabled'] !== "false"){
			$msg[] = "不正なレスポンスの状態が入力されています。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'response_timeout')){
			$msg[] = "レスポンスのタイムアウトを入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_input(INPUT_POST, 'response_timeout', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>60000)))){
			$msg[] = "レスポンスのタイムアウトは1から60000の整数で入力して下さい。";
			$ret = FALSE;
		}
		if(!filter_has_var(INPUT_POST, 'request_enabled')){
			$msg[] = "サーバーリクエストの状態を入力して下さい。";
			$ret = FALSE;
		}
		if($_POST['request_enabled'] !== "true" && $_POST['request_enabled'] !== "false"){
			$msg[] = "不正なサーバーリクエストの状態が入力されています。";
			$ret = FALSE;
		}
		return $ret;
	}
?>
