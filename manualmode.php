<input type="hidden" name="modetype" value="manual">
<fieldset>
	<legend><h5>HTTP設定</h5></legend>
	<section>
		<h4>リーダーをHTTPで制御する場合のコマンドフォーマットを変更できます。</h4>
		<p>HTTPコマンドフォーマットを設定してください。</p>
		<section>
			<label>HTTPコマンドフォーマット</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" id="http_command_format" name="http_command_format">
						<?php
							if ( $manualmode->httpcommandformat == "query"){
								echo "<option value=query selected>Query</option><option value=json>JSON</option>";
							} else if ( $manualmode->httpcommandformat == "json"){
								echo "<option value=query>Query</option><option value=json selected>JSON</option>";
							}
						?>
					</select>
				</column>
			</row>
		</section>
	</section>
	<section>
		<h4>リーダーをHTTPで制御する場合のレスポンスフォーマットを変更できます。</h4>
		<p>HTTPレスポンスフォーマットを設定してください。</p>
		<section>
			<label>HTTPレスポンスフォーマット</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" id="http_response_format" name="http_response_format">
						<?php
						if ( $manualmode->httpresponseformat == "json"){
							echo "<option value=json selected>JSON</option><option value=xml>XML</option>";
						} else if ( $manualmode->httpresponseformat == "xml"){
							echo "<option value=json>JSON</option><option value=xml selected>XML</option>";
						}
						?>
					</select>
				</column>
			</row>
		</section>
	</section>
</fieldset>
<script>
	function onToCurrentClicked(){
	<?php
	if ( $manualmode->httpcommandformat == "query"){
		echo "document.getElementsByName(\"http_command_format\")[0].selectedIndex= \"0\";";
	} else if ( $manualmode->httpcommandformat == "json"){
		echo "document.getElementsByName(\"http_command_format\")[0].selectedIndex= \"1\";";
	}
	if ( $manualmode->httpresponseformat == "json"){
		echo "document.getElementsByName(\"http_response_format\")[0].selectedIndex= \"0\";";
	} else if ( $manualmode->httpresponseformat == "xml"){
		echo "document.getElementsByName(\"http_response_format\")[0].selectedIndex= \"1\";";
	}
	?>
	}
function onToDefaultClicked(){
	document.getElementsByName("http_command_format")[0].selectedIndex= "0";
	document.getElementsByName("http_response_format")[0].selectedIndex= "0";
}
</script>
