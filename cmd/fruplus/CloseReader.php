<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class CloseReader extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'01','result'=>'FF');
	}
?>
