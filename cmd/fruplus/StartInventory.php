<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class StartInventory extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['method'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['method'])){
				return NULL;
			}
			if(strlen($cmd['method']) != 2){
				return NULL;
			}
			if($cmd['method'] == '01' || $cmd['method'] == '81'){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['method'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			$this->m_res['error'] = substr($res,5,4);
			if($this->m_res['result'] == '00'){
				$this->m_res['count'] = substr($res,9,2);
				if($this->m_res['count'] != '00'){
					include_once($_SERVER['DOCUMENT_ROOT'] . '/class/TagDataParser.php');
					$this->m_res['taginfo'] = TagDataParser::parse(substr($res,11,strlen($res) - 11));
				}
			}else{
				unset($this->m_res['count']);
			}

			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'20','result'=>'FF','error'=>'0000','count'=>'00');
	}
?>
