<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetDefaultConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['address'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['address'])){
				return NULL;
			}
			if(strlen($cmd['address']) != 2){
				return NULL;
			}
			if(!isset($cmd['count'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['count'])){
				return NULL;
			}
			if(strlen($cmd['count']) != 2){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['address'];
			$cmdstr .= $cmd['count'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['data'] = substr($res,5,strlen($res) - 5);
			}else{
				unset($this->m_res['data']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'89','result'=>'FF','data'=>'');
	}
?>
