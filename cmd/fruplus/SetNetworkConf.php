<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetNetworkConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['target'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['target'])){
				return NULL;
			}
			if(strlen($cmd['target']) != 2){
				return NULL;
			}
			if(!isset($cmd['bootproto'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['bootproto'])){
				return NULL;
			}
			if(strlen($cmd['bootproto']) != 2){
				return NULL;
			}
			if(!isset($cmd['ipaddress'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['ipaddress'])){
				return NULL;
			}
			if(strlen($cmd['ipaddress']) > 30){
				return NULL;
			}
			if(!isset($cmd['netmask'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['netmask'])){
				return NULL;
			}
			if(strlen($cmd['netmask']) > 30){
				return NULL;
			}
			if(!isset($cmd['gateway'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['gateway'])){
				return NULL;
			}
			if(strlen($cmd['gateway']) > 30){
				return NULL;
			}
			if(!isset($cmd['dns1'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['dns1'])){
				return NULL;
			}
			if(strlen($cmd['dns1']) > 30){
				return NULL;
			}
			if(!isset($cmd['dns2'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['dns2'])){
				return NULL;
			}
			if(strlen($cmd['dns2']) > 30){
				return NULL;
			}
			$cmdstr = 'S';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['target'];
			$cmdstr .= $cmd['bootproto'];
			$cmdstr .= $cmd['ipaddress'];
			$cmdstr .= $cmd['netmask'];
			$cmdstr .= $cmd['gateway'];
			$cmdstr .= $cmd['dns1'];
			$cmdstr .= $cmd['dns2'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'91','result'=>'FF');
	}
?>
