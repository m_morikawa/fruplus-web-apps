<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetNetworkConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['target'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['target'])){
				return NULL;
			}
			if(strlen($cmd['target']) != 2){
				return NULL;
			}
			$cmdstr = 'S';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['target'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['bootproto'] = substr($res,5,2);
				$this->m_res['ipaddress'] = substr($res,7,30);
				$this->m_res['netmask'] = substr($res,37,30);
				$this->m_res['gateway'] = substr($res,67,30);
				$this->m_res['dns1'] = substr($res,97,30);
				$this->m_res['dns2'] = substr($res,127,30);
				$this->m_res['macaddress'] = substr($res,157,34);
			}else{
				unset($this->m_res['bootproto']);
				unset($this->m_res['ipaddress']);
				unset($this->m_res['netmask']);
				unset($this->m_res['gateway']);
				unset($this->m_res['dns1']);
				unset($this->m_res['dns2']);
				unset($this->m_res['macaddress']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'90','result'=>'FF','bootproto'=>'','ipaddress'=>'','netmask'=>'','gateway'=>'','dns1'=>'','dns2'=>'','macaddress'=>'');
	}
?>
