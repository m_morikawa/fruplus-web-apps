<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class WriteMemory extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['password'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['password'])){
				return NULL;
			}
			if(strlen($cmd['password']) != 8){
				return NULL;
			}
			if(!isset($cmd['bank'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['bank'])){
				return NULL;
			}
			if(strlen($cmd['bank']) != 2){
				return NULL;
			}
			if(!isset($cmd['offset'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['offset'])){
				return NULL;
			}
			if(strlen($cmd['offset']) != 4){
				return NULL;
			}
			if(!isset($cmd['length'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['length'])){
				return NULL;
			}
			if(strlen($cmd['length']) != 2){
				return NULL;
			}
			if(!isset($cmd['data'])){
				return NULL;
			}
			$target='';
			if(isset($cmd['t_pc'])||isset($cmd['t_epc'])){
				if(!isset($cmd['t_pc'])){
					return NULL;
				}
				if(!ctype_xdigit($cmd['t_pc'])){
					return NULL;
				}
				if(strlen($cmd['t_pc']) != 4){
					return NULL;
				}
				if(!isset($cmd['t_epc'])){
					return NULL;
				}
				if(!ctype_xdigit($cmd['t_epc'])){
					return NULL;
				}
				$target = $cmd['t_pc'].$cmd['t_epc'];
			}

			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['password'];
			$cmdstr .= $cmd['bank'];
			$cmdstr .= $cmd['offset'];
			$cmdstr .= $cmd['length'];
			$cmdstr .= $cmd['data'];
			$cmdstr .= $target;
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 9){
				$this->m_res['result'] = substr($res,3,2);
				$this->m_res['error'] = substr($res,5,4);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'37','result'=>'FF','error'=>'0000');
	}
?>
