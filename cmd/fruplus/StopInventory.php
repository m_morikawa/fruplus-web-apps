<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class StopInventory extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			$this->m_res['error'] = substr($res,5,4);
			if($this->m_res['result'] == '00'){
				$this->m_res['count'] = substr($res,9,2);
				if($this->m_res['count'] != '00'){
					include_once($_SERVER['DOCUMENT_ROOT'] . '/class/TagDataParser.php');
					$this->m_res['taginfo'] = TagDataParser::parse(substr($res,11,strlen($res) - 11));
				}
			}else{
				unset($this->m_res['count']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'21','result'=>'FF','error'=>'0000','count'=>'00');
	}
?>
