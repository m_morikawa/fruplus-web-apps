<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetDIOConf extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['dioconf'] = substr($res,5,2);
			}else{
				unset($this->m_res['dioconf']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'8A','result'=>'FF','dioconf'=>'00');
	}
?>
