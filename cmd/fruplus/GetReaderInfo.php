<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetReaderInfo extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['code'] = substr($res,5,64);
				$this->m_res['info'] = substr($res,69,28);
				$this->m_res['fwver'] = substr($res,97,2);
				$this->m_res['fwlev'] = substr($res,99,2);
			}else{
				unset($this->m_res['code']);
				unset($this->m_res['info']);
				unset($this->m_res['fwver']);
				unset($this->m_res['fwlev']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'81','result'=>'FF','code'=>'','info'=>'','fwver'=>'','fwlev'=>'');
	}
?>
