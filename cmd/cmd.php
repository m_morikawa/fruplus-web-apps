<?php
$responseStatus='HTTP/1.1 200 OK';
include_once($_SERVER['DOCUMENT_ROOT'] . '/class/ModeParameter.php');

$parameter = new ModeParameter();
$manualmode = $parameter->xml()->manualmode;
$COMMAND = array();
$RESPONSE = array();
try{
	//Query
	if($manualmode->httpcommandformat == 'query')
	{
		$COMMAND = $_REQUEST;
	}
	//JSON
	else if($manualmode->httpcommandformat == 'json')
	{
		if($_SERVER["REQUEST_METHOD"] == 'GET'){
			$COMMAND =  json_decode(urldecode($_SERVER["QUERY_STRING"]),true);
			if($COMMAND===NULL||$COMMAND===FALSE){
				throw new Exception('コマンド解析に失敗しました');
			}
		}else if($_SERVER["REQUEST_METHOD"] == 'POST'){
			$COMMAND =  json_decode(file_get_contents("php://input"),true);
			if($COMMAND===NULL||$COMMAND===FALSE){
				throw new Exception('コマンド解析に失敗しました');
			}
		}
	}
	if (isset($COMMAND['type']) && isset($COMMAND['cmd'])){
		spl_autoload_register(function ($class_name) {
			include 'fruplus/'.$class_name.'.php';
		});
		if($COMMAND['type']=='P' || $COMMAND['type']=='S'){
			$cmdobj = NULL;
			if($COMMAND['cmd']=='00'){
				$cmdobj = new OpenReader();
			}else if($COMMAND['cmd']=='01'){
				$cmdobj = new CloseReader();
			}else if($COMMAND['cmd']=='20'){
				$cmdobj = new StartInventory();
			}else if($COMMAND['cmd']=='21'){
				$cmdobj = new StopInventory();
			}else if($COMMAND['cmd']=='22'){
				$cmdobj = new Polling();
			}else if($COMMAND['cmd']=='30'){
				$cmdobj = new WriteEpc();
			}else if($COMMAND['cmd']=='31'){
				$cmdobj = new WriteAccessPassword();
			}else if($COMMAND['cmd']=='32'){
				$cmdobj = new WriteKillPassword();
			}else if($COMMAND['cmd']=='33'){
				$cmdobj = new Lock();
			}else if($COMMAND['cmd']=='34'){
				$cmdobj = new UnLock();
			}else if($COMMAND['cmd']=='35'){
				$cmdobj = new Kill();
			}else if($COMMAND['cmd']=='36'){
				$cmdobj = new ReadMemory();
			}else if($COMMAND['cmd']=='37'){
				$cmdobj = new WriteMemory();
			}else if($COMMAND['cmd']=='52'){
				$cmdobj = new ReadDIO();
			}else if($COMMAND['cmd']=='53'){
				$cmdobj = new WriteDO();
			}else if($COMMAND['cmd']=='80'){
				$cmdobj = new GetReaderInfo();
			}else if($COMMAND['cmd']=='81'){
				$cmdobj = new GetFrequencyChannel();
			}else if($COMMAND['cmd']=='82'){
				$cmdobj = new SetFrequencyChannel();
			}else if($COMMAND['cmd']=='83'){
				$cmdobj = new GetAntennaConf();
			}else if($COMMAND['cmd']=='84'){
				$cmdobj = new SetAntennaConf();
			}else if($COMMAND['cmd']=='85'){
				$cmdobj = new GetEpcMask();
			}else if($COMMAND['cmd']=='86'){
				$cmdobj = new SetEpcMask();
			}else if($COMMAND['cmd']=='87'){
				$cmdobj = new GetCurrentConf();
			}else if($COMMAND['cmd']=='88'){
				$cmdobj = new SetCurrentConf();
			}else if($COMMAND['cmd']=='89'){
				$cmdobj = new GetDefaultConf();
			}else if($COMMAND['cmd']=='8A'){
				$cmdobj = new GetDIOConf();
			}else if($COMMAND['cmd']=='8B'){
				$cmdobj = new SetDIOConf();
			}else if($COMMAND['cmd']=='8C'){
				$cmdobj = new GetTemperature();
			}else if($COMMAND['cmd']=='90'){
				$cmdobj = new GetNetworkConf();
			}else if($COMMAND['cmd']=='91'){
				$cmdobj = new SetNetworkConf();
			}else if($COMMAND['cmd']=='92'){
				$cmdobj = new Disconnect();
			}else if($COMMAND['cmd']=='93'){
				$cmdobj = new Reboot();
			}else if($COMMAND['cmd']=='94'){
				$cmdobj = new GetCurrentMode();
			}else if($COMMAND['cmd']=='95'){
				$cmdobj = new GetDeviceId();
			}
			else{
				throw new Exception('不正なコマンドが送信されました');
			}
			include_once($_SERVER['DOCUMENT_ROOT'] . '/class/LocalBridge.php');
			$localBridge = new LocalBridge("m");
			if(!$localBridge->connect()){
				throw new Exception('RWとの接続に失敗しました');
			}
			$raw_command = $cmdobj->command($COMMAND);
			if($raw_command == NULL){
				throw new Exception('コマンド送信に失敗しました');
			}
			if(!$localBridge->send($raw_command)){
				throw new Exception('コマンド送信に失敗しました');
			}
			if(!$localBridge->receive($raw_response)){
				throw new Exception('コマンド受信に失敗しました');
			}
			$RESPONSE = $cmdobj->response($raw_response);
		}else{
			throw new Exception('不正なコマンドが送信されました');
		}
	}else{
		throw new Exception('不正なコマンドが送信されました');
	}
}catch(Exception $e){
	$responseStatus = 'HTTP/1.1 500 Internal Server Error';
}
if($manualmode->httpresponseformat == 'xml'){
	header("$responseStatus");
	header('Content-Type: application/xml');
	header("X-Content-Type-Options: nosniff");
	include_once($_SERVER['DOCUMENT_ROOT'] . '/class/JAX.php');
	$jax = new JAX();
	echo $jax->array2xml("response",$RESPONSE);
}else if($manualmode->httpresponseformat == 'json'){
	header("$responseStatus");
	header('Content-Type: application/json');
	header("X-Content-Type-Options: nosniff");
	echo json_encode($RESPONSE);
}
?>
