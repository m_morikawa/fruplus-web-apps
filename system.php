<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'SystemParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$parameter = new SystemParameter();
	$systeminfo = $parameter->xml()->system;
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['system_submit'])){
		function checkDateFormat($_date){
			if(!strptime($_date, '%Y/%m/%d')){
				return FALSE;
			}
			$Ymd = explode('/', $_date);
			if(!ctype_digit($Ymd[0])){
				return FALSE;
			}
			if($Ymd[0] < 1900 || 2999 < $Ymd[0]){
				return FALSE;
			}
			if(!ctype_digit($Ymd[1])){
				return FALSE;
			}
			if($Ymd[1] < 1 || 12 < $Ymd[1]){
				return FALSE;
			}
			if(!ctype_digit($Ymd[2])){
				return FALSE;
			}
			if($Ymd[2] < 1 || 31 < $Ymd[2]){
				return FALSE;
			}
			return TRUE;
		}

		function checkTimeFormat($_time){
			if(!strptime($_time, '%H:%M')){
				return FALSE;
			}
			$HM = explode(':', $_time);
			if(!ctype_digit($HM[0])){
				return FALSE;
			}
			if($HM[0] < 0 || 23 < $HM[0]){
				return FALSE;
			}
			if(!ctype_digit($HM[1])){
				return FALSE;
			}
			if($HM[1] < 0 || 59 < $HM[1]){
				return FALSE;
			}
			return TRUE;
		}

		function validateSystem(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'system_id')){
				$msg[] = "端末IDを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['system_id'] !== "0"){
				if(!filter_input(INPUT_POST, 'system_id', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>254)))){
					$msg[] = "端末IDは0から254の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'datetime_date')){
				$msg[] = "端末日付を入力して下さい。";
				$ret = FALSE;
			}
			if(!checkDateFormat($_POST['datetime_date'])){
				$msg[] = "端末日付は'yyyy/MM/dd'の形式で入力してください。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'datetime_time')){
				$msg[] = "端末時刻を入力して下さい。";
				$ret = FALSE;
			}
			if(!checkTimeFormat($_POST['datetime_time'])){
				$msg[] = "端末時刻は'hh:mm'の形式で入力してください。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'ntp_server')){
				$msg[] = "NTPサーバ名を入力して下さい。";
				$ret = FALSE;
			}
			if(strlen($_POST['ntp_server']) > 256){
				$msg[] = "NTPサーバ名は256文字以内で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'ntp_autoupdate')){
				$msg[] = "NTP自動同期の状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['ntp_autoupdate'] !== "true" && $_POST['ntp_autoupdate'] !== "false"){
				$msg[] = "不正なNTP自動同期の状態が入力されています。";
				$ret = FALSE;
			}
			if($_POST['ntp_autoupdate'] === "true"){
				if(!filter_has_var(INPUT_POST, 'ntp_updatetime')){
					$msg[] = "NTP自動同期時刻を入力して下さい。";
					$ret = FALSE;
				}
				if(!checkTimeFormat($_POST['ntp_updatetime'])){
					$msg[] = "NTP自動同期時刻は'hh:mm'の形式で入力してください。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateSystem($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$systeminfo->id[0] = $_POST['system_id'];
			$systeminfo->datetime[0]->date[0] = $_POST['datetime_date'];
			$systeminfo->datetime[0]->time[0] = $_POST['datetime_time'];
			$systeminfo->ntp[0]->server[0] = $_POST['ntp_server'];
			$systeminfo->ntp[0]->autoupdate[0] = $_POST['ntp_autoupdate'];
			if($systeminfo->ntp[0]->autoupdate[0] == "true"){
				$systeminfo->ntp[0]->updatetime[0] = $_POST['ntp_updatetime'];
			}
			$parameter->save();

			exec('sudo frucgi update system',$res);
			$status = json_decode($res[0],true);
			if(strpos($status['status'],"success") !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;

				sleep(1);
				exec('sudo frucgi datetime date',$date);
				exec('sudo frucgi datetime time',$time);
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "システム設定に失敗しました。";
			}
		}
	}else{
		// exec('sudo frucgi datetime date',$date);
		// exec('sudo frucgi datetime time',$time);
		$date[] = '{"datetime":"2016/01/01"}';
		$time[] = '{"datetime":"06:15"}';
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::DEVICE); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>システム設定 > 端末</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary">
<strong>システム設定を変更しました。</strong> <br/>
</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="system.php" class="forms">
					<fieldset>
						<legend><h5>端末情報</h5></legend>
						<section>
							<label>端末ID</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="system_id" value="{$systeminfo->id}" placeholder="{$systeminfo->id}">
EOF;
							?>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>端末時刻</h5></legend>
						<section>
							<label>日付</label>
							<?php
								$nowDate = json_decode($date[0],true);
echo <<< EOF
<input type="text" class="width-4 custom"  name="datetime_date" value="{$nowDate['datetime']}" placeholder="{$nowDate['datetime']}">
EOF;
							?>
						</section>
						<section>
							<label>時間</label>
							<?php
								$nowTime = json_decode($time[0],true);
echo <<< EOF
<input type="text" class="width-4 custom" name="datetime_time" value="{$nowTime['datetime']}" placeholder="{$nowTime['datetime']}">
EOF;
							?>
						</section>
						<section>
							<label>NTPサーバ名</label>
							<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="ntp_server" value="{$systeminfo->ntp->server}" placeholder="{$systeminfo->ntp->server}">
EOF;
							?>
						</section>
						<section>
							<label>NTP自動同期</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" id="ntp_autoupdate" name="ntp_autoupdate">
										<?php
											if ( $systeminfo->ntp->autoupdate == "true"){
												echo "<option value=true selected>有効</option><option value=false>無効</option>";
											} else if ( $systeminfo->ntp->autoupdate == "false"){
												echo "<option value=true>有効</option><option value=false selected>無効</option>";
											}
										?>
									</select>
								</column>
							</row>
							<script>
								function onNtpAutoupdateChanged(){
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("ntpconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("ntpconfig").style.display="none";
									}
								}
								if(document.getElementById("ntp_autoupdate").addEventListener){
									document.getElementById("ntp_autoupdate").addEventListener("change", onNtpAutoupdateChanged, false);
								}else if(document.getElementById("ntp_autoupdate").attachEvent){
									document.getElementById("ntp_autoupdate").attachEvent("onchange", onNtpAutoupdateChanged);
								}else{
									document.getElementById("ntp_autoupdate").onclick = onNtpAutoupdateChanged;
								}
							</script>
						</section>
						<div id="ntpconfig">
							<section>
							<label>同期時刻</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="ntp_updatetime" value="{$systeminfo->ntp->updatetime}" placeholder="{$systeminfo->ntp->updatetime}">
EOF;
								?>
						</section>
						<?php
							if ( $systeminfo->ntp->autoupdate == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("ntpconfig").style.display="block";
</script>
EOF;
							} else if ( $systeminfo->ntp->autoupdate == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("ntpconfig").style.display="none";
</script>
EOF;
							}
						?>
						</div>
					</fieldset>
					<div style="padding-bottom:16px;">
						<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
						<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
							ここで設定する内容は、変更すると即座にリーダーに反映されます。
						</div>
					</div>
					<section>
						<button class="primary" name="system_submit">変更する</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
</body>
</html>
