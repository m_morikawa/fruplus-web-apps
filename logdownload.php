<?php
	if(!isset($_GET['logtype'])){
		exit(1);
	}
	if($_GET['logtype'] == 'system'){
		$filepath = '/var/log/messages';
		$cmd = "sudo chmod 604 %s";
		exec(sprintf($cmd, $filepath), $msg);
		$filename = 'systemlog.txt';
	}else if($_GET['logtype'] == 'app'){
		$filepath = '/var/log/applog.log';
		$filename = 'applog.txt';
	}else if($_GET['logtype'] == 'sendaction'){
		exec('sudo frucgi log sendaction save', $msg);
		$filepath = '/var/log/sendaction.log';
		$filename = 'actionlog.txt';
	}else if($_GET['logtype'] == 'network'){
		exec('sudo frucgi log network save', $msg);
		$filepath = '/var/log/network.log';
		$filename = 'networklog.txt';
	}else{
		exit(1);
	}
	header('Content-Type: text/plain');
	header('Content-Disposition: attachment; filename='.$filename);
	header('Pragma: no-cache');
	header('Cache-Control: no-cache');
	readfile($filepath);
	exit(0);
?>
