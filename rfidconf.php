<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('rfidConf');
	list($langFlag,$validate_translation) = loadTranslation('rfidConf_validate');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$has4Port = 0;
	if($model->antennaPortCount() == 4){
		$has4Port = 1;
	}

	$isUpdateFormSeen = 0;

	if(isset($_POST['userForm_submit'])){
		$isUpdateFormSeen = 1;
	}

	$rfidConf = json_decode(json_encode($config->xml()->autoMode->rfidConf),TRUE);
?>
<!DOCTYPE html>
<html lang="<?php echo $langFlag;?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>

function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<span  style="float: right;">
			<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
			<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
				<option v-for="language in languages" v-bind:value="language.value">
					{{language.name}}
				</option>
			</select>
		</span>
	</div>

	<div id="contents">
  		<div id="contentsInner">
		  <?php require_once('function/put_nav.php'); put_nav(NavState::RFIDCONF); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo COMMON_UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>	
				<form method="post" action="rfidconf.php" name="userForm"  class="forms" @submit.prevent="onSubmit">
					<fieldset>
						<legend><h5><?php echo HEADER_ANTENNASET; ?></h5></legend>
						<section>
							<label><?php echo ANTENNA_0." ".ANTENNA_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="antennaSet_antenna_0_enable" @change="fetchAntenna0" class="select custom" class="width-10">
										<option v-for="option in useOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isAntenna0ConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ANTENNA_0." ".ANTENNA_POWER; ?></label>
								<input type="text" class="width-6 custom" v-model="antennaSet_antenna_0_power" >
							</section>
							<section>
								<label><?php echo ANTENNA_0." ".ANTENNA_DWELL; ?></label>
								<input type="text" class="width-4 custom" v-model="antennaSet_antenna_0_dwell">
							</section>
							<section>
								<label><?php echo ANTENNA_0." ".ANTENNA_INVCNT; ?></label>
								<input type="text" class="width-4 custom" v-model="antennaSet_antenna_0_invCnt">
							</section>
						</div>
						<section>
							<label><?php echo ANTENNA_1." ".ANTENNA_ENABLE; ?></label>
							<row>
								<column cols="6">
									<select v-model="antennaSet_antenna_1_enable" @change="fetchAntenna1" class="select custom" class="width-10">
										<option v-for="option in useOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
						<div v-show="isAntenna1ConfigSeen" class="configcontainer">
							<section>
								<label><?php echo ANTENNA_1." ".ANTENNA_POWER; ?></label>
								<input type="text" class="width-6 custom" v-model="antennaSet_antenna_1_power" >
							</section>
							<section>
								<label><?php echo ANTENNA_1." ".ANTENNA_DWELL; ?></label>
								<input type="text" class="width-4 custom" v-model="antennaSet_antenna_1_dwell">
							</section>
							<section>
								<label><?php echo ANTENNA_1." ".ANTENNA_INVCNT; ?></label>
								<input type="text" class="width-4 custom" v-model="antennaSet_antenna_1_invCnt">
							</section>
						</div>
						<section v-show="is4PortReader">
							<section>
								<label><?php echo ANTENNA_2." ".ANTENNA_ENABLE; ?></label>
								<row>
									<column cols="6">
										<select v-model="antennaSet_antenna_2_enable" @change="fetchAntenna2" class="select custom" class="width-10">
											<option v-for="option in useOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<div v-show="isAntenna2ConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ANTENNA_2." ".ANTENNA_POWER; ?></label>
									<input type="text" class="width-6 custom" v-model="antennaSet_antenna_2_power" >
								</section>
								<section>
									<label><?php echo ANTENNA_2." ".ANTENNA_DWELL; ?></label>
									<input type="text" class="width-4 custom" v-model="antennaSet_antenna_2_dwell">
								</section>
								<section>
									<label><?php echo ANTENNA_2." ".ANTENNA_INVCNT; ?></label>
									<input type="text" class="width-4 custom" v-model="antennaSet_antenna_2_invCnt">
								</section>
							</div>
							<section>
								<label><?php echo ANTENNA_3." ".ANTENNA_ENABLE; ?></label>
								<row>
									<column cols="6">
										<select v-model="antennaSet_antenna_3_enable" @change="fetchAntenna3" class="select custom" class="width-10">
											<option v-for="option in useOptions" v-bind:value="option.value">
												{{option.name}}
											</option>
										</select>
									</column>
								</row>
							</section>
							<div v-show="isAntenna3ConfigSeen" class="configcontainer">
								<section>
									<label><?php echo ANTENNA_3." ".ANTENNA_POWER; ?></label>
									<input type="text" class="width-6 custom" v-model="antennaSet_antenna_3_power" >
								</section>
								<section>
									<label><?php echo ANTENNA_3." ".ANTENNA_DWELL; ?></label>
									<input type="text" class="width-4 custom" v-model="antennaSet_antenna_3_dwell">
								</section>
								<section>
									<label><?php echo ANTENNA_3." ".ANTENNA_INVCNT; ?></label>
									<input type="text" class="width-4 custom" v-model="antennaSet_antenna_3_invCnt">
								</section>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5><?php echo HEADER_FREQUENCY; ?></h5></legend>
						<section>
							<label><?php echo ITEM_FREQUENCY; ?></label>
							<row>
								<column cols="6">
									<select v-model="frequency" class="select custom" class="width-10">
										<option v-for="option in frequencyOptions" v-bind:value="option.value">
											{{option.name}}
										</option>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<section>
						<button class="primary"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button" v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button"><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var validate_translation_config = JSON.parse('<?php echo json_encode($validate_translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($rfidConf,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"ja"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		useOptions:[
			{name:translation_config["OPTION_USE"],value:true},
			{name:translation_config["OPTION_NOTUSE"],value:false},
		],
		frequencyOptions:[
			{name:"LBT Scan(ALL)",value:0},
			{name:"CH5 (916.8MHz)",value:5},
			{name:"CH11(918.0MHz)",value:11},
			{name:"CH17(919.2MHz)",value:17},
			{name:"CH23(920.4MHz)",value:23},
			{name:"CH24(920.6MHz)",value:24},
			{name:"CH25(920.8MHz)",value:25},
			{name:"CH26(921.0MHz)",value:26},
			{name:"CH27(921.2MHz)",value:27},
			{name:"CH28(921.4MHz)",value:28},
			{name:"CH29(921.6MHz)",value:29},
			{name:"CH30(921.8MHz)",value:30},
			{name:"CH31(922.0MHz)",value:31},
			{name:"CH32(922.2MHz)",value:32},
			{name:"LBT Scan(CH26-H32)",value:255},
		],
		antennaSet_antenna_0_enable:current_config["antennaSet"]["antenna"][0]["enable"] == "true",
		antennaSet_antenna_0_power:current_config["antennaSet"]["antenna"][0]["power"],
		antennaSet_antenna_0_dwell:current_config["antennaSet"]["antenna"][0]["dwell"],
		antennaSet_antenna_0_invCnt:current_config["antennaSet"]["antenna"][0]["invCnt"],
		antennaSet_antenna_1_enable:current_config["antennaSet"]["antenna"][1]["enable"] == "true",
		antennaSet_antenna_1_power:current_config["antennaSet"]["antenna"][1]["power"],
		antennaSet_antenna_1_dwell:current_config["antennaSet"]["antenna"][1]["dwell"],
		antennaSet_antenna_1_invCnt:current_config["antennaSet"]["antenna"][1]["invCnt"],
		antennaSet_antenna_2_enable:current_config["antennaSet"]["antenna"][2]["enable"] == "true",
		antennaSet_antenna_2_power:current_config["antennaSet"]["antenna"][2]["power"],
		antennaSet_antenna_2_dwell:current_config["antennaSet"]["antenna"][2]["dwell"],
		antennaSet_antenna_2_invCnt:current_config["antennaSet"]["antenna"][2]["invCnt"],
		antennaSet_antenna_3_enable:current_config["antennaSet"]["antenna"][3]["enable"] == "true",
		antennaSet_antenna_3_power:current_config["antennaSet"]["antenna"][3]["power"],
		antennaSet_antenna_3_dwell:current_config["antennaSet"]["antenna"][3]["dwell"],
		antennaSet_antenna_3_invCnt:current_config["antennaSet"]["antenna"][3]["invCnt"],
		frequency:current_config["frequency"],
		isAntenna0ConfigSeen:false,
		isAntenna1ConfigSeen:false,
		isAntenna2ConfigSeen:false,
		isAntenna3ConfigSeen:false,
		is4PortReader:<?php echo $has4Port; ?>,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:0,
		errorMessage:""
	},
	methods: {
		fetchAntenna0:function(){
			this.isAntenna0ConfigSeen = this.antennaSet_antenna_0_enable;
		},
		fetchAntenna1:function(){
			this.isAntenna1ConfigSeen = this.antennaSet_antenna_1_enable;
		},
		fetchAntenna2:function(){
			this.isAntenna2ConfigSeen = this.antennaSet_antenna_2_enable;
		},
		fetchAntenna3:function(){
			this.isAntenna3ConfigSeen = this.antennaSet_antenna_3_enable;
		},
		toCurrent:function(){
			this.antennaSet_antenna_0_enable = current_config["antennaSet"]["antenna"][0]["enable"] == "true";
			this.antennaSet_antenna_0_power = current_config["antennaSet"]["antenna"][0]["power"];
			this.antennaSet_antenna_0_dwell = current_config["antennaSet"]["antenna"][0]["dwell"];
			this.antennaSet_antenna_0_invCnt = current_config["antennaSet"]["antenna"][0]["invCnt"];
			this.antennaSet_antenna_1_enable = current_config["antennaSet"]["antenna"][1]["enable"] == "true";
			this.antennaSet_antenna_1_power = current_config["antennaSet"]["antenna"][1]["power"];
			this.antennaSet_antenna_1_dwell = current_config["antennaSet"]["antenna"][1]["dwell"];
			this.antennaSet_antenna_1_invCnt = current_config["antennaSet"]["antenna"][1]["invCnt"];
			this.antennaSet_antenna_2_enable = current_config["antennaSet"]["antenna"][2]["enable"] == "true";
			this.antennaSet_antenna_2_power = current_config["antennaSet"]["antenna"][2]["power"];
			this.antennaSet_antenna_2_dwell = current_config["antennaSet"]["antenna"][2]["dwell"];
			this.antennaSet_antenna_2_invCnt = current_config["antennaSet"]["antenna"][2]["invCnt"];
			this.antennaSet_antenna_3_enable = current_config["antennaSet"]["antenna"][3]["enable"] == "true";
			this.antennaSet_antenna_3_power = current_config["antennaSet"]["antenna"][3]["power"];
			this.antennaSet_antenna_3_dwell = current_config["antennaSet"]["antenna"][3]["dwell"];
			this.antennaSet_antenna_3_invCnt = current_config["antennaSet"]["antenna"][3]["invCnt"];
			this.frequency = current_config["frequency"];
			this.fetchAntenna0();
			this.fetchAntenna1();
			this.fetchAntenna2();
			this.fetchAntenna3();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		onSubmit: function () {
			axios.post('./api/v1/commands.php', {
				"name":"setRfidConf",
				"parameters" : {
					"rfidConf" : {
						"antennaSet" : [
							{
								"antenna" : {
									"port" : 0,
									"enable" : this.antennaSet_antenna_0_enable,
									"power" : this.antennaSet_antenna_0_power,
									"dwell" : this.antennaSet_antenna_0_dwell,
									"invCnt" : this.antennaSet_antenna_0_invCnt
								}
							},
							{
								"antenna" : {
									"port" : 1,
									"enable" : this.antennaSet_antenna_1_enable,
									"power" : this.antennaSet_antenna_1_power,
									"dwell" : this.antennaSet_antenna_1_dwell,
									"invCnt" : this.antennaSet_antenna_1_invCnt
								}
							}
							<?php
								if($has4Port){
echo <<< EOF
,
{
	"antenna" : {
		"port" : 2,
		"enable" : this.antennaSet_antenna_2_enable,
		"power" : this.antennaSet_antenna_2_power,
		"dwell" : this.antennaSet_antenna_2_dwell,
		"invCnt" : this.antennaSet_antenna_2_invCnt
	}
},
{
	"antenna" : {
		"port" : 3,
		"enable" : this.antennaSet_antenna_3_enable,
		"power" : this.antennaSet_antenna_3_power,
		"dwell" : this.antennaSet_antenna_3_dwell,
		"invCnt" : this.antennaSet_antenna_3_invCnt
	}
}
EOF;
								}
							?>
						],
						"frequency":this.frequency
					}
				
				}
			})
			.then(response=>{
				this.isErrorLabelSeen = false;
				var rand = Math.floor( Math.random() * 9998 ) + 1 ;
				var script = document.createElement('script');
				script.id = "beforeSubmit";
				script.src = './js/beforesubmit.php?rand='+rand;
				script.type = "text/javascript";
				beforeSubmit = undefined;
				if(script.addEventListener){
					script.addEventListener("load", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.userForm.submit();
						}
					}, false);
				}else if(script.attachEvent){
					script.attachEvent("onload", function(){
						if(typeof beforeSubmit == 'function'){
							beforeSubmit();
							document.uuserForm.submit();
						}
					});
					script.onreadystatechange = function(){
						if(this.readyState=="loaded"||this.readyState=="complete"){
							if(typeof beforeSubmit == 'function'){
								beforeSubmit();
								document.userForm.submit();
							}
						}
					}
				}
				document.userForm.appendChild(script);
			})
			.catch(error=>{
				this.isErrorLabelSeen = true;
				this.errorMessage = error.response.data.error.detail + "<br>" + validate_translation_config[error.response.data.error.detail];
				window.scroll(0,0);
			});
		}
	},
	created:function(){
		this.fetchAntenna0();
		this.fetchAntenna1();
		this.fetchAntenna2();
		this.fetchAntenna3();
	},
	computed: {

	}
});

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById('btn_do_update') != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>
