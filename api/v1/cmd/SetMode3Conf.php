<?php
	class SetMode3Conf extends CommandBase
	{
		private $sendData = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setMode3Conf";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check SendData
			if(!isset($this->parameters['mode3Conf'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_MISSING";
				return FALSE;
			}

			//Check Event
			if(!isset($this->parameters['mode3Conf']['event'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_EVENT_MISSING";
				return FALSE;
			}

			//Check Event In
			if(!isset($this->parameters['mode3Conf']['event']['in'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_EVENT_IN_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['event']['in'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_EVENT_IN_INVALID";
				return FALSE;
			}

			//Check Event Out
			if(!isset($this->parameters['mode3Conf']['event']['out'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_EVENT_OUT_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['event']['out'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_EVENT_OUT_INVALID";
				return FALSE;
			}

			//Check Event Timeout
			if(!isset($this->parameters['mode3Conf']['event']['timeout'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_EVENT_TIMEOUT_MISSING";
				return FALSE;
			}
			
			if(filter_var($this->parameters['mode3Conf']['event']['timeout'],
				FILTER_VALIDATE_INT,
				array('options' => array("min_range"=>100,"max_range"=>60000))) === FALSE)
			{
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_EVENT_TIMEOUT_OUTRANGE";
				return FALSE;
			}

			//Check Event DataCond
			if(!isset($this->parameters['mode3Conf']['event']['data'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_EVENT_DATA_MISSING";
				return FALSE;
			}
			if($this->parameters['mode3Conf']['event']['data'] !== "change" &&
				$this->parameters['mode3Conf']['event']['data'] !== "all"){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_EVENT_DATA_INVALID";
				return FALSE;
			}

			//Check Monitor
			if(!isset($this->parameters['mode3Conf']['monitor'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_MONITOR_MISSING";
				return FALSE;
			}

			//Check Monitor Enable
			if(!isset($this->parameters['mode3Conf']['monitor']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_MONITOR_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['monitor']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_MONITOR_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode3Conf']['monitor']['enable']){
				//Check Monitor Interval
				if(!isset($this->parameters['mode3Conf']['monitor']['interval'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_MONITOR_INTERVAL_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode3Conf']['monitor']['interval'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>3600))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_MONITOR_INTERVAL_OUTRANGE";
					return FALSE;
				}
			}

			//Check Dout
			if(!isset($this->parameters['mode3Conf']['dout'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_DOUT_MISSING";
				return FALSE;
			}

			//Check Dout In
			if(!isset($this->parameters['mode3Conf']['dout']['in'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_DOUT_IN_MISSING";
				return FALSE;
			}

			//Check Dout In Enable
			if(!isset($this->parameters['mode3Conf']['dout']['in']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_DOUT_IN_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['dout']['in']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_DOUT_IN_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode3Conf']['dout']['in']['enable']){
				//Check Dout In Port
				if(!isset($this->parameters['mode3Conf']['dout']['in']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_DOUT_IN_PORT_MISSING";
					return FALSE;
				}
				if(intval($this->parameters['mode3Conf']['dout']['in']['port']) !== 0 &&
					intval($this->parameters['mode3Conf']['dout']['in']['port']) !== 1){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_DOUT_IN_PORT_INVALID";
					return FALSE;
				}

				//Check Dout In HoldTime
				if(!isset($this->parameters['mode3Conf']['dout']['in']['holdTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_DOUT_IN_HOLDTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode3Conf']['dout']['in']['holdTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_DOUT_IN_HOLDTIME_OUTRANGE";
					return FALSE;
				}
			}

			//Check Dout Out
			if(!isset($this->parameters['mode3Conf']['dout']['out'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_DOUT_OUT_MISSING";
				return FALSE;
			}

			//Check Dout In Enable
			if(!isset($this->parameters['mode3Conf']['dout']['out']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_DOUT_OUT_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['dout']['out']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_DOUT_OUT_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode3Conf']['dout']['out']['enable']){
				//Check Dout In Port
				if(!isset($this->parameters['mode3Conf']['dout']['out']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_DOUT_OUT_PORT_MISSING";
					return FALSE;
				}
				if(intval($this->parameters['mode3Conf']['dout']['out']['port']) !== 0 &&
					intval($this->parameters['mode3Conf']['dout']['out']['port']) !== 1){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_DOUT_OUT_PORT_INVALID";
					return FALSE;
				}

				//Check Dout In HoldTime
				if(!isset($this->parameters['mode3Conf']['dout']['out']['holdTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_DOUT_OUT_HOLDTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode3Conf']['dout']['out']['holdTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_DOUT_OUT_HOLDTIME_OUTRANGE";
					return FALSE;
				}
			}

			//Check Response
			if(!isset($this->parameters['mode3Conf']['response'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_RESPONSE_MISSING";
				return FALSE;
			}

			//Check Response Enable
			if(!isset($this->parameters['mode3Conf']['response']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_RESPONSE_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['response']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_RESPONSE_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode3Conf']['response']['enable']){
				//Check Response Timeout
				if(!isset($this->parameters['mode3Conf']['response']['timeout'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE3CONF_RESPONSE_TIMEOUT_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode3Conf']['response']['timeout'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE3CONF_RESPONSE_TIMEOUT_OUTRANGE";
					return FALSE;
				}
			}

			//Check Request
			if(!isset($this->parameters['mode3Conf']['request'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_REQUEST_MISSING";
				return FALSE;
			}

			//Check Request Enable
			if(!isset($this->parameters['mode3Conf']['request']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE3CONF_REQUEST_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode3Conf']['request']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE3CONF_REQUEST_ENABLE_INVALID";
				return FALSE;
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$mode3Conf = $xml->autoMode->mode3Conf;
			$mode3Conf->event->in = var_export($this->parameters['mode3Conf']['event']['in'],true);
			$mode3Conf->event->out = var_export($this->parameters['mode3Conf']['event']['out'],true);
			$mode3Conf->event->timeout = $this->parameters['mode3Conf']['event']['timeout'];
			$mode3Conf->event->data = $this->parameters['mode3Conf']['event']['data'];
			$mode3Conf->monitor->enable = var_export($this->parameters['mode3Conf']['monitor']['enable'],true);
			if($this->parameters['mode3Conf']['monitor']['enable']){
				$mode3Conf->monitor->interval = $this->parameters['mode3Conf']['monitor']['interval'];
			}
			$mode3Conf->dout->in->enable = var_export($this->parameters['mode3Conf']['dout']['in']['enable'],true);
			if($this->parameters['mode3Conf']['dout']['in']['enable']){
				$mode3Conf->dout->in->port = $this->parameters['mode3Conf']['dout']['in']['port'];
				$mode3Conf->dout->in->holdTime = $this->parameters['mode3Conf']['dout']['in']['holdTime'];
			}
			$mode3Conf->dout->out->enable = var_export($this->parameters['mode3Conf']['dout']['out']['enable'],true);
			if($this->parameters['mode3Conf']['dout']['out']['enable']){
				$mode3Conf->dout->in->port = $this->parameters['mode3Conf']['dout']['out']['port'];
				$mode3Conf->dout->in->holdTime = $this->parameters['mode3Conf']['dout']['out']['holdTime'];
			}
			$mode3Conf->response->enable = var_export($this->parameters['mode3Conf']['response']['enable'],true);
			if($this->parameters['mode3Conf']['response']['enable']){
				$mode3Conf->response->timeout = $this->parameters['mode3Conf']['response']['timeout'];
			}
			$mode3Conf->request->enable = var_export($this->parameters['mode3Conf']['request']['enable'],true);

			$xml->asXml($configPath);
			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
