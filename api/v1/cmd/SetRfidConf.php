<?php
	class SetRfidConf extends CommandBase
	{
		private $sendData = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setRfidConf";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			require_once('../../class/Model.php');
			$model = new Model();

			//Check RfidConf
			if(!isset($this->parameters['rfidConf'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "RFIDCONF_MISSING";
				return FALSE;
			}

			//Check AntennaSet
			if(!isset($this->parameters['rfidConf']['antennaSet'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "RFIDCONF_ANTENNASET_MISSING";
				return FALSE;
			}
			if(count($this->parameters['rfidConf']['antennaSet']) !== $model->antennaPortCount()){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "RFIDCONF_ANTENNASET_INVALID_COUNT";
				return FALSE;
			}

			$antennas = array();
			//Check Antenna
			foreach($this->parameters['rfidConf']['antennaSet'] as $antenna){
				//Check Port
				if(!isset($antenna['antenna']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "RFIDCONF_ANTENNASET_PORT_MISSING";
					return FALSE;
				}
				if(filter_var($antenna['antenna']['port'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>$model->antennaPortCount() - 1))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_PORT_OUTRANGE";
					return FALSE;
				}

				$port = $antenna['antenna']['port'];
				//Check Enable
				if(!isset($antenna['antenna']['enable'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_ENABLE_MISSING";
					return FALSE;
				}
				if(filter_var($antenna['antenna']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_ENABLE_INVALID";
					return FALSE;
				}

				//Check Power
				if(!isset($antenna['antenna']['power'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_POWER_MISSING";
					return FALSE;
				}
				if(filter_var($antenna['antenna']['power'],
					FILTER_VALIDATE_INT,
					array('options' => $model->rfPowerRange())) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_POWER_OUTRANGE";
					return FALSE;
				}

				//Check Dwell
				if(!isset($antenna['antenna']['dwell'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_DWELL_MISSING";
					return FALSE;
				}
				if(filter_var($antenna['antenna']['dwell'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>65535))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_DWELL_OUTRANGE";
					return FALSE;
				}

				//Check InvCnt
				if(!isset($antenna['antenna']['invCnt'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_INVCNT_MISSING";
					return FALSE;
				}
				if(filter_var($antenna['antenna']['invCnt'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>65535))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_INVCNT_OUTRANGE";
					return FALSE;
				}

				//Check Dwell&InvCnt
				if($antenna['antenna']['dwell'] === 0 && $antenna['antenna']['invCnt'] === 0){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "RFIDCONF_ANTENNASET_ANTENNA".$port."_DWELL_INVCNT_INVALID";
					return FALSE;
				}

				$antennas[$port] = $antenna;
				unset($antenna);
			}

			//Check Frequency
			if(!isset($this->parameters['rfidConf']['frequency'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "RFIDCONF_FREQUENCY_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['rfidConf']['frequency'],
				FILTER_VALIDATE_INT,
				array('options' => array("min_range"=>0,"max_range"=>255))) === FALSE)
			{
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "RFIDCONF_FREQUENCY_OUTRANGE";
				return FALSE;
			}
			require_once("../../function/check_ch.php");
			if(filter_var($this->parameters['rfidConf']['frequency'], FILTER_CALLBACK, array('options' => 'check_ch')) === FALSE){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "RFIDCONF_FREQUENCY_INVALID";
				return FALSE;
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);

			$rfidConf = $xml->autoMode->rfidConf;
			for($i = 0;$i < count($rfidConf->antennaSet->antenna);$i++){
				$port = intval($rfidConf->antennaSet->antenna[$i]->attributes()->port);
				
				if(array_key_exists($port,$antennas)){
					$rfidConf->antennaSet->antenna[$i]->enable = var_export($antennas[$port]['antenna']['enable'],true);
					$rfidConf->antennaSet->antenna[$i]->power = $antennas[$port]['antenna']['power'];
					$rfidConf->antennaSet->antenna[$i]->dwell = $antennas[$port]['antenna']['dwell'];
					$rfidConf->antennaSet->antenna[$i]->invCnt = $antennas[$port]['antenna']['invCnt'];
				}
			}
			$xml->asXml($configPath);
			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
