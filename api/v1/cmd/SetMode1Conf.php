<?php
	class SetMode1Conf extends CommandBase
	{
		private $sendData = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setMode1Conf";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check SendData
			if(!isset($this->parameters['mode1Conf'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_MISSING";
				return FALSE;
			}

			//Check MultiEpcCheck
			if(!isset($this->parameters['mode1Conf']['multiEpcCheck'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_MISSING";
				return FALSE;
			}

			//Check MultiEpcCheck Enable
			if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode1Conf']['multiEpcCheck']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode1Conf']['multiEpcCheck']['enable']){
				//Check ScanReset
				if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_MISSING";
					return FALSE;
				}

				//Check ScanReset ByTrigger
				if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_MISSING";
					return FALSE;
				}

				//Check ScanReset ByTrigger Enable
				if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['enable'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_ENABLE_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_ENABLE_INVALID";
					return FALSE;
				}

				if($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['enable']){
					//Check ScanReset ByTrigger Port
					if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['port'])){
						$this->errorCode = CommandBase::MISSINGPARAMETER;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_PORT_MISSING";
						return FALSE;
					}
					if(intval($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['port']) !== 0 &&
						intval($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['port']) !== 1){
						$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_PORT_INVALID";
						return FALSE;
					}

					//Check ScanReset ByTrigger Trigger
					if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['trigger'])){
						$this->errorCode = CommandBase::MISSINGPARAMETER;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_TRIGGER_MISSING";
						return FALSE;
					}
					if($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['trigger'] !== 'up' &&
						$this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['trigger'] !== 'down'){
						$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYTRIGGER_TRIGGER_INVALID";
						return FALSE;
					}
				}
				
				//Check ScanReset ByInterval
				if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYINTERVAL_MISSING";
					return FALSE;
				}

				//Check ScanReset ByInterval Enable
				if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['enable'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYINTERVAL_ENABLE_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYINTERVAL_ENABLE_INVALID";
					return FALSE;
				}

				if($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['enable']){
					//Check ScanReset ByInterval Interval
					if(!isset($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['interval'])){
						$this->errorCode = CommandBase::MISSINGPARAMETER;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYINTERVAL_INTERVAL_MISSING";
						return FALSE;
					}
					if(filter_var($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['interval'],
						FILTER_VALIDATE_INT,
						array('options' => array("min_range"=>300,"max_range"=>3600000))) === FALSE)
					{
						$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
						$this->errorDetail = "MODE1CONF_MULTIEPCCHECK_SCANRESET_BYINTERVAL_INTERVAL_OUTRANGE";
						return FALSE;
					}
				}
			}

			//Check Dout
			if(!isset($this->parameters['mode1Conf']['dout'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_DOUT_MISSING";
				return FALSE;
			}

			//Check Dout Enable
			if(!isset($this->parameters['mode1Conf']['dout']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_DOUT_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode1Conf']['dout']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE1CONF_DOUT_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode1Conf']['dout']['enable']){
				//Check Dout Port
				if(!isset($this->parameters['mode1Conf']['dout']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_DOUT_PORT_MISSING";
					return FALSE;
				}
				if(intval($this->parameters['mode1Conf']['dout']['port']) !== 0 &&
					intval($this->parameters['mode1Conf']['dout']['port']) !== 1){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE1CONF_DOUT_PORT_INVALID";
					return FALSE;
				}

				//Check Dout HoldTime
				if(!isset($this->parameters['mode1Conf']['dout']['holdTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_DOUT_HOLDTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode1Conf']['dout']['holdTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE1CONF_DOUT_HOLDTIME_OUTRANGE";
					return FALSE;
				}
			}

			//Check Response
			if(!isset($this->parameters['mode1Conf']['response'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_RESPONSE_MISSING";
				return FALSE;
			}

			//Check Response Enable
			if(!isset($this->parameters['mode1Conf']['response']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_RESPONSE_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode1Conf']['response']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE1CONF_RESPONSE_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode1Conf']['response']['enable']){
				//Check Response Timeout
				if(!isset($this->parameters['mode1Conf']['response']['timeout'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE1CONF_RESPONSE_TIMEOUT_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode1Conf']['response']['timeout'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE1CONF_RESPONSE_TIMEOUT_OUTRANGE";
					return FALSE;
				}
			}

			//Check Request
			if(!isset($this->parameters['mode1Conf']['request'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_REQUEST_MISSING";
				return FALSE;
			}

			//Check Request Enable
			if(!isset($this->parameters['mode1Conf']['request']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE1CONF_REQUEST_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode1Conf']['request']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE1CONF_REQUEST_ENABLE_INVALID";
				return FALSE;
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$mode1Conf = $xml->autoMode->mode1Conf;
			$mode1Conf->multiEpcCheck->enable = var_export($this->parameters['mode1Conf']['multiEpcCheck']['enable'],true);
			if($this->parameters['mode1Conf']['multiEpcCheck']['enable']){
				$mode1Conf->multiEpcCheck->scanReset->byTrigger->enable = var_export($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['enable'],true);
				if($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['enable']){
					$mode1Conf->multiEpcCheck->scanReset->byTrigger->port = $this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['port'];
					$mode1Conf->multiEpcCheck->scanReset->byTrigger->trigger = $this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byTrigger']['trigger'];
				}
				$mode1Conf->multiEpcCheck->scanReset->byInterval->enable = var_export($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['enable'],true);
				if($this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['enable']){
					$mode1Conf->multiEpcCheck->scanReset->byInterval->interval = $this->parameters['mode1Conf']['multiEpcCheck']['scanReset']['byInterval']['interval'];
				}
			}
			$mode1Conf->dout->enable = var_export($this->parameters['mode1Conf']['dout']['enable'],true);
			if($this->parameters['mode1Conf']['dout']['enable']){
				$mode1Conf->dout->port = $this->parameters['mode1Conf']['dout']['port'];
				$mode1Conf->dout->downTime = $this->parameters['mode1Conf']['dout']['downTime'];
			}
			$mode1Conf->response->enable = var_export($this->parameters['mode1Conf']['response']['enable'],true);
			if($this->parameters['mode1Conf']['response']['enable']){
				$mode1Conf->response->timeout = $this->parameters['mode1Conf']['response']['timeout'];
			}
			$mode1Conf->request->enable = var_export($this->parameters['mode1Conf']['request']['enable'],true);
			$xml->asXml($configPath);

			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
