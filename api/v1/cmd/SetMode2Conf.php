<?php
	class SetMode2Conf extends CommandBase
	{
		private $sendData = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setMode2Conf";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check SendData
			if(!isset($this->parameters['mode2Conf'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_MISSING";
				return FALSE;
			}

			//Check MultiEpcCheck
			if(!isset($this->parameters['mode2Conf']['multiEpcCheck'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_MULTIEPCCHECK_MISSING";
				return FALSE;
			}

			//Check MultiEpcCheck Enable
			if(!isset($this->parameters['mode2Conf']['multiEpcCheck']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_MULTIEPCCHECK_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode2Conf']['multiEpcCheck']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_MULTIEPCCHECK_ENABLE_INVALID";
				return FALSE;
			}

			//Check Inventory
			if(!isset($this->parameters['mode2Conf']['inventory'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_INVENTORY_MISSING";
				return FALSE;
			}

			//Check Inventory Port
			if(!isset($this->parameters['mode2Conf']['inventory']['port'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_INVENTORY_PORT_MISSING";
				return FALSE;
			}
			if(intval($this->parameters['mode2Conf']['inventory']['port']) !== 0 &&
				intval($this->parameters['mode2Conf']['inventory']['port']) !== 1){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_INVENTORY_PORT_INVALID";
				return FALSE;
			}

			//Check Inventory Trigger
			if(!isset($this->parameters['mode2Conf']['inventory']['trigger'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_INVENTORY_TRIGGER_MISSING";
				return FALSE;
			}
			if($this->parameters['mode2Conf']['inventory']['trigger'] !== 'up' &&
				$this->parameters['mode2Conf']['inventory']['trigger'] !== 'down'){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_INVENTORY_TRIGGER_INVALID";
				return FALSE;
			}

			//Check Inventory ScanMode
			if(!isset($this->parameters['mode2Conf']['inventory']['scanMode'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE_MISSING";
				return FALSE;
			}
			if(intval($this->parameters['mode2Conf']['inventory']['scanMode']) !== 1 &&
				intval($this->parameters['mode2Conf']['inventory']['scanMode']) !== 2){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE_INVALID";
				return FALSE;
			}

			if(intval($this->parameters['mode2Conf']['inventory']['scanMode']) === 1){
				//Check Inventory ScanMode1
				if(!isset($this->parameters['mode2Conf']['inventory']['scanMode1'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE1_MISSING";
					return FALSE;
				}
				//Check ScanReset ScanMode1 Timeout
				if(!isset($this->parameters['mode2Conf']['inventory']['scanMode1']['timeout'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE1_TIMEOUT_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode2Conf']['inventory']['scanMode1']['timeout'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE1_TIMEOUT_OUTRANGE";
					return FALSE;
				}
			}

			if(intval($this->parameters['mode2Conf']['inventory']['scanMode']) === 2){
				//Check Inventory ScanMode2
				if(!isset($this->parameters['mode2Conf']['inventory']['scanMode2'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE2_MISSING";
					return FALSE;
				}

				//Check ScanReset ScanMode2 DelayTime
				if(!isset($this->parameters['mode2Conf']['inventory']['scanMode2']['delayTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE2_DELAYTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode2Conf']['inventory']['scanMode2']['delayTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE2_DELAYTIME_OUTRANGE";
					return FALSE;
				}

				//Check ScanReset ScanMode2 ScanTime
				if(!isset($this->parameters['mode2Conf']['inventory']['scanMode2']['scanTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE2_SCANTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode2Conf']['inventory']['scanMode2']['scanTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_INVENTORY_SCANMODE2_SCANTIME_OUTRANGE";
					return FALSE;
				}
			}

			//Check WhenPost
			if(!isset($this->parameters['mode2Conf']['whenPost'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_WHENPOST_MISSING";
				return FALSE;
			}
			if($this->parameters['mode2Conf']['whenPost'] !== 'byScanEnd' &&
				$this->parameters['mode2Conf']['whenPost'] !== 'byTagDetected'){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_WHENPOST_INVALID";
				return FALSE;
			}

			//Check Dout
			if(!isset($this->parameters['mode2Conf']['dout'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_DOUT_MISSING";
				return FALSE;
			}

			//Check Dout Enable
			if(!isset($this->parameters['mode2Conf']['dout']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_DOUT_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode2Conf']['dout']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_DOUT_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode2Conf']['dout']['enable']){
				//Check Dout port
				if(!isset($this->parameters['mode2Conf']['dout']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_DOUT_PORT_MISSING";
					return FALSE;
				}
				if(intval($this->parameters['mode2Conf']['dout']['port']) !== 0 &&
					intval($this->parameters['mode2Conf']['dout']['port']) !== 1){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_DOUT_PORT_INVALID";
					return FALSE;
				}

				//Check Dout HoldTime
				if(!isset($this->parameters['mode2Conf']['dout']['holdTime'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_DOUT_HOLDTIME_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode2Conf']['dout']['holdTime'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_DOUT_HOLDTIME_OUTRANGE";
					return FALSE;
				}
			}

			//Check Response
			if(!isset($this->parameters['mode2Conf']['response'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_RESPONSE_MISSING";
				return FALSE;
			}

			//Check Response Enable
			if(!isset($this->parameters['mode2Conf']['response']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_RESPONSE_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode2Conf']['response']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_RESPONSE_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['mode2Conf']['response']['enable']){
				//Check Response Timeout
				if(!isset($this->parameters['mode2Conf']['response']['timeout'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "MODE2CONF_RESPONSE_TIMEOUT_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['mode2Conf']['response']['timeout'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>60000))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "MODE2CONF_RESPONSE_TIMEOUT_OUTRANGE";
					return FALSE;
				}
			}

			//Check Request
			if(!isset($this->parameters['mode2Conf']['request'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_REQUEST_MISSING";
				return FALSE;
			}

			//Check Request Enable
			if(!isset($this->parameters['mode2Conf']['request']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "MODE2CONF_REQUEST_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['mode2Conf']['request']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "MODE2CONF_REQUEST_ENABLE_INVALID";
				return FALSE;
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$mode2Conf = $xml->autoMode->mode2Conf;
			$mode2Conf->multiEpcCheck->enable = var_export($this->parameters['mode2Conf']['multiEpcCheck']['enable'],true);
			$mode2Conf->inventory->port = $this->parameters['mode2Conf']['inventory']['port'];
			$mode2Conf->inventory->trigger = $this->parameters['mode2Conf']['inventory']['trigger'];
			$mode2Conf->inventory->scanMode = $this->parameters['mode2Conf']['inventory']['scanMode'];
			if(intval( $this->parameters['mode2Conf']['inventory']['scanMode']) === 1){
				$mode2Conf->inventory->scanMode1->timeout = $this->parameters['mode2Conf']['inventory']['scanMode1']['timeout'];
			}
			if(intval( $this->parameters['mode2Conf']['inventory']['scanMode']) === 2){
				$mode2Conf->inventory->scanMode2->delayTime = $this->parameters['mode2Conf']['inventory']['scanMode2']['delayTime'];
				$mode2Conf->inventory->scanMode2->scanTime = $this->parameters['mode2Conf']['inventory']['scanMode2']['scanTime'];
			}
			$mode2Conf->whenPost = $this->parameters['mode2Conf']['whenPost'];
			$mode2Conf->dout->enable = var_export($this->parameters['mode2Conf']['dout']['enable'],true);
			if($this->parameters['mode2Conf']['dout']['enable']){
				$mode2Conf->dout->port = $this->parameters['mode2Conf']['dout']['port'];
				$mode2Conf->dout->downTime = $this->parameters['mode2Conf']['dout']['downTime'];
			}
			$mode2Conf->response->enable = var_export($this->parameters['mode2Conf']['response']['enable'],true);
			if($this->parameters['mode2Conf']['response']['enable']){
				$mode2Conf->response->timeout = $this->parameters['mode2Conf']['response']['timeout'];
			}
			$mode2Conf->request->enable = var_export($this->parameters['mode2Conf']['request']['enable'],true);

			$xml->asXml($configPath);
			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
