<?php
	class GetRfidConf extends CommandBase
	{
		public function __construct(){
			$this->name = "getRfidConf";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$rfidConf = $xml->autoMode->rfidConf;
			$rfidConfArray = array('antennaSet'=>array());

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();

			$rfidConfSrc = $jax->xml2array($rfidConf->asXml());
			for($i = 0;$i < count($rfidConfSrc['antennaSet']['antenna']);$i++){
				$antenna = array(
									'port'=> intval($rfidConfSrc['antennaSet']['antenna'][$i]['@']['port']),
									'enable'=>($rfidConfSrc['antennaSet']['antenna'][$i]['enable'] == "true"),
									'power'=>intval($rfidConfSrc['antennaSet']['antenna'][$i]['power']),
									'dwell'=>intval($rfidConfSrc['antennaSet']['antenna'][$i]['dwell']),
									'invCnt'=>intval($rfidConfSrc['antennaSet']['antenna'][$i]['invCnt'])
								);
				array_push($rfidConfArray['antennaSet'],array('antenna'=>$antenna));
				unset($antenna);
			}
			$rfidConfArray[frequency] = intval($rfidConfSrc['frequency']);

			return array('rfidConf'=>$rfidConfArray);
		}
	}
?>
