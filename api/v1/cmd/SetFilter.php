<?php
	class SetFilter extends CommandBase
	{
		private $filter = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setFilter";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check Filter
			if(!isset($this->parameters['filter'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_MISSING";
				return FALSE;
			}

			//Check PCFilter
			if(!isset($this->parameters['filter']['pcFilter'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_PCFILTER_MISSING";
				return FALSE;
			}

			//Check PCFilter Enable
			if(!isset($this->parameters['filter']['pcFilter']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_PCFILTER_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['filter']['pcFilter']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "FILTER_PCFILTER_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['filter']['pcFilter']['enable']){
				//Check PCFilter Offset
				if(!isset($this->parameters['filter']['pcFilter']['offset'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_PCFILTER_OFFSET_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['pcFilter']['offset'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>15))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_PCFILTER_OFFSET_OUTRANGE";
					return FALSE;
				}
				//Check PCFilter Length
				if(!isset($this->parameters['filter']['pcFilter']['length'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_PCFILTER_LENGTH_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['pcFilter']['length'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>16))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_PCFILTER_LENGTH_OUTRANGE";
					return FALSE;
				}
				//Check PCFilter Data
				if(!isset($this->parameters['filter']['pcFilter']['data'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_PCFILTER_DATA_MISSING";
					return FALSE;
				}
				if(!ctype_xdigit($this->parameters['filter']['pcFilter']['data'])){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_PCFILTER_DATA_INVALID_LETTER";
					return FALSE;
				}
				if(strlen($this->parameters['filter']['pcFilter']['data']) != 4){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_PCFILTER_DATA_INVALID_LENGTH";
					return FALSE;
				}
				// exec('sudo frucgi validate pcmask '.
				// $this->parameters['filter']['pcFilter']['offset'].' '.
				// $this->parameters['filter']['pcFilter']['length'].' '.
				// $this->parameters['filter']['pcFilter']['data'],$res);
				// $validate = json_decode($res[0],true);
				// if(strpos($validate["status"],'success') === false){
				// 	$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				// 	$this->errorDetail = "FILTER_PCFILTER_DATA_INVALID_FORMAT";
				// 	return FALSE;
				// }
			}

			//Check EPCFilter
			if(!isset($this->parameters['filter']['epcFilter'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_EPCFILTER_MISSING";
				return FALSE;
			}

			//Check EPCFilter Enable
			if(!isset($this->parameters['filter']['epcFilter']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_EPCFILTER_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['filter']['epcFilter']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "FILTER_EPCFILTER_ENABLE_INVALID";
				return FALSE;
			}

			if($this->parameters['filter']['epcFilter']['enable']){
				//Check EPCFilter Offset
				if(!isset($this->parameters['filter']['epcFilter']['offset'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_EPCFILTER_OFFSET_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['epcFilter']['offset'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>127))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_EPCFILTER_OFFSET_OUTRANGE";
					return FALSE;
				}
				//Check EPCFilter Length
				if(!isset($this->parameters['filter']['epcFilter']['length'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_EPCFILTER_LENGTH_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['epcFilter']['length'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>128))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_EPCFILTER_LENGTH_OUTRANGE";
					return FALSE;
				}
				//Check EPCFilter Data
				if(!isset($this->parameters['filter']['epcFilter']['data'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_EPCFILTER_DATA_MISSING";
					return FALSE;
				}
				if(!ctype_xdigit($this->parameters['filter']['epcFilter']['data'])){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_EPCFILTER_DATA_INVALID_LETTER";
					return FALSE;
				}
				if(strlen($this->parameters['filter']['epcFilter']['data']) != 32){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_EPCFILTER_DATA_INVALID_LENGTH";
					return FALSE;
				}
				// exec('sudo frucgi validate epcmask '.
				// $this->parameters['filter']['epcFilter']['offset'].' '.
				// $this->parameters['filter']['epcFilter']['length'].' '.
				// $this->parameters['filter']['epcFilter']['data'],$res);
				// $validate = json_decode($res[0],true);
				// if(strpos($validate["status"],'success') === false){
				// 	$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				// 	$this->errorDetail = "FILTER_EPCFILTER_DATA_INVALID_FORMAT";
				// 	return FALSE;
				// }
			}

			//Check RSSIFilter
			if(!isset($this->parameters['filter']['rssiFilter'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_RSSIFILTER_MISSING";
				return FALSE;
			}
			//Check RSSIFilter Lower
			if(!isset($this->parameters['filter']['rssiFilter']['lower'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_RSSIFILTER_LOWER_MISSING";
				return FALSE;
			}
			//Check RSSIFilter Lower Enable
			if(!isset($this->parameters['filter']['rssiFilter']['lower']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_RSSIFILTER_LOWER_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['filter']['rssiFilter']['lower']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "FILTER_RSSIFILTER_LOWER_ENABLE_INVALID";
				return FALSE;
			}
			if($this->parameters['filter']['rssiFilter']['lower']['enable']){
				//Check RSSIFilter Lower Value
				if(!isset($this->parameters['filter']['rssiFilter']['lower']['value'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_RSSIFILTER_LOWER_VALUE_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['rssiFilter']['lower']['value'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>-100,"max_range"=>0))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_RSSIFILTER_LOWER_VALUE_OUTRANGE";
					return FALSE;
				}
			}
			//Check RSSIFilter Upper
			if(!isset($this->parameters['filter']['rssiFilter']['upper'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_RSSIFILTER_UPPER_MISSING";
				return FALSE;
			}
			//Check RSSIFilter Upper Enable
			if(!isset($this->parameters['filter']['rssiFilter']['upper']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "FILTER_RSSIFILTER_UPPER_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['filter']['rssiFilter']['upper']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "FILTER_RSSIFILTER_UPPER_ENABLE_INVALID";
				return FALSE;
			}
			if($this->parameters['filter']['rssiFilter']['upper']['enable']){
				//Check RSSIFilter Upper Value
				if(!isset($this->parameters['filter']['rssiFilter']['upper']['value'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "FILTER_RSSIFILTER_UPPER_VALUE_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['filter']['rssiFilter']['upper']['value'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>-100,"max_range"=>0))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_RSSIFILTER_UPPER_VALUE_OUTRANGE";
					return FALSE;
				}
			}
			if($this->parameters['filter']['rssiFilter']['upper']['enable'] && 
				$this->parameters['filter']['rssiFilter']['lower']['enable']){
				if($this->parameters['filter']['rssiFilter']['upper']['value'] < $this->parameters['filter']['rssiFilter']['lower']['value']){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "FILTER_RSSIFILTER_LOWER_UPPER_VALUE_INVALID";
					return FALSE;
				}
			}
			

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$filter = $xml->autoMode->filter;
			$filter->pcFilter->enable = var_export($this->parameters['filter']['pcFilter']['enable'],true);
			if($this->parameters['filter']['pcFilter']['enable']){
				$filter->pcFilter->offset = $this->parameters['filter']['pcFilter']['offset'];
				$filter->pcFilter->length = $this->parameters['filter']['pcFilter']['length'];
				$filter->pcFilter->data = $this->parameters['filter']['pcFilter']['data'];
			}
			$filter->epcFilter->enable = var_export($this->parameters['filter']['epcFilter']['enable'],true);
			if($this->parameters['filter']['epcFilter']['enable']){
				$filter->epcFilter->offset = $this->parameters['filter']['epcFilter']['offset'];
				$filter->epcFilter->length = $this->parameters['filter']['epcFilter']['length'];
				$filter->epcFilter->data = $this->parameters['filter']['epcFilter']['data'];
			}
			$filter->rssiFilter->lower->enable = var_export($this->parameters['filter']['rssiFilter']['lower']['enable'],true);
			if($this->parameters['filter']['rssiFilter']['lower']['enable']){
				$filter->rssiFilter->lower->value = $this->parameters['filter']['rssiFilter']['lower']['value'];
			}
			$filter->rssiFilter->upper->enable = var_export($this->parameters['filter']['rssiFilter']['upper']['enable'],true);
			if($this->parameters['filter']['rssiFilter']['upper']['enable']){
				$filter->rssiFilter->upper->value = $this->parameters['filter']['rssiFilter']['upper']['value'];
			}
			
			$xml->asXml($configPath);

			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
