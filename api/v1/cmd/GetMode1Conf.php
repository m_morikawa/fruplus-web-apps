<?php
	class GetMode1Conf extends CommandBase
	{
		public function __construct(){
			$this->name = "getMode1Conf";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$mode1Conf = $xml->autoMode->mode1Conf;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$mode1ConfArray = $jax->xml2array($mode1Conf->asXml());
			$mode1ConfArray['multiEpcCheck']['enable'] = ($mode1ConfArray['pcFilter']['enable'] === "true");
			$mode1ConfArray['multiEpcCheck']['scanReset']['byTrigger']['enable'] = ($mode1ConfArray['multiEpcCheck']['scanReset']['byTrigger']['enable'] === "true");
			$mode1ConfArray['multiEpcCheck']['scanReset']['byTrigger']['port'] = intval($mode1ConfArray['multiEpcCheck']['scanReset']['byTrigger']['port']);
			$mode1ConfArray['multiEpcCheck']['scanReset']['byInterval']['enable'] = ($mode1ConfArray['multiEpcCheck']['scanReset']['byInterval']['enable'] === "true");
			$mode1ConfArray['multiEpcCheck']['scanReset']['byInterval']['interval'] = intval($mode1ConfArray['multiEpcCheck']['scanReset']['byInterval']['interval']);
			$mode1ConfArray['dout']['enable'] = ($mode1ConfArray['dout']['enable'] === "true");
			$mode1ConfArray['dout']['port'] = intval($mode1ConfArray['dout']['port']);
			$mode1ConfArray['dout']['holdTime'] = intval($mode1ConfArray['dout']['holdTime']);
			$mode1ConfArray['response']['enable'] = ($mode1ConfArray['response']['enable'] === "true");
			$mode1ConfArray['response']['timeout'] = intval($mode1ConfArray['response']['timeout']);
			$mode1ConfArray['request']['enable'] = ($mode1ConfArray['request']['enable'] === "true");
			return array('mode1Conf'=>$mode1ConfArray);
		}
	}
?>
