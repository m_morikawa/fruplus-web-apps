<?php
	abstract class CommandBase
	{
		protected $name;
		protected $errorCode = "";
		protected $errorDetail = "";

		const MISSINGPARAMETER = "missingParameter";
		const INVALIDPARAMETERNAME = "invalidParameterName";
		const INVALIDPARAMETERVALUE = "invalidParameterValue";

		public function name(){
			return $this->name;
		}

		abstract public function getResults();
		
		public function getError(){
			return array("code"=>$this->errorCode,"detail"=>$this->errorDetail);
		}
	}
?>
