<?php
	class GetSendData extends CommandBase
	{
		public function __construct(){
			$this->name = "getSendData";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$sendData = $xml->autoMode->sendData;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$sendDataArray = $jax->xml2array($sendData->asXml());
			$sendDataArray['tagInfo']['offset'] = intval($sendDataArray['tagInfo']['offset']);
			$sendDataArray['tagInfo']['length'] = intval($sendDataArray['tagInfo']['length']);
			$sendDataArray['addInfo']['id'] = ($sendDataArray['addInfo']['id'] === "true");
			$sendDataArray['addInfo']['time'] = ($sendDataArray['addInfo']['time'] === "true");
			$sendDataArray['addInfo']['antenna'] = ($sendDataArray['addInfo']['antenna'] === "true");
			$sendDataArray['addInfo']['pc'] = ($sendDataArray['addInfo']['pc'] === "true");
			$sendDataArray['addInfo']['rssi'] = ($sendDataArray['addInfo']['rssi'] === "true");
			$sendDataArray['addInfo']['state'] = ($sendDataArray['addInfo']['state'] === "true");
			$sendDataArray['addInfo']['memory'] = ($sendDataArray['addInfo']['memory'] === "true");
			$sendDataArray['addInfo']['accessInfo']['offset'] = intval($sendDataArray['addInfo']['accessInfo']['offset']);
			$sendDataArray['addInfo']['accessInfo']['length'] = intval($sendDataArray['addInfo']['accessInfo']['length']);
			$sendDataArray['errorInfo']['enable'] = ($sendDataArray['errorInfo']['enable'] === "true");
			$sendDataArray['noReadInfo']['enable'] = ($sendDataArray['noReadInfo']['enable'] === "true");
			return array('filter'=>$sendDataArray);
		}
	}
?>
