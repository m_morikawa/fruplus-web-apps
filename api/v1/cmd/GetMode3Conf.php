<?php
	class GetMode3Conf extends CommandBase
	{
		public function __construct(){
			$this->name = "getMode3Conf";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$mode3Conf = $xml->autoMode->mode3Conf;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$mode3ConfArray = $jax->xml2array($mode3Conf->asXml());
			$mode3ConfArray['event']['in'] = ($mode3ConfArray['event']['in'] === "true");
			$mode3ConfArray['event']['out'] = ($mode3ConfArray['event']['out'] === "true");
			$mode3ConfArray['event']['timeout'] = intval($mode3ConfArray['event']['timeout']);
			$mode3ConfArray['monitor']['enable'] = ($mode3ConfArray['monitor']['enable'] === "true");
			$mode3ConfArray['monitor']['interval'] = intval($mode3ConfArray['monitor']['interval']);
			$mode3ConfArray['dout']['in']['enable'] = ($mode3ConfArray['dout']['in']['enable'] == "true");
			$mode3ConfArray['dout']['in']['port'] = intval($mode3ConfArray['dout']['in']['port']);
			$mode3ConfArray['dout']['in']['holdTime'] = intval($mode3ConfArray['dout']['in']['holdTime']);
			$mode3ConfArray['dout']['out']['enable'] = ($mode3ConfArray['dout']['out']['enable'] == "true");
			$mode3ConfArray['dout']['out']['port'] = intval($mode3ConfArray['dout']['out']['port']);
			$mode3ConfArray['dout']['out']['holdTime'] = intval($mode3ConfArray['dout']['out']['holdTime']);
			$mode3ConfArray['response']['enable'] = ($mode3ConfArray['response']['enable'] == "true");
			$mode3ConfArray['response']['timeout'] = intval($mode3ConfArray['response']['timeout']);
			$mode3ConfArray['request']['enable'] = ($mode3ConfArray['request']['enable'] == "true");

			return array('mode3Conf'=>$mode3ConfArray);
		}
	}
?>
