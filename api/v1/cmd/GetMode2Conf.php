<?php
	class GetMode2Conf extends CommandBase
	{
		public function __construct(){
			$this->name = "getMode2Conf";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$mode2Conf = $xml->autoMode->mode2Conf;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$mode2ConfArray = $jax->xml2array($mode2Conf->asXml());
			$mode2ConfArray['multiEpcCheck']['enable'] = ($mode2ConfArray['multiEpcCheck']['enable'] === "true");
			$mode2ConfArray['inventory']['port'] = intval($mode2ConfArray['inventory']['port']);
			$mode2ConfArray['inventory']['scanMode'] = intval($mode2ConfArray['inventory']['scanMode']);
			$mode2ConfArray['inventory']['scanMode1']['timeout'] = intval($mode2ConfArray['inventory']['scanMode1']['timeout']);
			$mode2ConfArray['inventory']['scanMode2']['delayTime'] = intval($mode2ConfArray['inventory']['scanMode2']['delayTime']);
			$mode2ConfArray['inventory']['scanMode2']['scanTime'] = intval($mode2ConfArray['inventory']['scanMode2']['scanTime']);
			$mode2ConfArray['dout']['enable'] = ($mode2ConfArray['dout']['enable'] == "true");
			$mode2ConfArray['dout']['port'] = intval($mode2ConfArray['dout']['port']);
			$mode2ConfArray['dout']['holdTime'] = intval($mode2ConfArray['dout']['holdTime']);
			$mode2ConfArray['response']['enable'] = ($mode2ConfArray['response']['enable'] == "true");
			$mode2ConfArray['response']['timeout'] = intval($mode2ConfArray['response']['timeout']);
			$mode2ConfArray['request']['enable'] = ($mode2ConfArray['request']['enable'] == "true");

			return array('mode2Conf'=>$mode2ConfArray);
		}
	}
?>
