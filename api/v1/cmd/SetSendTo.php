<?php
	class SetSendTo extends CommandBase
	{
		private $sendTo = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setSendTo";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check SendTo
			if(!isset($this->parameters['sendTo'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDTO_MISSING";
				return FALSE;
			}

			//Check Protocol
			if(!isset($this->parameters['sendTo']['protocol'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDTO_PROTOCOL_MISSING";
				return FALSE;
			}
			$protocols = array('serial','socket','http','https');
			if(!in_array($this->parameters['sendTo']['protocol'], $protocols)){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDTO_PROTOCOL_INVALID";
				return FALSE;
			}
			
			if($this->parameters['sendTo']['protocol'] === "socket"){
				//Check Socket
				if(!isset($this->parameters['sendTo']['socket'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_SOCKET_MISSING";
					return FALSE;
				}

				//Check Socket Host
				if(!isset($this->parameters['sendTo']['socket']['host'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_SOCKET_HOST_MISSING";
					return FALSE;
				}
				if(strlen($this->parameters['sendTo']['socket']['host']) > 256){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_SOCKET_HOST_TOOLONG";
					return FALSE;
				}

				//Check Socket Port
				if(!isset($this->parameters['sendTo']['socket']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_SOCKET_PORT_MISSING";
					return FALSE;
				} 
				if(filter_var($this->parameters['sendTo']['socket']['port'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>65535))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_SOCKET_PORT_OUTRANGE";
					return FALSE;
				}
			}

			if($this->parameters['sendTo']['protocol'] === "http" ||
				$this->parameters['sendTo']['protocol'] === "https"){
				//Check Http
				if(!isset($this->parameters['sendTo']['http'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_MISSING";
					return FALSE;
				}
				
				//Check Http Host
				if(!isset($this->parameters['sendTo']['http']['host'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_HOST_MISSING";
					return FALSE;
				}
				if(strlen($this->parameters['sendTo']['http']['host']) > 256){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_HOST_TOOLONG";
					return FALSE;
				}

				//Check Http Path
				if(!isset($this->parameters['sendTo']['http']['path'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_PATH_MISSING";
					return FALSE;
				}
				if(strlen($this->parameters['sendTo']['http']['path']) > 256){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_PATH_TOOLONG";
					return FALSE;
				}

				//Check Http port
				if(!isset($this->parameters['sendTo']['http']['port'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_PORT_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['sendTo']['http']['port'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>65535))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_PORT_OUTRANGE";
					return FALSE;
				}

				//Check Http Method
				if(!isset($this->parameters['sendTo']['http']['method'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_METHOD_MISSING";
					return FALSE;
				}
				if($this->parameters['sendTo']['http']['method'] !== "get" &&
					$this->parameters['sendTo']['http']['method'] !== "post")
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_METHOD_INVALID";
					return FALSE;
				}

				//Check Http Format
				if(!isset($this->parameters['sendTo']['http']['format'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_FORMAT_MISSING";
					return FALSE;
				}
				if($this->parameters['sendTo']['http']['format'] !== "query" &&
					$this->parameters['sendTo']['http']['format'] !== "json")
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_FORMAT_INVALID";
					return FALSE;
				}

				//Check Http AuthUser
				if(!isset($this->parameters['sendTo']['http']['authUser'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_AUTHUSER_MISSING";
					return FALSE;
				}
				if(strlen($this->parameters['sendTo']['http']['authUser']) > 32){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_AUTHUSER_TOOLONG";
					return FALSE;
				}

				//Check Http AuthUser
				if(!isset($this->parameters['sendTo']['http']['authPass'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDTO_HTTP_AUTHPASS_MISSING";
					return FALSE;
				}
				if(strlen($this->parameters['sendTo']['http']['authPass']) > 32){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDTO_HTTP_AUTHPASS_TOOLONG";
					return FALSE;
				}
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$sendTo = $xml->autoMode->sendTo;
			$sendTo->protocol = $this->parameters['sendTo']['protocol'];
			if($this->parameters['sendTo']['protocol'] === "socket"){
				$sendTo->socket->host = $this->parameters['sendTo']['socket']['host'];
				$sendTo->socket->port = $this->parameters['sendTo']['socket']['port'];
			}
			if($this->parameters['sendTo']['protocol'] === "http" ||
				$this->parameters['sendTo']['protocol'] === "https"){
				$sendTo->http->host = $this->parameters['sendTo']['http']['host'];
				$sendTo->http->path = $this->parameters['sendTo']['http']['path'];
				$sendTo->http->port = $this->parameters['sendTo']['http']['port'];
				$sendTo->http->method = $this->parameters['sendTo']['http']['method'];
				$sendTo->http->format = $this->parameters['sendTo']['http']['format'];
				$sendTo->http->authUser = $this->parameters['sendTo']['http']['authUser'];
				$sendTo->http->authPass = $this->parameters['sendTo']['http']['authPass'];
			}
			$xml->asXml($configPath);

			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>
