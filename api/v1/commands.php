<?php
	//Fixed headers
	header('Content-Type: application/json');
	header('X-Content-Type-Options: nosniff');
	header('Access-Control-Allow-Origin: *');

	$postDat =  json_decode(file_get_contents("php://input"),true);

	if(!isset($postDat['name'])){
		header('HTTP/1.1 400 Bad Request');
		exit();
	}

	spl_autoload_register(function ($class_name) {
		include 'cmd/'.$class_name.'.php';
	});

	$cmdobj = NULL;
	if($postDat['name'] === 'getSendTo'){
		$cmdobj = new GetSendTo();
	}else if($postDat['name'] === 'setSendTo'){
		$cmdobj = new SetSendTo($postDat['parameters']);
	}else if($postDat['name'] === 'getFilter'){
		$cmdobj = new GetFilter();
	}else if($postDat['name'] === 'setFilter'){
		$cmdobj = new SetFilter($postDat['parameters']);
	}else if($postDat['name'] === 'getSendData'){
		$cmdobj = new GetSendData();
	}else if($postDat['name'] === 'setSendData'){
		$cmdobj = new SetSendData($postDat['parameters']);
	}else if($postDat['name'] === 'getRfidConf'){
		$cmdobj = new GetRfidConf();
	}else if($postDat['name'] === 'setRfidConf'){
		$cmdobj = new SetRfidConf($postDat['parameters']);
	}else if($postDat['name'] === 'getMode1Conf'){
		$cmdobj = new GetMode1Conf();
	}else if($postDat['name'] === 'setMode1Conf'){
		$cmdobj = new SetMode1Conf($postDat['parameters']);
	}else if($postDat['name'] === 'getMode2Conf'){
		$cmdobj = new GetMode2Conf();
	}else if($postDat['name'] === 'setMode2Conf'){
		$cmdobj = new SetMode2Conf($postDat['parameters']);
	}else if($postDat['name'] === 'getMode3Conf'){
		$cmdobj = new GetMode3Conf();
	}else if($postDat['name'] === 'setMode3Conf'){
		$cmdobj = new SetMode3Conf($postDat['parameters']);
	}
	
	if($cmdobj->exec()){
		$response = array('name'=>$cmdobj->name(),'state'=>'done','result'=>$cmdobj->getResults());

		header('HTTP/1.1 200 OK');
		echo json_encode($response,JSON_PRETTY_PRINT);
		exit();
	}else{
		$response = array('name'=>$cmdobj->name(),'state'=>'error','error'=>$cmdobj->getError());

		header('HTTP/1.1 400 Bad Request');
		echo json_encode($response,JSON_PRETTY_PRINT);
		exit();
	}

	
?>
