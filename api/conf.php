<?php
$responseStatus='HTTP/1.1 200 OK';
$filePath='';
$xmlData='';
try{
	if(!isset($_REQUEST["type"])){
		throw new Exception('type項目がありません');
	}
	if($_REQUEST["type"] === "mode"){
		$filePath = "/etc/config/fruappconfig.xml";
	}else if($_REQUEST["type"] === "system"){
		$filePath = "/etc/config/systemparameter.xml";
	}else if($_REQUEST["type"] === "log"){
		$filePath = "/etc/config/logconfig.xml";
	}else if($_REQUEST["type"] === "rw"){
		$filePath = "/etc/rwconfig.xml";
	}else{
		throw new Exception('type項目が不正です');
	}
}catch(Exception $e){
	$responseStatus = 'HTTP/1.1 500 Internal Server Error';
}
header("$responseStatus");
header("Access-Control-Allow-Origin:*");
header("Content-type: text/xml");
header("X-Content-Type-Options: nosniff");
readfile($filePath);
?>
