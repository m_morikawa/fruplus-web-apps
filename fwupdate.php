<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$updateflag = array('kernel' => false,'userland' => false);
	$msg = array();
	
	$ret=1;

	if(isset($_POST['fwupdate_submit'])){
		function updateFirmware(&$msg){
			$ret = TRUE;
			if(empty($_FILES['firm_kernel']['name']) && empty($_FILES['firm_userland']['name'])){
				$msg[] = 'ファイルが選択されていません。';
				$msg[] = 'カーネルファイルまたはユーザーランドファイルの少なくともどちらか一方を選択して下さい。';
				$ret = FALSE;
				return $ret;
			}

			if(!empty($_FILES['firm_kernel']['name'])){
				switch ($_FILES["firm_kernel"]['error']) {
					case UPLOAD_ERR_OK:
						//OK
						break;
					case UPLOAD_ERR_INI_SIZE:
						//値: 1; アップロードされたファイルは、php.ini の upload_max_filesize ディレクティブの値を超えています（post_max_size, upload_max_filesize）
						$msg[] = 'アップロードされたカーネルファイルが大きすぎます。' . ini_get('upload_max_filesize') . '以下のファイルをアップロードしてください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_FORM_SIZE:
						//値: 2; アップロードされたファイルは、HTML フォームで指定された MAX_FILE_SIZE を超えています。
						$msg[] = 'アップロードされたカーネルファイルが大きすぎます。' . ($_POST['MAX_FILE_SIZE'] / 1000) . 'KB以下のファイルをアップロードしてください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_PARTIAL:
						//値: 3; アップロードされたファイルは一部のみしかアップロードされていません。
						$msg[] = 'カーネルファイルのアップロードに失敗しています（通信エラー）。もう一度アップロードをお試しください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_NO_FILE:
						//値: 4; ファイルはアップロードされませんでした。（この場合のみ、ファイルがないことを表している）
						$msg[] = 'カーネルファイルをアップロードしてください';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_NO_TMP_DIR:
						//値: 6; テンポラリフォルダがありません。PHP 4.3.10 と PHP 5.0.3 で導入されました。
						$msg[] = 'カーネルファイルのアップロードに失敗しています（システムエラー）。もう一度アップロードをお試しください。';
						$ret = FALSE;
						return $ret;
					default:
						//UPLOAD_ERR_CANT_WRITE 値: 7; ディスクへの書き込みに失敗しました。PHP 5.1.0 で導入されました。
						//UPLOAD_ERR_EXTENSION 値: 8; ファイルのアップロードが拡張モジュールによって停止されました。 PHP 5.2.0 で導入されました。
						$msg[] = 'カーネルファイルを確認して下さい。';
						$ret = FALSE;
						return $ret;
				}
				if(!is_uploaded_file($_FILES['firm_kernel']['tmp_name'])){
					$msg[] = 'カーネルファイルを確認してください。';
					$ret = FALSE;
					return $ret;
				}
				if($_FILES['firm_kernel']['size'] > 2000000){
					$msg[] = 'カーネルファイルが大きすぎます。カーネルファイルを確認してください。';
					$ret = FALSE;
					return $ret;
				}
			}
			if(!empty($_FILES['firm_userland']['name'])){
				switch ($_FILES["firm_userland"]['error']) {
					case UPLOAD_ERR_OK:
						//OK
						break;
					case UPLOAD_ERR_INI_SIZE:
						//値: 1; アップロードされたファイルは、php.ini の upload_max_filesize ディレクティブの値を超えています（post_max_size, upload_max_filesize）
						$msg[] = 'アップロードされたユーザーランドファイルが大きすぎます。' . ini_get('upload_max_filesize') . '以下のファイルをアップロードしてください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_FORM_SIZE:
						//値: 2; アップロードされたファイルは、HTML フォームで指定された MAX_FILE_SIZE を超えています。
						$msg[] = 'アップロードされたユーザーランドファイルが大きすぎます。' . ($_POST['MAX_FILE_SIZE'] / 1000) . 'KB以下のファイルをアップロードしてください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_PARTIAL:
						//値: 3; アップロードされたファイルは一部のみしかアップロードされていません。
						$msg[] = 'ユーザーランドファイルのアップロードに失敗しています（通信エラー）。もう一度アップロードをお試しください。';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_NO_FILE:
						//値: 4; ファイルはアップロードされませんでした。（この場合のみ、ファイルがないことを表している）
						$msg[] = 'ユーザーランドファイルをアップロードしてください';
						$ret = FALSE;
						return $ret;
					case UPLOAD_ERR_NO_TMP_DIR:
						//値: 6; テンポラリフォルダがありません。PHP 4.3.10 と PHP 5.0.3 で導入されました。
						$msg[] = 'ユーザーランドファイルのアップロードに失敗しています（システムエラー）。もう一度アップロードをお試しください。';
						$ret = FALSE;
						return $ret;
					default:
						//UPLOAD_ERR_CANT_WRITE 値: 7; ディスクへの書き込みに失敗しました。PHP 5.1.0 で導入されました。
						//UPLOAD_ERR_EXTENSION 値: 8; ファイルのアップロードが拡張モジュールによって停止されました。 PHP 5.2.0 で導入されました。
						$msg[] = 'ユーザーランドファイルを確認してください。';
						$ret = FALSE;
						return $ret;
				}
				if(!is_uploaded_file($_FILES['firm_userland']['tmp_name'])){
					$msg[] = 'ユーザーランドファイルを確認してください。';
					$ret = FALSE;
					return $ret;
				}
				if($_FILES['firm_userland']['size'] < 10000000){
					$msg[] = 'ユーザーランドファイルが小さすぎます。ユーザーランドファイルを確認してください。';
					$ret = FALSE;
					return $ret;
				}
			}
			return $ret;
		}

		$msg = array();

		if(!updateFirmware($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			exec("sudo frucgi fwupdate1",$ret);
			$status = json_decode($ret[0],true);
			if(strpos($status['status'],'success') === false){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] = 'FWファイル・ディレクトリの準備に失敗しました。'."<br/>";
				$ret=1;
				goto FINISH;
			}else{
				if(!empty($_FILES['firm_kernel']['name'])){
					if(!move_uploaded_file($_FILES['firm_kernel']['tmp_name'], '/var/fwfiles/linux.bin.gz')){
						$alertstate['visible'] = true;
						$alertstate['state'] = false;
						$alertstate['msg'] = 'カーネルファイルの移動に失敗しました。'."<br/>";
						$ret=1;
						goto FINISH;
					}
					$updateflag['kernel'] = true;
				}
				if(!empty($_FILES['firm_userland']['name'])){
					if(!move_uploaded_file($_FILES['firm_userland']['tmp_name'], '/var/fwfiles/romfs.img.gz')){
						$alertstate['visible'] = true;
						$alertstate['state'] = false;
						$alertstate['msg'] = 'ユーザーランドの移動に失敗しました。'."<br/>";
						$ret=1;
						goto FINISH;
					}
					$updateflag['userland'] = true;
				}
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
				$ret=0;
			}
		}
	}
	FINISH:
	if($ret){
		exec("sudo frucgi fwupdate2",$ret);
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::FWUPDATE); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF0BD;"></span><strong>FW更新</strong></h2>
				<?php
					$path=array();
					if($alertstate['visible']){
						if($alertstate['state']){
							if($updateflag['kernel']){
								$path[]='kernel=update';
							}
							if($updateflag['userland']){
								$path[]='userland=update';
							}
							$jspath='./js/fwupdateprocess.php?'.$path[0];
							if(isset($path[1])){
								$jspath.='&'.$path[1];
							}
echo <<< EOF
<script>
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var jspath = "{$jspath}"+"&rand="+rand;
</script>
<div class="alert alert-primary" id="fwupdate">
<strong>ファームのアップデートを開始します。</strong> <br/>
ファームのアップデートを開始するには、<strong>ファームアップデートを開始する</strong>ボタンをクリックして下さい。<br/>
カーネルファイルをアップデートする場合には約1分、ユーザーランドをアップデートする場合には約3分かかります。<br/><br/>
ファームアップデート中は<strong>LED A(緑)が点灯</strong>します。<br/>
アップデート正常終了後、<strong>LED A(緑)は消灯</strong>し、メッセージが表示されます。<br/>
「OK」ボタンをクリック後、リーダーは自動的に再起動します。
<div style="padding-top:24px;">
<button id="btn_update" class="primary" onclick="onUpdateButtonClicked();"><strong>ファームアップデートを開始する</strong></button>
</div>
</div>
<script>
function onScriptLoaded(){
if(updateret){
window.alert('FW更新が完了しました。リーダーは自動的に再起動します。初期化後のIPアドレスは192.168.209.10です。');
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var rebootscript = document.createElement('script');
rebootscripttype = "text/javascript";
rebootscript.src = "./js/rebootandinit.php?rand=" + rand;
document.getElementsByTagName('body')[0].appendChild(rebootscript);
}
}
function onUpdateButtonClicked(){
var uploadscript = document.createElement('script');
uploadscript.type = "text/javascript";
uploadscript.src = jspath;
if(uploadscript.addEventListener){
uploadscript.addEventListener("load", onScriptLoaded, false);
}else if(uploadscript.attachEvent){
uploadscript.attachEvent("onload", onScriptLoaded);
uploadscript.onreadystatechange = function(){
if(uploadscript.readyState === "loaded" || uploadscript.readyState=="complete"){
	onScriptLoaded();
}
}
}
document.getElementById('btn_update').innerHTML="<strong>アップデート中です。このまま画面を操作せずお待ち下さい。</strong>";
document.getElementById('btn_update').disabled=true;
document.getElementsByTagName('body')[0].appendChild(uploadscript);
}
</script>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>

				<form method="post" action="fwupdate.php" class="forms" enctype="multipart/form-data" id="form_fwupdate">
					<section>
						<div>
							<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
							<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
								更新しないファイルは、選択する必要はありません。
							</div>
						</div>
					</section>
					<section>
						<label>カーネル</label>
						<input type="file" name="firm_kernel">
					</section>
					<section>
						<label>ユーザーランド</label>
						<input type="file" name="firm_userland">
					</section>
					<section>
						<button class="primary" name="fwupdate_submit" id="btn_upload" onclick="onFwUploadClicked()">アップロードする</button>
						<?php
							if($ret == 0){
echo <<< EOF
<script>
document.getElementById('btn_upload').disabled = true;
</script>
EOF;
							}	
						?>
						<script>
							function onFwUploadClicked(){
								document.getElementById('btn_upload').disabled = true;
								document.getElementById('btn_upload').innerHTML="<strong>アップロードしています。このままお待ち下さい。</strong>";
								
								var elem = document.createElement('input');
								elem.type = 'hidden';
								elem.name = "fwupdate_submit";
								document.getElementById('form_fwupdate').appendChild(elem);
								document.getElementById('form_fwupdate').submit();
								return true;
							}
						</script>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
</body>
</html>
